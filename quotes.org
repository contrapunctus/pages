#+TITLE: Quotes

[[file:index.org][↩ home]]

* Words I often quote
:PROPERTIES:
:CUSTOM_ID: words-i-often-quote
:END:
#+BEGIN_QUOTE
"If you do what you always did, you get what you always got."

@@html: <footer>@@
possibly [[https://quoteinvestigator.com/2016/04/25/get/][Jessie Potter]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"It's always impossible until it's done."

@@html: <footer>@@
[[https://quoteinvestigator.com/2016/01/05/done/][various]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"Metadata is a love note to the future."

@@html: <footer>@@
[[https://twitter.com/textfiles/status/119403173436850176][Jason Scott]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"The great enemy of communication is the illusion of it."

@@html: <footer>@@
[[https://quoteinvestigator.com/2014/08/31/illusion/][William H. Whyte]], /"Is Anybody Listening?"/ (1950)
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"We try things. Occasionally they even work."

@@html: <footer>@@
Rob Balder, [[https://archives.erfworld.com/Book+1/121][/Erfworld/]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"It's impossible!" \\
"No, it's /necessary/."

@@html: <footer>@@
[[https://en.wikipedia.org/wiki/Interstellar_(film)][/Interstellar/]] (2014)
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"Success occurs when opportunity meets preparation."

@@html: <footer>@@
[[https://en.wikiquote.org/wiki/Zig_Ziglar][Zig Ziglar]], /See You at the Top/ (2000)
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
You live and learn. At any rate, you live.

@@html: <footer>@@
Douglas Adams, [[https://en.wikipedia.org/wiki/Mostly_Harmless][/Mostly Harmless/]] (1992), Chapter 16
@@html: </footer>@@
#+END_QUOTE

* Words which resonate with me
:PROPERTIES:
:CUSTOM_ID: words-which-resonate-with-me
:END:
#+BEGIN_QUOTE
"The problems of the world cannot possibly be solved by skeptics or cynics whose horizons are limited by the obvious realities. We need men who can dream of things that never were."

@@html: <footer>@@
John Keats
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"Not all battles are fought for victory. Some are fought simply to tell the world that someone was there on the battlefield."

@@html: <footer>@@
Ravish Kumar
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"Common sense is the collection of prejudices acquired by age eighteen."

@@html: <footer>@@
[[https://quoteinvestigator.com/2014/04/29/common-sense/][Lincoln Barnett, possibly paraphrasing Albert Einstein]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"Disobedience, in the eyes of any one who has read history, is man's original virtue. It is through disobedience that progress has been made, through disobedience and through rebellion."

@@html: <footer>@@
Oscar Wilde, [[https://en.wikisource.org/wiki/The_Fortnightly_Review/Volume_49/The_Soul_of_Man_Under_Socialism][/The Soul of Man Under Socialism/]] (1891)
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
What nobody tells people who are beginners — and I really wish someone had told this to me . . . is that all of us who do creative work, we get into it because we have good taste. But there is this gap. For the first couple years you make stuff, and it’s just not that good. It’s trying to be good, it has potential, but it’s not.

But your taste, the thing that got you into the game, is still killer. And your taste is why your work disappoints you. A lot of people never get past this phase. They quit. Most people I know who do interesting, creative work went through years of this. We know our work doesn’t have this special thing that we want it to have. We all go through this. And if you are just starting out or you are still in this phase, you gotta know it’s normal and the most important thing you can do is do a lot of work. Put yourself on a deadline so that every week you will finish one story.

It is only by going through a volume of work that you will close that gap, and your work will be as good as your ambitions. And I took longer to figure out how to do this than anyone I’ve ever met. It’s gonna take awhile. It’s normal to take awhile. You’ve just gotta fight your way through.

@@html: <footer>@@
[[http://www.brainpickings.org/2014/01/29/ira-glass-success-daniel-sax/][Ira Glass]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
LORD GORING. You see, Phipps, Fashion is what one wears oneself. What is unfashionable is what other people wear.

PHIPPS. Yes, my lord.

LORD GORING. Just as vulgarity is simply the conduct of other people.

PHIPPS. Yes, my lord.

LORD GORING. And falsehoods the truths of other people.

PHIPPS. Yes, my lord.

LORD GORING. Other people are quite dreadful. The only possible society is oneself.

PHIPPS. Yes, my lord.

LORD GORING. To love oneself is the beginning of a lifelong romance, Phipps.

PHIPPS. Yes, my lord.

# Fashion is what one wears oneself. What is unfashionable is what other people wear.

# Just as vulgarity is simply the conduct of other people.

# And falsehoods the truths of other people.

# Other people are quite dreadful. The only possible society is oneself.

# To love oneself is the beginning of a lifelong romance.

@@html: <footer>@@
Oscar Wilde, [[https://en.wikisource.org/wiki/An_Ideal_Husband/Act_III][/An Ideal Husband/]], Act III
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
Birds born in a cage think flying is an illness.

@@html: <footer>@@
[[https://en.wikiquote.org/wiki/Alejandro_Jodorowsky][Alejandro Jodorowsky]]
@@html: </footer>@@
#+END_QUOTE

#+BEGIN_QUOTE
"Il est bien malaisé (puisqu’il faut enfin m’expliquer) d’ôter à des insensés des chaînes qu’ils révèrent."\\

"It is difficult to free fools from the chains they revere."

@@html: <footer>@@
[[https://en.m.wikiquote.org/wiki/Obedience#V][Voltaire]]
@@html: </footer>@@
#+END_QUOTE

* Some of mine
:PROPERTIES:
:CUSTOM_ID: some-of-mine
:END:
#+BEGIN_QUOTE
Music fingerings are like software. You're either saying, "What were they smoking when they wrote this? Easier to roll my own instead.", or you're saying, "Oh god, why did I roll my own instead of using a perfectly workable existing solution?"

@@html: <footer>2020-06-23</footer>@@
#+END_QUOTE

#+INCLUDE: includes.org::#footer :only-contents t
