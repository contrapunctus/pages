#+TITLE: A life-changing keyboard tweak
#+DATE: 2021-06-11T19:44:23+0530

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

Suggested by my good friend [[https://pranavats.gitlab.io/][Pranav Vats]] -

1. Disable any keyboard configuration made through your desktop environment. Disable Sticky Keys, too, if you use them. (I had them enabled, and turns out it was a /sticking point/ in getting this to work 😏)
2. Install =xcape=, e.g. =apt install xcape= on Debian.
3. In a terminal, run =setxkbmap -option ctrl:swapcaps ; xcape=

Now your left Ctrl is both Ctrl and Esc! It sends Ctrl if you hold it down in a combination, and Escape if you press and release it.

This is a vast improvement for me. It makes Ctrl-based bindings everywhere more tolerable, without removing Escape from Caps Lock (my previous setup). There's minimal movement away from the home row, no curling of the little finger.

I like to use modal editing (currently Boon, previously god-mode, and before that, Evil), which involves switching modes with the Escape key. But I still have many Ctrl-based bindings, such as C-h (backward-delete-character), C-w (backward-kill-word), and C-u (delete till start of line); moreover, Boon does not currently work in the minibuffer (I can't remember if god-mode or Evil did), so there I must use vanilla Emacs' Ctrl/Meta-based keybindings.

Additionally, most non-Emacs applications - in my case Gajim and Tor Browser - use Ctrl-based bindings for common tasks. C-n (new chat in Gajim), C-t (new tab in Firefox), C-w (close tab), and the good old C-x, C-c and C-v have gotten a whole lot easier to hit.

Getting used to this will take some time, though - I've accidentally hit the physical left Ctrl a few times, triggering Caps Lock. But I can see it having a major impact. For one, I might be reaching for Backspace within Emacs a lot less, since C-h will now be a more efficient and comfortable alternative.

Did you try it? Or perhaps you have a similar game-changer to share? [[file:/media/data/anon/Documents/Text Files/homepage/contrapunctus/org/contact.org][Write to me!]] 🙂

Note: =xcape= returns immediately and keeps running in the background, so make sure you only run one =xcape= process at a time.


@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+INCLUDE: includes.org::#footer :only-contents t
