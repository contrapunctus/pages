<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-07-31 Wed 18:22 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Keyboard machinations with Kmonad</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">Keyboard machinations with Kmonad</h1>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="emacs-sidebars.html">← Previous: Emacs Sidebars</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="literate-programming-2.html">→ Next: Literate Programming 2</a></div>
 </nav>
</p>

<p>
Yesterday, as I was going through the xcape issue tracker (I wrote about <a href="life-changing-keyboard-tweak.html">my experience with xcape</a> earlier) trying to find a way to configure it such that Shift would emit parentheses and place the cursor between them when tapped, I learned about <a href="https://github.com/kmonad/kmonad">Kmonad</a>.
</p>

<p>
I began looking through their documentation to see if it could do what I wanted. I was pleasantly surprised to see that they chose s-expressions for their configuration format. I couldn't find my answers in the documentation, but the small-yet-active community in the IRC channel confirmed that it had the constructs for what I wanted.
</p>

<p>
Time to install it and give it a shot.
</p>
<section id="outline-container-installation" class="outline-2">
<h2 id="installation"><a href="#installation">Installation</a></h2>
<div class="outline-text-2" id="text-installation">
<p>
It wasn't present in the Debian Testing repositories, but installation was far from the ordeal I was expecting - I downloaded the Linux binary from their releases, did the usual <code>chmod u+x &lt;binary&gt;</code>, and placed it in <code>~/bin/</code>. It's not every day that life is so easy - that was all it took.
</p>
</div>
</section>
<section id="outline-container-configuration" class="outline-2">
<h2 id="configuration"><a href="#configuration">Configuration</a></h2>
<div class="outline-text-2" id="text-configuration">
<p>
Each configuration file requires a <code>defcfg</code> form -
</p>
<div class="org-src-container">
<pre class="src src-lisp">(defcfg
  ;; ** For Linux **
  input  (device-file "/dev/input/by-id/usb-04d9_1203-event-kbd")
  output (uinput-sink "KMonad output")
  fallthrough true)
</pre>
</div>

<p>
The easiest thing to do was to swap Caps Lock and Escape, like I do with <code>setxkbmap</code>.
</p>
<div class="org-src-container">
<pre class="src src-lisp">(defsrc CapsLock Esc)
(deflayer default Esc CapsLock)
</pre>
</div>

<p>
I tried invoking kmonad with the <code>-d</code> option to dry-run my configuration and report any errors. Once I had a configuration it was happy with, I tried running it without <code>-d</code>, and ran into the uinput permissions problem covered in their FAQ. With some hassle and more help from the IRC channel (lesson learned - udev rule files must have the extension <code>.rules</code>), I finally got it running.
</p>
</div>
</section>
<section id="outline-container-inserting-parentheses-with-shift" class="outline-2">
<h2 id="inserting-parentheses-with-shift"><a href="#inserting-parentheses-with-shift">Inserting parentheses with Shift</a></h2>
<div class="outline-text-2" id="text-inserting-parentheses-with-shift">
<p>
I had a few false starts here; first, I tried -
</p>
<div class="org-src-container">
<pre class="src src-lisp">(defalias
  parens      (tap-macro \( \) Left :delay 5)
  left-shift  (tap-next @parens LeftShift)
  right-shift (tap-next @parens RightShift))

(defsrc
  LeftShift RightShift
  CapsLock Esc)

(deflayer default
  @left-shift @right-shift
  Esc CapsLock)
</pre>
</div>
<p>
&#x2026;only for it to complain about the <code>:delay</code> keyword. I was helpfully informed on the IRC channel that the <code>:delay</code> feature was not yet released.
</p>

<p>
Then, I tried -
</p>
<div class="org-src-container">
<pre class="src src-lisp">(defalias
  parens      (tap-macro \( P5 \) P5 Left)
  left-shift  (tap-next @parens LeftShift)
  right-shift (tap-next @parens RightShift))

(defsrc
  LeftShift RightShift
  CapsLock Esc)

(deflayer default
  @left-shift @right-shift
  Esc CapsLock)
</pre>
</div>
<p>
&#x2026;which did result in <code>()</code> being inserted when I pressed Shift, but also in the cursor going backward indefinitely until I pressed something else.
</p>

<p>
With more input from the community over the IRC channel, I finally got it working -
</p>
<div class="org-src-container">
<pre class="src src-lisp">(defalias
  parens      (tap-macro \( P5 \) P5 Left P5)
  left-shift  (tap-next @parens LeftShift)
  right-shift (tap-next @parens RightShift))

(defsrc
  LeftShift RightShift
  CapsLock Esc)

(deflayer default
  @left-shift @right-shift
  Esc CapsLock)
</pre>
</div>
</div>
</section>
<section id="outline-container-using-space-to-send-ctrl" class="outline-2">
<h2 id="using-space-to-send-ctrl"><a href="#using-space-to-send-ctrl">Using Space to send Ctrl</a></h2>
<div class="outline-text-2" id="text-using-space-to-send-ctrl">
<p>
Lastly, I tried getting Space to trigger Ctrl when used as part of a key chord (or "held", in Kmonad parlance). The rationale was that Ctrl is used in nearly every program—the idea of using the strongest fingers to press it, and to do so without leaving the home row, was alluring. Within Emacs, I'm a heavy user of C-m for Enter, C-w for backward-kill-word, and C-h for backward-delete-char, along with many other Ctrl-based bindings (even in a modal editing setup).
</p>

<p>
This was what got me to finally learn, through trial and error, what all the different button-definition commands were for.
</p>

<p>
<code>tap-next</code> behaves similar (but not identical) to xcape -
</p>
<ul class="org-ul">
<li>to send Space, you tap and release Space;</li>
<li>to send Ctrl, you hold Space and press another key.</li>
</ul>
<p>
I tested it in Emacs' <code>speed-type-top-1000</code>, and quickly ran into its downside - if you type quickly, Space may not release fast enough and you can unwittingly send Ctrl. Having to avoid it while typing slowed me down quite a lot.
</p>

<p>
With <code>tap-hold</code> -
</p>
<ul class="org-ul">
<li>to send Space, you tap and release Space within the timeout;</li>
<li>to send Ctrl, you hold Space beyond the timeout.</li>
</ul>
<p>
This, too, is not suitable for fast typing.
</p>

<p>
<code>tap-hold-next</code> is a combination of the above two, and is (I think) the exact behaviour of xcape -
</p>
<ul class="org-ul">
<li>to send Space, you tap and release Space;</li>
<li>to send Ctrl, you either hold Space and press another key, or hold Space beyond the timeout.</li>
</ul>
<p>
Also not suitable for fast typing. The use case is to prevent unintentional triggers of Ctrl (not very useful in this case, but imagine if it was Esc (tap) and Ctrl (hold) instead of Ctrl and Space, and an accidental Esc could close your chat window) - if you accidentally press Space, just hold it down and it will trigger Space instead of Ctrl.
</p>

<p>
<code>tap-next-release</code> seemed like the solution -
</p>
<ul class="org-ul">
<li>to send Space, you tap Space (no timeout, no waiting for the release);</li>
<li>to send Ctrl, you hold Space and press and <span class="underline">release</span> another key to send Ctrl-&lt;that key&gt;.</li>
</ul>
<p>
With this, I was able to type at my usual speed. The only downside <label id='fnr.1' for='fnr-in.1.1716693' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.1716693' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
A possible downside to setting tap/hold behaviour for Space in general is that you can't hold it down to insert many spaces. Not something I'm particularly concerned about. I also imagine it would be an issue if one were playing a game, such as an FPS.
</span> was that holding down the keychord did nothing (e.g. you can't hold down C-left to go back many words).
</p>

<p>
<code>tap-hold-next-release</code> removed that downside -
</p>
<ul class="org-ul">
<li>to send Space, you tap Space (no timeout, no waiting for the release);</li>
<li>to send Ctrl, you either hold Space and press another key, or hold Space and another key for longer than the timeout. Once the timeout is exceeded, holding down the chord repeats it.</li>
</ul>

<p>
My final configuration looks like this -
</p>
<div class="org-src-container">
<pre class="src src-lisp">(defcfg
  ;; ** For Linux **
  ;; TVSe Gold Prime
  input  (device-file "/dev/input/by-id/usb-04d9_1203-event-kbd")
  output (uinput-sink "KMonad output")
  fallthrough true)

(defalias
  parens      (tap-macro \( P5 \) P5 Left P5)
  ;; tap-hold-next rather than tap-next, to prevent accidental
  ;; insertion of parentheses while typing capital letters
  left-shift  (tap-hold-next 700 @parens LeftShift)
  right-shift (tap-hold-next 700 @parens RightShift)
  space       (tap-hold-next-release 200 Space LeftCtrl))

(defsrc
  LeftShift RightShift
  Space
  CapsLock Esc)

(deflayer default
  @left-shift @right-shift
  @space
  Esc CapsLock)
</pre>
</div>
</div>
</section>
<section id="outline-container-epilogue" class="outline-2">
<h2 id="epilogue"><a href="#epilogue">Epilogue</a></h2>
<div class="outline-text-2" id="text-epilogue">
<p>
I'm quite excited to get used to this new keyboard layout, even though I'm still accidentally hitting Caps Lock and exiting chats in Gajim at the moment. 😅 (stemming from the muscle memory of <a href="life-changing-keyboard-tweak.html">my previous configuration</a>)
</p>

<p>
In the upcoming days I might try creating a layer for emojis, to get a uniform interface for the ones I use regularly. I also want to try binding C-h to Backspace and C-w to C-backspace, hopefully bringing these bindings out of terminals and my Emacs to all applications.
</p>

<p>
<a href="file:///media/data/anon/Documents/Text Files/homepage/contrapunctus/org/contact.html">Drop me a line</a>
</p>

<p>
<a href="https://liberapay.com/contrapunctus">Support me on Liberapay</a>
</p>


<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="emacs-sidebars.html">← Previous: Emacs Sidebars</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="literate-programming-2.html">→ Next: Literate Programming 2</a></div>
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
