<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-07-31 Wed 18:27 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Magrathea</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">Magrathea</h1>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="the-web-today.html">← Previous: The web today</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="life-changing-keyboard-tweak.html">→ Next: A life-changing keyboard tweak</a></div>
 </nav>
</p>

<p>
<a href="gemini:tilde.team/~contrapunctus/gemlog/magrathea.gmi">gemini/gemtext version</a>
</p>

<p>
In July 2020, I finally got frustrated enough by existing software that I decided to look for a new software environment. Existing solutions not quite fitting the bill, I have since then been designing one. I came to call it Magrathea, after the planet in The Hitch Hiker's Guide to the Galaxy.
</p>

<p>
The ideas are not particularly unique. They are drawn from existing platforms - UNIX, Android, and Emacs are particular inspirations. Very late in the design process, I discovered that Plan 9 has many similar ideas, too.
</p>

<p>
Additionally, the impact of <a href="https://malleable.systems">malleable.systems</a> on this endeavor cannot be overstated. Their seven principles opened my eyes to what I actually liked about Emacs (my previous environment of choice), to what it lacks, and to the world of malleable software beyond it. The malleable.systems principles are the spirit that guides Magrathea -
</p>

<blockquote>
<ol class="org-ol">
<li>Software must be as <a href="https://malleable.systems/mission/#1-easy-to-change">easy to change</a> as it is to use it</li>
<li>All layers, from the user interface through functionality to the data within, must support <a href="https://malleable.systems/mission/#2-arbitrary-recombination-and-reuse">arbitrary recombination and reuse</a> in new environments</li>
<li>Tools should strive to be easy to begin working with but still have lots of <a href="https://malleable.systems/mission/#3-open-ended-potential">open-ended potential</a></li>
<li>People of all experience levels must be able to <a href="https://malleable.systems/mission/#4-retain-ownership-and-control">retain ownership and control</a></li>
<li>Recombined workflows and experiences must be <a href="https://malleable.systems/mission/#5-freely-sharable">freely sharable</a> with others</li>
<li>Modifying a system should happen <a href="https://malleable.systems/mission/#6-modifying-in-the-context-of-use">in the context of use</a>, rather than through some separate development toolchain and skill set</li>
<li>Computing should be a <a href="https://malleable.systems/mission/#7-thoughtfully-crafted">thoughtfully crafted</a>, fun, and empowering experience</li>
</ol>
</blockquote>

<p>
Here is an overview of the primary ideas and goals of Magrathea.
</p>
<section id="outline-container-system-wide-requirements" class="outline-2">
<h2 id="system-wide-requirements"><a href="#system-wide-requirements">System-wide requirements</a></h2>
<div class="outline-text-2" id="text-system-wide-requirements">
<ol class="org-ol">
<li>It must be implemented in a Lisp. <label id='fnr.1' for='fnr-in.1.1716693' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.1716693' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
I really can't stand non-Lisp languages, largely because of -

<ol class="org-ol">
<li>the complex and inconsistent syntax,</li>
<li>lack of structural editing, and</li>
<li>lack of means to extend the language.</li>
</ol>


Moreover, Lisp is the <i>original</i> memory-safe language - I really hate applications which crash, or languages which want me to bother with irrelevant details which computers are perfectly capable of handling.



Common Lisp brings, in addition, the ability to modify a running application, a unique object system, and - when desired - the ability to add type information and run static type checks.
</span>
<ul class="org-ul">
<li>Common Lisp is my top choice here.</li>
</ul></li>

<li><a id="org9beed48"></a> <i>Scale</i> - it must be possible for a single person to (re)implement it within a reasonable timeframe, and to maintain and understand it completely.</li>

<li><a id="org2dc1f56"></a> <i>Immediate response</i> - it must be possible for the user to inspect, reconfigure, and modify any component of the system as it runs, without restarting anything.</li>

<li>The system and application user interface is primarily graphical.
<ul class="org-ul">
<li>Textual interfaces are something I am avoiding, unless there is something which cannot be achieved without them.</li>
</ul></li>

<li>Window management is tiled.
<ul class="org-ul">
<li>I have not seen any particular benefits of overlapping windows. It only seems to increase the burden of window management for the user.</li>
</ul></li>

<li>All GUIs can be modified by the user - similar to Firefox's toolbar customization, but more powerful. All applications can be inspected and customized by the user, similar to Emacs.</li>

<li><a id="orgde0f8b8"></a> The system is object-oriented.
<ul class="org-ul">
<li>I will probably use the Common Lisp Object System (CLOS).</li>
</ul></li>

<li><a id="orgfa898cb"></a> <i>Consistency</i> is important. It simplifies both use and maintenance (<a href="#org9beed48">#2</a>).</li>

<li><a id="org7161a33"></a> <i>Operations depend on objects</i>, not context. Operations for specific classes of objects are registered with the system by applications. Wherever in the system an object appears, the operations applicable to it can be performed. This greatly improves consistency (<a href="#orgfa898cb">#8</a>).</li>

<li><i>Means of combination in GUI</i> - applications <label id='fnr.2' for='fnr-in.2.852288' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.852288' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
Yes, <i>GUI</i> applications 🙂
</span> can pass data to each other. They can be composed to create pipelines, similar to UNIX shell pipelines. Consequently, an application need not implement something another application does, keeping the size of the system down (<a href="#org9beed48">#2</a>). Components are modular and the user has the power to trivially recombine them as they wish.

<ul class="org-ul">
<li>I have numerous examples in my notes, but the one that started it all was working in a video editor and wanting to open an audio track in a separate audio editor, then having the changes reflected in the video editor. Currently this requires much fiddling with files, exporting, and importing. In Magrathea, I want it to be a trivial operation.</li>

<li>Unlike shell pipelines, Magrathea pipelines are not necessarily unidirectional - it must be possible not only for data to flow from application A → B → C, but also from A → B → C → B → A, akin to a call stack.</li>

<li><a id="org0043671"></a> Whether data passed in a pipeline is updated in source/destination programs only at application launch and exit, or as soon as it change on either side, is something I've not yet been able to decide on.</li>
</ul></li>

<li><i>Means of abstraction in GUI</i> - pipelines can be abstracted into GUI elements, such as buttons, and added to existing applications.</li>

<li>GUI elements can be combined to create new applications. As long as the user does not need to create new primitives from scratch, this requires no knowledge other than the knowledge required to use the system - it is done through clicking, menus, and drag-and-drop. <label id='fnr.3' for='fnr-in.3.1471703' class='margin-toggle sidenote-number'><sup class='numeral'>3</sup></label><input type='checkbox' id='fnr-in.3.1471703' class='margin-toggle'><span class='sidenote'><sup class='numeral'>3</sup>
Now you know why it's called Magrathea—the program which creates new programs, named after the planet which creates new planets.
</span></li>

<li><p>
The default applications and their configuration must target the majority of users. Design the UX to -
</p>
<ol class="org-ol">
<li>Help the new user use the system, and perhaps become an experienced user;</li>
<li>Help the experienced user tinker with the system, and perhaps become a new tinkerer;</li>
<li>Help the tinkerer program the system, and perhaps become a new programmer.</li>
</ol>

<p>
In particular, this means that we do not simply give a bunch of components which the end user must learn to combine before they can use them - we give them a software environment which is familiar to them, one they can start using right away.
</p></li>

<li>Wherever possible, move common functionality from applications to the system. This aids consistency (<a href="#orgfa898cb">#8</a>) and keeps the size down (<a href="#org9beed48">#2</a>).
<ul class="org-ul">
<li>For instance, how to render data such as mathematical notation or musical staff notation, or how to syntax highlight code, is not the purview of individual programs, but of the system itself. Additional packages can install new data types and/or ways of rendering them.</li>
</ul></li>

<li>It must be designed for multiple devices - laptops, phones, tablets, et al. Among other things, this means -
<ul class="org-ul">
<li>It must be possible to run the exact same programs on another device, rather than having to reinvent reduced-functionality versions of the same. <label id='fnr.4' for='fnr-in.4.9670868' class='margin-toggle sidenote-number'><sup class='numeral'>4</sup></label><input type='checkbox' id='fnr-in.4.9670868' class='margin-toggle'><span class='sidenote'><sup class='numeral'>4</sup>
This is the situation we have with existing mobile operating systems.
</span></li>
<li>All GUIs must be able to adapt to different devices.</li>
</ul></li>
</ol>
</div>
</section>
<section id="outline-container-minor-ideas" class="outline-2">
<h2 id="minor-ideas"><a href="#minor-ideas">Minor ideas</a></h2>
<div class="outline-text-2" id="text-minor-ideas">
<p>
Some things I have given relatively less thought to -
</p>

<ol class="org-ol">
<li value="16"><p>
<i>Omnipresent undo and redo</i> - it should be possible to undo or redo most operations in the system, including -
</p>
<ul class="org-ul">
<li>launching or closing applications,</li>
<li>changing window layout,</li>
<li>jumping to a new position in an audio/video player,</li>
</ul>

<p>
&#x2026;and so on. In a way, it is similar to the forward/back buttons in a web browser, or the back button in Android.
</p></li>

<li><a id="org1520f12"></a> Per-application capabilities/permissions, sandboxing, and resource limits, so the user can run untrusted applications fearlessly.</li>

<li><a id="orgff52af2"></a> <i>Adapting to hardware</i> - I can't stand slow software; but rather than forgo human-friendliness for the sake of performance, I'd prefer a system which adapts to the available hardware resources.</li>

<li>It must not be possible for any single application to consume resources to the point that the system becomes unresponsive&#x2026;unless the user explicitly permits it. Related to <a href="#org1520f12">#17</a> and <a href="#orgff52af2">#18</a>.</li>

<li>Structured documentation at the system package level. Each package must provide a tutorial, how-to guides, an explanation, and a reference - this way, the <a href="https://documentation.divio.com/">four essential types of documentation</a> are covered.</li>
</ol>
</div>
</section>
<section id="outline-container-some-application-level-desires" class="outline-2">
<h2 id="some-application-level-desires"><a href="#some-application-level-desires">Some application-level desires</a></h2>
<div class="outline-text-2" id="text-some-application-level-desires">
</div>
<div id="outline-container-wysiwyg-word-processor" class="outline-3">
<h3 id="wysiwyg-word-processor"><a href="#wysiwyg-word-processor">WYSIWYG word processor</a></h3>
<div class="outline-text-3" id="text-wysiwyg-word-processor">
<p>
&#x2026;one that is at par with WYSIWYM systems like LaTeX. I detest edit-compile-view cycles (see <a href="#org2dc1f56">#3</a> - immediate response). This is the tool I would like to use to write literate programs. Some aspects that set it apart from existing solutions -
</p>

<ul class="org-ul">
<li>The final document format must be a simpler and smaller specification than the ODF standard (to serve <a href="#org9beed48">#2</a>).</li>

<li>The document <i>must not</i> make any network requests nor run any scripts when rendered, to make it safer than mainstream formats (alternatively, see <a href="#org1520f12">#17</a>).</li>

<li>I really like Org's outlines, tags, properties, and "sparse trees", and this word processor will support these features.</li>

<li>The ability to link to arbitrary elements of a document, especially to verbatim text or a range of verbatim text.</li>

<li><p>
"Layers", similar to those in a raster image editing program. Each layer can contain either -
</p>
<ol class="org-ol">
<li>content (anchored to content in another layer), or</li>
<li>presentation information (applied to content in another layer), or</li>
<li>modifications to content in another layer.</li>
</ol>

<p>
Thus, content/style can be layered onto a document, while keeping it separate from the older content/style. This is useful for annotations, editorial changes, and other things. Layers can be individually hidden or displayed differently by the program.
</p></li>

<li>There may be a concept of (local) transclusion.</li>

<li>While scripting in a document is useful, I'm not sure how to add it without turning it into an application platform like the WWW 🙁</li>
</ul>
</div>
</div>
<div id="outline-container-sheet-music-viewer" class="outline-3">
<h3 id="sheet-music-viewer"><a href="#sheet-music-viewer">Sheet music viewer</a></h3>
<div class="outline-text-3" id="text-sheet-music-viewer">
<p>
The dominant mode of viewing music on a screen is via PDFs or images, and I absolutely hate it - it is equivalent to storing text as an image. You cannot reflow, copy, (easily) annotate, or modify the appearance. The navigation commands understand images or pages, not bars or systems of music.
</p>

<p>
The sheet music viewer I have in mind will read structured data (e.g. MusicXML <label id='fnr.5' for='fnr-in.5.8196407' class='margin-toggle sidenote-number'><sup class='numeral'>5</sup></label><input type='checkbox' id='fnr-in.5.8196407' class='margin-toggle'><span class='sidenote'><sup class='numeral'>5</sup>
Or better yet, s-expressions/EDN.
</span>) and render it according to user-specified constraints (e.g. screen size, font size; it could even transpose the music if necessary, hide one or more instruments, and so on).
</p>

<p>
The design of the system (see <a href="#orgde0f8b8">#7</a>-<a href="#org7161a33">#9</a>) means one can select and operate on (copy, etc) individual musical elements (notes, stems, beams, bars, systems, etc) from this viewer, and indeed anywhere sheet music is displayed (e.g. when embedded in a document, chat message, or email), without each application having to support it separately.
</p>

<p>
The concept of layers from the word processor is available in the sheet music too, so it is trivial to make annotations and editorial changes.
</p>
</div>
</div>
<div id="outline-container-multitrack-metaformat-audio" class="outline-3">
<h3 id="multitrack-metaformat-audio"><a href="#multitrack-metaformat-audio">A multitrack metaformat for audio</a></h3>
<div class="outline-text-3" id="text-multitrack-metaformat-audio">
<p>
Every audio format I know of is a squashing together of multiple tracks into one. I'm imagining a metaformat where each track is preserved separately. As a result -
</p>
<ul class="org-ul">
<li>the volume of each instrument could be changed (e.g. you want to hear something more prominently, perhaps for transcription - this is currently done by EQ, which is an inexact hack), and</li>
<li>specific tracks could be extracted and unwanted tracks could be removed (e.g. remove vocal tracks to make a karaoke version).</li>
</ul>
</div>
</div>
</section>
<section id="outline-container-conclusion" class="outline-2">
<h2 id="conclusion"><a href="#conclusion">Conclusion</a></h2>
<div class="outline-text-2" id="text-conclusion">
<p>
So that's the essentials of Magrathea. I have tried to avoid mentioning implementation details for now. It is not as ambitious at its foundation as <a href="http://metamodular.com/lispos.pdf">CLOSOS</a> or <a href="https://dynamicland.org/">Dynamicland</a>, perhaps. While I would love it if it empowered lay users as I want it to, it is designed primarily to cater to me.
</p>

<p>
If you have some feedback, or want to help, I'd love to hear from you. <a href="gemini:tilde.team/~contrapunctus/contact.gmi">Get in touch!</a>
</p>
</div>
</section>
<section id="outline-container-comments" class="outline-2">
<h2 id="comments"><a href="#comments">Comments</a></h2>
<div class="outline-text-2" id="text-comments">
<p>
khinsen, 2021-05-20T19:27:32+Z
</p>
<blockquote>
<p>
A nice vision - and a nice name!
</p>

<p>
It doesn't look easy though. You have two items in your wish list that could well turn out to be contradictory: understandable by a single person, and graphics everywhere. Graphics support means a lot of essential complexity, just because of the sheer number of items you need to provide and make composable. Even text is very complicated nowadays, thanks to Unicode.
</p>

<p>
Even though your top priority is Common Lisp, you should probably take a close look at today's Smalltalk systems, which check off many points on your list, probably more than any other. Finally, on a slightly provocative note, I'll disagree with your footnote on Lisps. There are languages with even less syntax: the concatenative languages of the Forth family. Structural editing in Forth is just the "move to next word" function that every text editor has. Consider Factor, a modern descendant of Forth. It has the equivalent of Common Lisps macros and even reader macros, plus a CLOS-like object system. It's also a live programming environment. Of course it's a niche language, with nothing near the support you get in the Common Lisp world, but I just wanted to point out that your criteria don't necessarily lead to Lisp.
</p>
</blockquote>


<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="the-web-today.html">← Previous: The web today</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="life-changing-keyboard-tweak.html">→ Next: A life-changing keyboard tweak</a></div>
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
