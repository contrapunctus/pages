#+TITLE: Page moved

You've arrived here through an old link. This post was renamed to something less dramatic. Please update your bookmarks.

The new page is here - blog:unexpected-opposition-to-free-software-advocacy.org
