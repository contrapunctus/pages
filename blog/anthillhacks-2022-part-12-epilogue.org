#+TITLE: AnthillHacks 2022

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

@@html: <div class="note">@@
You might like to read [[file:anthillhacks-2022.org][all parts of this series on a single page.]]
@@html: </div>@@

#+INCLUDE: anthillhacks-2022.org::#epilogue
