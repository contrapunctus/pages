<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2025-03-06 Thu 11:38 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Why Prav is Important</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">Why Prav is Important</h1>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="portable-computer.html">← Previous: Design notes for my ideal portable computer</a></div> <a href="index.html">↩ blog</a> 
 </nav>
</p>

<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#cooperative-structure">Cooperative structure</a></li>
<li><a href="#mass-adoption-focus">The mass adoption focus</a></li>
<li><a href="#uniform-xmpp-experience">A uniform and foolproof XMPP experience</a></li>
<li><a href="#service-and-support">Service and support</a></li>
<li><a href="#conclusion">Conclusion</a></li>
</ul>
</div>
</nav>

<p>
<a href="https://prav.app/">Prav</a> has been an interesting development in the XMPP world, as well as the Indian free software community. I've been following the project and occasionally contributing to it.
</p>

<p>
But from its inception it has faced disapproving questions - if not outright hostility - from the XMPP community, especially the developers.
</p>

<p>
Why is Prav forking projects? Why is it running its own service? What does it even bring to the table?
</p>
<section id="outline-container-cooperative-structure" class="outline-2">
<h2 id="cooperative-structure"><a href="#cooperative-structure">Cooperative structure</a></h2>
<div class="outline-text-2" id="text-cooperative-structure">
<p>
From the beginning, the cooperative governance structure was Prav's most important characteristic to me.
</p>

<p>
Companies claim to "listen" to their customers and employees. Hardly any of them actually do. And if they do today, they could choose not to, at any time.
</p>

<p>
This is because a privately-owned company is a dictatorship. The owner has sweeping powers and is not answerable to anyone - not even customers. The owner can do anything if they think customers (or the coercion of network effect) will let them get away with it.
</p>

<p>
The failings of the privately-owned company should be obvious to all, with the most recent examples being -
</p>

<ul class="org-ul">
<li>Privately-owned companies making record profits, their CEOs buying their second or third yatches and buying up islands, all the while inflating prices, decreasing quality and quantity of products, and forcing their workers to work longer hours and survive on poverty wages.</li>

<li>Privately-owned news and social media promoting disinformation, fascist and pro-billionaire propaganda, and hate against minorities. They have been quite effective in influencing elections across the world.</li>

<li>Private companies updating their "privacy" policies has become something of a meme in tech circles. Such decisions are usually unilateral, and it's almost a certainty that they're made to expand on how the company can exploit its user.</li>
</ul>

<p>
If companies are bad, typical BDFL or doocratic free software projects can be even worse from a non-programmer user's perspective, which often don't even pretend to care about users. "You're not entitled to anything from the project" is the common rebuke. And while freedom-respecting code is owned collectively, other important parts of a project - trademarks, domains, servers, etc - are usually owned privately, which hinders collective governance.
</p>

<p>
But what if companies were <i>legally required</i> to give their users and workers a say in the issues that affect them? Be it income, working hours, remote work policy, privacy and data retention policies, product roadmap, and more?
</p>

<p>
That's exactly what cooperatives do. By definition, cooperatives give their customers, employees, and other stakeholders an equal <label id='fnr.1' for='fnr-in.1.2856921' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.2856921' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
One person, one vote. The alternative I'm aware of is one <i>share</i> one vote, which grants disproportionate power to the richest stakeholders.
</span> and legally-enforced voice in their governance. If you want to fix the majority of ills in the world, starting a cooperative is the best constructive action you can take.
</p>

<p>
Prav is in the process of registering itself as such a cooperative. The difference is that what I've described above is a direct democracy, whereas Prav is aiming to be a representative democracy instead (similar to the <a href="https://osmfoundation.org/">OpenStreetMap Foundation</a>) - Prav members won't make proposals and vote on them, but will instead elect board members to represent them and make decisions.
</p>

<p>
That's not nearly as empowering as direct democracy, but in a world of dictatorships (i.e. privately-owned companies), even representative democracy is a vast improvement.
</p>

<p>
(Incidentally, Prav is drafting its bylaws. Everyone is invited to <a href="https://codeberg.org/prav/bye-laws">read the bylaws draft</a>, provide their input, and participate in the draft process.)
</p>
</div>
</section>
<section id="outline-container-mass-adoption-focus" class="outline-2">
<h2 id="mass-adoption-focus"><a href="#mass-adoption-focus">The mass adoption focus</a></h2>
<div class="outline-text-2" id="text-mass-adoption-focus">
<p>
The cooperative structure is great and all, but recent events made me realize that there's much more to Prav.
</p>

<p>
Not all projects are focused on mass adoption. Snikket, which inspired Prav to rebrand and improve existing clients, is clearly focused on tech-savvy users looking to self-host (or pay for hosting).
</p>

<p>
But Prav is. And that changes everything - from features to business model.
</p>

<p>
For example, we know that the pay-to-download model (used by Conversations, Monocles Chat, and Cheogram) is a tough sell - at least in India, where Prav is based.
</p>

<p>
If I've made a new acquaintance and am onboarding them to XMPP, I can't ask them to pay for the app right away - I need to let them get acquainted with not just me and the app, but also the ecosystem of channels and other people on the network. Given time, they are more likely to become paying customers.
</p>

<p>
With that in mind, a free demo or subscription model is preferable. And Prav is aiming to run on a subscription model.
</p>

<p>
The focus on mass adoption is really what enables the rest of the aspects I will now describe. XMPP needs someone covering this base, and Prav is stepping up to the challenge.
</p>
</div>
</section>
<section id="outline-container-uniform-xmpp-experience" class="outline-2">
<h2 id="uniform-xmpp-experience"><a href="#uniform-xmpp-experience">A uniform and foolproof XMPP experience</a></h2>
<div class="outline-text-2" id="text-uniform-xmpp-experience">
<p>
Of late, I've spent a lot of time talking to people on the Fediverse, trying to get users of WhatsApp, Telegram, Signal, and Matrix to move to XMPP.
</p>

<p>
The main complaint I hear is that XMPP clients and servers have too many differences in features and UX. You have to know what clients and services to use, and which ones to avoid.
</p>

<p>
And even after you know the clients to use, there's still the problem of feature parity.
</p>

<ul class="org-ul">
<li>If I want easy onboarding, I have to choose Quicksy - and forget about message retraction, moderation, and the hundred other enhancements present in Cheogram and Monocles Chat.</li>

<li>If I want group AV calls, I have to choose Dino. But if I want an otherwise featureful and mature desktop client, I have to choose Gajim and live without AV calls entirely.</li>

<li>If I want to participate in threads, I have to use a mobile client. People on desktop clients make threads less useful, because desktop clients don't support threads. (They can still read threaded messages, but can't see threads, and their own messages will be unthreaded.)</li>
</ul>

<p>
The obvious solution to this is to improve existing clients and servers, so they have greater feature parity and a more uniform UX.
</p>

<p>
But you're also limited by how much upstream projects are willing to accept. As one example, the developer of a prominent Conversations fork noted that the PRs he made to Conversations were ignored, and sometimes re-implemented by upstream with no meaningful difference from his PRs.
</p>

<p>
And not all public servers will want to enable all modern XMPP features - it may even be unfair to expect them to.
</p>

<p>
So while improving clients, servers, and existing instances is important, sooner or later you will also need to take the Snikket route - fork existing projects, provide a consistent experience in all of them (upstreaming your changes wherever possible), and remove the client discovery problem. Running your own service removes the service discovery problem.
</p>

<p>
This is why it's a good thing that Prav is working on rebranded and extended forks of Android and iOS Quicksy, and is running its own service.
</p>

<p>
The vision of clients on all platforms with uniform features and branding is a vitally important area to work on. Prav has not yet acquired the financial and human resources necessary to realize it (help welcome), but it is one of the few projects making an effort in this direction.
</p>
</div>
</section>
<section id="outline-container-service-and-support" class="outline-2">
<h2 id="service-and-support"><a href="#service-and-support">Service and support</a></h2>
<div class="outline-text-2" id="text-service-and-support">
<p>
You've probably heard of Quicksy, the XMPP client and service uniquely suited to onboarding non-technical people -
</p>

<ol class="org-ol">
<li>Unlike Conversations, Cheogram, and Monocles Chat, it's available for free on the Play Store and App Store.</li>

<li>It uses your phone number for registration and recovery - no password to enter or forget. You just enter your phone number and an OTP.</li>

<li>It has contact discovery to easily discover other Quicksy users through their phone numbers.</li>
</ol>

<p>
Quicksy is perfect at this task, and I'm grateful to its developer for providing such an option in the XMPP ecosystem, and also for making it freedom-respecting software.
</p>

<p>
For about a year, I've been onboarding people to XMPP using Quicksy. I was so dependent on it that "Quicksy" was almost synonymous with "XMPP" for me.
</p>

<p>
It was great&#x2026;until recently, when a number of people whom I tried to onboard to Quicksy complained about not receiving their registration OTP.
</p>

<p>
Surprisingly, the Quicksy developer offered no solutions at all. All I could discern from his terse response was that there is no support channel for the Quicksy service. These users were left in the lurch, and so was I.
</p>

<p>
There's also something to be said about the moderation policies of the Conversations community channel, which acts as the primary (unofficial) point of support for Quicksy. It has <i>no written rules whatsoever</i> (I've known the owner to state that he considers them to be unnecessary) - so any mod can ban you without the faintest warning, and for no reason apparent to the other moderators. Your participation is entirely at the whim of the channel owner.
</p>

<p>
Fortunately, the federated nature of XMPP allows us to choose the server with the characteristics we like - including the quality of support.<label id='fnr.2' for='fnr-in.2.572549' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.572549' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
And Quicksy being freedom-respecting software allowed Prav to fork it rather than having to reimplement it from scratch.
</span>
</p>

<p>
I know the people behind Prav. They are the ones behind the <a href="https://fsci.in/">Free Software Community of India (FSCI)</a> and the volunteer-run XMPP servers <a href="https://www.poddery.com/">poddery.com</a> and <a href="https://diasp.in/">diasp.in/durare.org</a>. They are friendly, passionate about software freedom, privacy, and decentralization, and empathetic to users' needs.
</p>

<p>
They also enforce a formal CoC in their support channel. You're not randomly getting banned just because the owner woke up on the wrong side of the bed.<label id='fnr.3' for='fnr-in.3.4993595' class='margin-toggle sidenote-number'><sup class='numeral'>3</sup></label><input type='checkbox' id='fnr-in.3.4993595' class='margin-toggle'><span class='sidenote'><sup class='numeral'>3</sup>
"Smokey, this is not 'Nam, this is bowling - there are rules."
</span>
</p>
</div>
</section>
<section id="outline-container-conclusion" class="outline-2">
<h2 id="conclusion"><a href="#conclusion">Conclusion</a></h2>
<div class="outline-text-2" id="text-conclusion">
<p>
Some people have questioned the <i>modus operandi</i> and even the necessity of Prav in the past. For the longest time, I myself used to think that the cooperative aspect was the most important bit. But Prav is doing what nobody else is, and fulfills a unique and critically important role in the XMPP ecosystem.
</p>

<p>
You can think of Prav as the equivalent of New Vector Limited for XMPP - aiming to provide a uniform experience and helpful support, but also&#x2026;
</p>
<ol class="org-ol">
<li>instead of developing a new NIH protocol, Prav is building on existing standards;</li>
<li>instead of making a new NIH client and server, Prav is rebranding and extending existing projects;</li>
<li>instead of a <del>dictatorship</del> privately-owned company, Prav is a cooperative;</li>
<li>instead of leveraging "open source" and providing proprietary servers for "nation-scale" deployments (wink wink), Prav is run by free software activists and is committed to software freedom.</li>
</ol>

<p>
If you like the sound of that, Prav is looking for developers, sponsors, designers, translators, and communicators. It's a friendly community, so go and see what you can do. They are also hosting their first conference - <a href="https://prav.app/conf/2025/">PravConf</a> - in March. Check it out!
</p>

<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="portable-computer.html">← Previous: Design notes for my ideal portable computer</a></div> <a href="index.html">↩ blog</a> 
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
