;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-html-postamble . nil)
              (org-export-with-section-numbers . nil)
              (org-export-with-toc . nil)
              (org-html-self-link-headlines . t)
              (org-html-head . "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />")
              (org-link-file-path-type . relative)
              (eval . (add-hook 'org-insert-heading-hook
                                (lambda nil
                                  (save-excursion
                                    (org-set-property "CREATED"
                                                      (format-time-string "%FT%T%z"))))
                                nil t))
              (eval . (require 'ox-tufte))
              (eval . (add-hook 'after-save-hook #'org-tufte-export-to-html 0 t))
              (org-footnote-section . nil)
              (org-footnote-auto-adjust . nil)
              (eval . (auto-id-mode 1)))))
