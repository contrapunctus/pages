#+TITLE: Organizing OpenStreetMap mapping parties
#+CREATED: 2024-04-10

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+TOC: headlines 2

I've been asked a number of times to provide tips on organizing mapping parties. I don't claim to be an authority on the matter - I'm still figuring things out.

* What I've done
:PROPERTIES:
:CREATED:  2024-04-10T14:35:35+0530
:CUSTOM_ID: work-experience
:END:
I should probably mention my "work experience", in chronological order -

1. Volunteering to teach [[https://en.wikipedia.org/wiki/Accredited_Social_Health_Activist][ASHA]] workers to map, at a Médecins Sans Frontières (Doctors Without Borders) clinic (2023). This was my first time teaching a group of people to map, and it was a +disaster+ /vital learning experience/.[fn:1]

2. Conducting OSM workshops at [[https://indiafoss.net/2023/unconference-track/f22804299a][IndiaFOSS]] (2023) and [[https://indiafoss.net/Delhi/2024/talk/da15132b17][DelhiFOSS]] (2024)

3. Conducting six mapping parties in Delhi, with help from [[https://blog.sahilister.in/][Sahil]], [[https://ravidwivedi.in/][Ravi]], [[https://aaru_swartz.gitlab.io/][Aaru Swartz]] from [[https://sflc.in/][SFLC.in]], and many other friends. We started in June 2023, and we'll have our seventh one this Saturday.

4. Conducting a 5-day workshop for students of Planning and Architecture at IIT Roorkee (2023), in which I was assisted by [[https://ravidwivedi.in/][Ravi]].

[fn:1] It was here that I learned that you need to be at least a little literate in English and technology to even be able to register for OSM, let alone contribute. Additionally, the ASHAs' own areas were very sparsely mapped, and most of them didn't have laptops either.

* What I've learned
:PROPERTIES:
:CREATED:  2024-04-10T02:16:09+0530
:CUSTOM_ID: guidance
:END:
This should not be considered perfect advice, for I'm constantly observing, fine-tuning, and sometimes changing my approach wholesale, to give the best experience to participants and to attract as many people as I can to the magical world of OpenStreetMap contribution.

** The location
:PROPERTIES:
:CREATED:  2024-04-10T14:02:41+0530
:CUSTOM_ID: location
:END:
Select a place to map, one where participants would feel safe walking around.[fn:2]

Markets make good mapping party locations - shops and eateries are useful to everyone, and (unlike e.g. park features) are usually laid out linearly, which makes them easier to map.

Avoid indoor situations such as malls, as they can be challenging for beginners.

You can map buildings and streets remotely in advance, to give new mappers some points of reference.

@@html: <div class="row">@@
@@html:   <div class="column">@@
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-tips/orig/2024-08-09 Satya Niketan buildings before.svg]]
@@html:   </div>@@
@@html:   <div class="column">@@
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-tips/orig/2024-08-09 Satya Niketan buildings after.svg]]
@@html:   </div>@@
@@html: </div>@@
@@html:<figcaption>Before and after - preparing buildings in Satya Niketan for a mapping party (using JOSM and the Terracer plugin)<figcaption>@@

[fn:2] This often means that mapping parties are held in relatively posh areas. Which means that map data in economically-backward areas is worse. For this reason, I've been pondering about making an "advanced" variant of mapping parties, where we choose less-than-posh locations and only invite experienced and passionate mappers who are not afraid to venture in these areas.

** Announcing and inviting
:PROPERTIES:
:CREATED:  2024-04-10T14:03:18+0530
:CUSTOM_ID: announcing-inviting
:END:
Announce the party at least two weeks in advance. Make an event on https://osmcal.org/ and share it around. Reach out in
+ the [[https://lists.openstreetmap.org/listinfo][OpenStreetMap mailing lists]]
+ the [[https://community.openstreetmap.org/][OpenStreetMap Discourse forums]]
+ local OSM communities (the [[https://openstreetmap.community/][OpenStreetMap Community Index]] may help you find them)
+ FOSS circles
+ LUGs (Linux User Groups)
+ GIS circles
+ colleges (especially data science and planning and architecture departments)
+ social media, etc.
By announcing and conducting parties regularly, you'll build up a community.

Outside of OpenStreetMap communities, people don't know what OpenStreetMap is, nor what a "mapping party" entails. Describe what you'll be doing, e.g. like [[https://old.reddit.com/r/delhi/comments/1byzz2e/7th_openstreetmap_mapping_meetup/][our Reddit announcement]].

Make sure to get the phone number of each participant. Talking over phone is the most reliable way to announce the party, send reminders and confirmations, and to coordinate during the party.[fn:3] That way, there's no question of "didn't check my messages/email".

[fn:3] And I say that as someone who doesn't like talking on phone, unless it's over Jabber/XMPP. Cell calls are very likely to always be recorded by the telecom provider.


As a mapping party organizer, you are in the powerful position of choosing the platform for the community you are building. It's easy to pick a proprietary network like Telegram/Discord/WhatsApp or an unsustainable one like Matrix, but for sake of your community's privacy and freedom, I strongly recommend avoiding that temptation and using [[https://wiki.openstreetmap.org/wiki/XMPP][Jabber/XMPP]] instead.

We frequently onboard participants to XMPP using projects like [[https://quicksy.im/][Quicksy]] and [[https://prav.app/][Prav]], which make onboarding and contact discovery easier. We don't bridge to other networks either, so everyone can enjoy a modicum of privacy and freedom.

You should join me and others at the [[https://join.jabber.network/#openstreetmap@conference.macaw.me?join][OpenStreetMap XMPP room]] and the [[https://join.jabber.network/#osm-in@conference.a3.pm?join][OSM India XMPP room]].

** Equipment
:PROPERTIES:
:CREATED:  2024-04-10T14:59:25+0530
:CUSTOM_ID: equipment
:END:
We always ask participants to get some optional equipment - a charging cable, a power bank, and a tape measure of some sort.

Still, we find it useful to bring some spare devices.

Someone's phone develops a problem? I can lend them my old spare Android phone.

I thought I wouldn't need my own laptop at an indoor iD mapping party, but it ended up being used by a number of people - some who just happened to be there (and we encouraged them to try it out), and some who missed the memo that a laptop was required.

Get a power bank and charging cables for common ports. Somebody else's phone might run low on battery and you could lend them your power bank and cable.

Bring water and snacks on hot days.

All surveyors should carry pepper spray for emergencies, whether they involve human or non-human animals.[fn:4] India is not for beginners, as the meme goes.

[fn:4] Usually, aggressive pet dogs who are outside without a leash.

*** A tape measure?
:PROPERTIES:
:CREATED:  2024-04-10T17:58:45+0530
:CUSTOM_ID: tape-measure
:END:
A tape measure can be used to map a number of things, like [[https://wiki.openstreetmap.org/wiki/Key:maxwidth:physical][maxwidth:physical]] for barriers and [[https://wiki.openstreetmap.org/wiki/Key:width:carriageway][width:carriageway]] for streets.[fn:5]

[fn:5] Especially important when you realize that OSM has no concept of "can a bicycle/motorcycle/car/truck [physically] pass through this street?" - you can only say whether a vehicle is /legally/ allowed or not. Which is perfectly sensible (since these vehicles don't have a universal standard width), but it means you have to survey street widths to answer that question.


A tailor's tape is good for mapping small lengths like [[https://wiki.openstreetmap.org/wiki/Tag:barrier%3Dkerb][kerb]] heights, [[https://wiki.openstreetmap.org/wiki/Tag:barrier%3Dbollard][bollard]] gaps, widths of [[https://wiki.openstreetmap.org/wiki/Tag:barrier%3Dwicket_gate][pedestrian gates]], etc. (These are all tagged with =maxwidth:physical=.)

A metal tape measure (used by carpenters and plumbers) can do all that, and also measure somewhat larger distances such as widths of narrow streets. High-quality ones are stiff enough to [[https://www.youtube.com/watch?v=6517aSbzlyE][extend a significant distance without folding]], allowing you to measure without needing someone to hold the other end.[fn:6]

[fn:6] This distance is known as the "standout" of a tape measure.


Surveyor's tape (which can be 100m or more in length) can be used for all of the above, and also for mapping widths of larger streets, buildings footprints, indoor rooms, boundaries, etc.

A laser distance meter (like the [[https://www.bosch-pt.co.in/in/en/products/glm-50-23-g-0601072VK0][Bosch GLM 50-23G]], which I have) is a more expensive alternative to the above. It allows measurements in hard-to-reach places, e.g. measuring the =maxheight:physical= of tall height restrictors, gates, bridges, etc. Some have ingress protection (IP) ratings, allowing for use in dusty and/or wet conditions. Some of them also have angle sensors, useful for accurate distance measurements and for measuring inclines of roads, wheelchair ramps, steps, etc. (Inclines can also be measured on modern smartphones without any other equipment, using free software such as [[https://f-droid.org/packages/org.woheller69.level/][Bubble]].)

** Meeting
:PROPERTIES:
:CREATED:  2024-04-10T14:03:47+0530
:CUSTOM_ID: meeting
:END:
We usually meet in a park near the mapping location. Ideally, the park will have a gazebo or a cluster of benches to seat everyone comfortably - if not, make do with a good patch of grass. In order to find a good location to meet, I've started surveying parks in the weeks leading up to a mapping party.

The park shouldn't be too far from the area to be mapped - you don't want people getting tired from just walking to the place. If you're regrouping at the same spot, they also need to get back after the survey.

It's important to fix a time for every component of the party and to stick to the schedule. In particular, don't wait for latecomers - it amounts to punishing those who came on time, and gives latecomers further incentive to come late. Participants don't have infinite time and energy, so waste not a minute.

** Teaching
:PROPERTIES:
:CREATED:  2024-04-10T14:04:29+0530
:CUSTOM_ID: teaching
:END:
OSM is a big rabbithole, so avoid trying to teach everything at once. Despite our best intentions, experienced mappers can overload people with information. Make a lesson plan each time and stick to it.

You may have to talk about some common issues before they happen. Some of them depend on the editor you'll use.
1. Don't copy from other maps
2. Use Title Case in names
3. The name field can be left blank - not everything has a name
4. What the address fields mean (beginners often fill =addr:street= with the full address)
5. The format for phone numbers

** Mapping
:PROPERTIES:
:CREATED:  2024-04-10T14:04:18+0530
:CUSTOM_ID: mapping
:END:
Split people into groups of 2-3 people, assign each group an area to cover, and agree upon meeting again in (say) one hour. Review everyone's changes.

This motivates people to contribute while also preventing duplication and conflicts. You could add a competitive aspect by announcing a winning group...but we've never done that.

* What apps should we introduce?
:PROPERTIES:
:CUSTOM_ID: what-apps
:END:
This is a question I agonize over before each party and workshop.

[[https://organicmaps.app/][Organic Maps]]? [[https://osmand.net/][OsmAnd]]? [[https://streetcomplete.app/][StreetComplete]]? [[https://every-door.app/][Every Door]]? [[https://wiki.openstreetmap.org/w/index.php?title=Go_Map!!][Go Map!!]] and [[https://vespucci.io/][Vespucci]]? One, or many? And how many is too many?

(Note that I don't have a whole lot of experience with Go Map!!, as I don't own an iOS device. I only got to see it in action once in the IIT Roorkee workshop, and all I know is that it's the closest thing to StreetComplete or Vespucci on iOS.)

** Old advice and new realizations
:PROPERTIES:
:CREATED:  [2024-04-10 Wed 02:43]
:CUSTOM_ID: old-advice
:END:
Remember when I said I may change my approach radically? Just a week ago, I would have advised -

#+BEGIN_QUOTE
New mappers should start editing with Organic Maps. This gives them one easy app for both using and editing OSM.

Those who outgrow Organic Maps can progress to OsmAnd, Every Door, StreetComplete, and (for the most experienced ones) Vespucci/Go Map!!

You can also host an indoor party to teach iD.
#+END_QUOTE

Organic Maps also adds your contributed POIs to its local map, which is cool - you can use your own changes right away.

That said, user [[https://www.openstreetmap.org/user/uknown-gryphus][uknown-gryphus]] also points out that
#+BEGIN_QUOTE
[...] Organic Maps can be very confusing if you updated the buildings and street network just before the mapping party. In Bengaluru we've had great success with EveryDoor. [[https://fieldpapers.org/][Field Papers]] were also a lot of fun but a little confusing for beginners.
#+END_QUOTE

Recently, however, I realized that StreetComplete, iD, Vespucci, Go Map!! and Every Door may be much more fun to work with than Organic Maps and OsmAnd. Thus, by not introducing them to newcomers, /I may be giving them a bad impression of OSM editing, and preventing the entry of new regular contributors./

** StreetComplete
:PROPERTIES:
:CREATED:  2024-04-11T22:57:15+0530
:CUSTOM_ID: streetcomplete
:END:
StreetComplete gamifies OpenStreetMap contribution with its quests, scores, and achievements. Its icons and pictorial answers add visual interest. The wide variety of quests prevent editing from becoming monotonous.

StreetComplete is not limited to just editing existing data, either. The Places and Things overlays allow users to add shops, amenities, etc, and the address overlay allows users to add addresses even when there aren't any buildings.

That said, StreetComplete may not be suitable for all kinds of mapping parties. When you use StreetComplete, /what you add is determined by what quests are shown (and selectable) on the map./ The app dictates what you map, not the other way round.

That's good if your mapping party has no specific mapping goals in mind. In such cases, SC also has a nice team mode which divides quests among mappers working in the same area.

But this aspect is not useful for the kind of mapping parties we hold, where we want to exhaustively cover a specific set of features within an hour of surveying. [fn:7] So while we may mention it, it's not our primary choice for a mapping party editor.

[fn:7] In my parties, I ask people to map shops/eateries (and their details), building addresses, and building levels (for those cool 3D buildings).

** iD, Vespucci, and Go Map!!
:PROPERTIES:
:CREATED:  2024-04-11T22:57:30+0530
:CUSTOM_ID: id,-vespucci,-go-map!
:END:
iD, Vespucci, and Go Map!! allow users to draw ways. This is a lot like drawing (as in illustrating), which most people enjoy - there's something very satisfying about tracing roads, buildings, feature boundaries, etc, getting them just right, and admiring your handiwork.

That could be much more fun than contributing with Organic Maps or OsmAnd, which have the simpler but less interesting process of "add nodes and fill forms".

iD/Vespucci sessions can take many forms -
 * indoors or outdoors
 * remote mapping - in participants' neighborhoods, or in unmapped areas
 * walking around and creating OSM notes (or writing in [[https://fieldpapers.org/][Field Papers]]), then regrouping to edit from them (this lets you inspect everyone's edits and provide feedback faster)

#+CAPTION: Teaching iD at our recent indoor mapping party at SFLC.in
#+ATTR_HTML: :loading lazy :class center
[[file:img/mapping-party-tips/small/2024-03-17_iD_mapping_party.jpg][file:img/mapping-party-tips/orig/2024-03-17_iD_mapping_party.jpg]]

And of course you can also do mobile mapping sessions with Vespucci, focusing on one feature type per session to keep complexity down, e.g. -
1. shops, eateries, and amenities as tagged nodes
2. buildings (possibly with building details and addresses) as tagged nodes
3. building as areas
4. roads

** Vespucci? Really?
:PROPERTIES:
:CREATED:  [2024-04-10 Wed 02:44]
:CUSTOM_ID: vespucci
:END:
Even though I use Vespucci for almost all my editing, I had my doubts about teaching it to newcomers. Many regular contributors find it difficult to get started with. It took me a few false starts myself, and it certainly has its rough edges.

But we must remember is that most of us who struggled to learn it (and sometimes succeeded) did not have the help of a teacher. As teachers, it's our job to craft a good learning experience - we can flatten steep learning curves, soften rough edges, and move stumbling blocks out of the way.

Simon Poole (the current lead developer of Vespucci) provided another interesting perspective. In response to my question -
#+BEGIN_QUOTE
Has anybody tried introducing Vespucci to complete OSM newbies? What was your experience like? Or is it a Very Bad Idea™?
#+END_QUOTE

He said[fn:8] -
#+BEGIN_QUOTE
That is something that needs a multi-page-essay as an answer. But essentially the question is whether you want largest possible numbers, or long-term engagement.

Vespucci isn't geared towards beginners in the sense that it assumes you know your way around OSM data at least a bit. [...] So you would need to cover those basics in an introduction.

But in general, just because something isn't straightforward doesn't mean it affects long-term engagement. If anything, it is the other way around - that is, *learning a skill will drive engagement, and not having to learn anything will lead to disinterest.*
#+END_QUOTE

[fn:8] I've edited it a bit. The emphasis is mine.

** Organic Maps is still important
:PROPERTIES:
:CREATED:  [2024-04-10 Wed 02:53]
:CUSTOM_ID: organic-maps
:END:
That said, I think the "using the map motivates you to contribute" factor is also important.

So I would definitely still introduce Organic Maps to newcomers, mentioning its benefits (works offline, no account needed, no ads, no tracking, free software, and you can improve the map directly [fn:9]), and then focus on dedicated editors.

I don't "install and walk-through" OsmAnd these days - rather, I mention it as an option for power users, along with why they may want to try it, e.g. -
+ hourly incremental updates
+ GPS track recording
+ multi-modal PT routing, especially share taxi routing,
+ aerial imagery
+ rendering of details like road surface, road smoothness, road access restrictions, locked gates, street lighting, etc
+ showing photos from Wikimedia Commons, etc.

Power users can then explore OsmAnd by themselves at home. (Thanks to Sahil for this insight.)

[fn:9] Rather than submitting suggestions to Google Maps and waiting for days, weeks, or months for them to be approved by whatever opaque approval system they use.

** Every Door - our choice
:PROPERTIES:
:CREATED:  2024-04-10T11:49:13+0530
:CUSTOM_ID: every-door
:END:
I haven't discussed Every Door yet, which the Bangalore community often uses and recommends. The advantages I see are -

1. It works on both Android and iOS. [fn:12]

2. It's not a full-fledged editor like Vespucci/iD/Go Map!!, so the interface is simpler, it's easier to learn, and new editors can't mess up ways or geometries.

3. It's the only "simple" mobile editor that can add building nodes with building details. If you want a 3D map of a neighbourhood, teach Every Door to the participants, and ask them to map =building:levels=. [fn:10]

4. It's the only "simple" mobile editor which can use aerial imagery out of the box. [fn:11] That means you can use it in undermapped areas.

5. It has some unique interfaces that can make life easier -

   1. When you add a certain feature, it auto-applies the key-values you used for the last instance of that feature. This is unique to Every Door (as far as I know) and often speeds up editing significantly.

   2. In Micromapping mode, it has an easy interface for adding directions to e.g. surveillance cameras, which I haven't seen in any other editor. (Which is why I switch to Every Door for adding surveillance cameras.)

   3. While Vespucci can add more details, the details Every Door /can/ add, the interface allows adding much faster. (In Vespucci, most fields need to be tapped to see the preset values, which then take another tap to select the value. In Every Door, common values can be selected in one tap.)

6. It generates changeset comments for you, so you can upload frequently without breaking your flow.

7. It may provide more long-term engagement than editing in Organic Maps and OsmAnd, because of the satellite imagery, different modes, more operations and feature types, and richer forms.

There are some downsides, though -

1. There's no compass, which makes it harder to know where to place new data.

   + Recent versions of Every Door now show you the path you've recently traversed. It's like track recording in Vespucci, but automatic and emphemeral. That somewhat alleviates the lack of compass.

2. You can rotate the map, but there's no rotation indicator, nor an easy way to reset the rotation.

   There's a workaround for this, but it's clunky - switch back to OSM tiles and get them the right way up. It snaps into the north-side-up orientation and resists rotation there - that's all the indication you currently get.

3. It does not support [[https://github.com/Zverik/every_door/issues/455#issuecomment-1763369436][addr:block]], [[https://github.com/Zverik/every_door/issues/663][addr:suburb, and addr:neighbourhood]], which makes it problematic for Indian addresses - unless your first-time mappers have no issue with adding tags manually, but that brings the editing experience to OsmAnd levels of raw key-value drudgery.

   The best workarounds I can think of are -
   1. leave address mapping to somewhat experienced editors
   2. let new editors add addresses as notes; someone can add the data later

4. There's no dark mode.

If you are not careful of the first two, you can get thrown off and make mistakes in editing. I saw this happen when we used it at our IndiaFOSS workshop.

Despite those flaws, Every Door currently seems to be the best option for us, and has been our mainstay for the last few mobile mapping parties.

[fn:10] You can achieve this with StreetComplete by enabling the building levels quest, but somebody has to trace the buildings in the area in advance.

[fn:11] Satellite imagery in OsmAnd needs some setup.

In Vespucci, it requires dealing with the powerful-but-complex layer system. (The need for setup has hopefully been reduced with Vespucci v20's new introduction dialogue, which offers to enable satellite imagery.)

StreetComplete and Organic Maps don't support aerial imagery at all.

[fn:12] StreetComplete is only available on Android, although an iOS version is in the works. The OSM wiki for SC mentions that Go Maps!! supports StreetComplete quests, which may work as a workaround.

** openstreetmap.org
:PROPERTIES:
:CREATED:  2024-04-10T16:38:13+0530
:CUSTOM_ID: osm-website
:END:
The website is an important resource to mention, so users can see their edit history, find nearby mappers, see how their changeset comments are used, how to respond to changeset discussions, etc.

You could demonstrate it at the end of the survey by asking everyone to check out their changes on the website, while also reviewing their changes and discussing mistakes.

* Epilogue
:PROPERTIES:
:CREATED:  2024-04-10T15:11:35+0530
:CUSTOM_ID: epilogue
:END:
That's all I have for now. I hope your mapping parties succeed in growing OpenStreetMap's community and data coverage. Happy mapping [party organizing]!

#+ATTR_HTML: :class note
Someone shared this on Hacker News. It made it to the front page, and I was overwhelmed by the positive responses.
@@html: <br/>@@
[[https://news.ycombinator.com/item?id=40009997][Comments on HN]]

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+INCLUDE: includes.org::#footer :only-contents t
