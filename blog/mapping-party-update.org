#+TITLE: OSM Delhi Mapping Party Diaries
#+CREATED: 2024-06-17T11:32:10+0530

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+TOC: headlines 2

I haven't written about our mapping parties since party #3 in Greater Kailash 1. We have since tried to host a party each month. Individual parties seemed too trivial to write about, back then...and I also felt fairly drained after organizing each one, so writing about them felt like a chore. But somehow, after the 9th party, motivation hit me and here I am, writing about every party since the third.

* Party #4 - Khan Market (Sunday, 28th of January)
:PROPERTIES:
:CREATED:  2024-06-17T17:07:54+0530
:CUSTOM_ID: party-4
:END:
The fourth mapping party was held two days after the Republic Day. We arrived early to determine a place where we could sit and meet, but failed to find anything of the sort and had to make do with a park some distance away. Khan Market seemingly has no place to sit, unless it's inside a café or a restaurant. There were 13 participants, and we wandered around (roughly) as a single group.

I wasn't really satisfied with the amount of mapping done - a lot of time was lost in walking to and from the park to the market. (Sahil later floated the novel idea of meeting at a bus stop instead of a park.) Another issue was waiting for late arrivals, which we resolved to avoid in the future.

#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Photograph of six male participants, dressed in winter clothing, standing on the side of the street and smiling at the camera. One has hidden his face with a V-sign.
[[file:img/mapping-party-update/orig/zb2rhjDD1w3P1kWrd5oqG2akVboZQ5jx1A3zg91EKaBkSfy2J.jpg]]

#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Selfie of ten male participants, dressed in winter clothing, standing in a market corridor. One has hidden his face with a V-sign.
[[file:img/mapping-party-update/orig/IMG_20240128_163325.jpg]]

* Party #5 - Connaught Place (Sunday, 18th of February)
:PROPERTIES:
:CREATED:  2024-06-17T17:07:59+0530
:CUSTOM_ID: party-5
:END:
This was one of our smallest parties, with just 6 participants - 7 if we count Chirag, who only joined us after the mapping phase ended. [fn:1] We really had to drag Mohit over the phone to attend this one. 😅

#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Selfie of six male participants standing in the sun, in the central park of Rajiv Chowk. One has hidden his face with a V-sign.
[[file:img/mapping-party-update/orig/IMG_20240218_135607.jpg]]

For the first time, we divided ourselves into teams, assigned specific areas to each team, went out to map the assigned areas, and regrouped at a predetermined point at a predetermined time. It was super effective! This strategy resulted in a significant improvement in mapping productivity, and (unlike previous parties, where a number of people weren't really mapping anything) everyone was engaged in mapping. We were all happy to finally be approaching the mapping party productivity of the Bangalore community (whom we admire, and regard as friendly rivals and a source of inspiration).

#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Selfie of six male participants sitting at an outdoor table at the Indian Coffee House. One has hidden his face with a V-sign.
[[file:img/mapping-party-update/orig/IMG_20240218_153416.jpg]]

[fn:1] He was driving from Rohtak and was impeded by the police barricades set up to repress the farmer protests.

* DelhiFOSS - IIT Delhi (Sunday, 9th of March)
:PROPERTIES:
:CREATED:  2024-06-17T17:08:12+0530
:CUSTOM_ID: delhifoss
:END:
#+CAPTION: Getting a flex printed for the table, at Nehru Place. Photo by Ravi Dwivedi.
#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Photograph of a man holding up a large glossy printed sign depicting the Prav, OpenStreetMap, and F-Droid logos, each accompanied by the text "Convenient and private chat", "The libre wiki world map", and "The libre app store" respectively.
[[file:img/mapping-party-update/orig/zb2rhkH5dgPMJi9gbU378sK62ECrhd5hiswd1yLGd9QotpfCD.jpg]]

On the 9th of March, we took part in DelhiFOSS (at IIT Delhi) by jointly operating a table for OSM, [[https://prav.app/][Prav]], and [[https://f-droid.org/][F-Droid]]. Of all the people I met and informed about OSM that day at the OSM table, around 25 expressed an interest in attending our mapping parties and shared their phone numbers for it. I was excited at the prospect of having a mapping party of 20+ or even 30 people, and gushed over this success for many days...but the excitement later turned to disappointment, as most of them never attended our sessions (many of them have never answered my calls to date), and even fewer became regulars.

It wasn't all bad, though - we met a few people at the event who became regular participants, the most notable examples being Ujjawal and Krishna. Ujjawal quickly went from being a brand new contributor to a regular participant, an invested co-organizer, and a mapper active enough to rank among the best of us. [fn:2] Krishna is an enthusiastic mapper and provides valuable input for organizing parties.

I also held a little 40-minute OSM workshop at DelhiFOSS, which really ended up being a kind of talk. The original plan was to hold a 2 hour mapping party and improve coverage at IIT Delhi, but DelhiFOSS wasn't able to give us the requested time without any parallel events competing for time and attention. Near the end of the workshop, some of the participants actually went out and mapped (where they were assisted by Sahil, Saswata, Orendra, Aaru, Ravi, and others), while others stayed behind and mapped on their laptops.

#+CAPTION: The workshop/talk.
#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Photograph of a man with long hair giving a talk in a large conference hall in IIT Delhi. The presentation on the screen displays information about him. A smaller sign below the screen, affixed over some chalkboards, says "DelhiFOSS 2.0" and depicts various free software logos.
[[file:img/mapping-party-update/orig/zb2rhaKJybG1twAUvdHpmUb2FBn3Y4RebACB2Y3tVH1C16Zj6.jpg][file:img/mapping-party-update/small/zb2rhaKJybG1twAUvdHpmUb2FBn3Y4RebACB2Y3tVH1C16Zj6.jpg]]

#+ATTR_HTML: :loading lazy
#+ATTR_HTML: :alt Group photograph showing a group of roughly 40 DelhiFOSS participants and volunteers, standing on the steps of a building whose entrance says "LECTURE HALL COMPLEX". Seven are wearing the olive T-shirts of the volunteers, of which three are women. Two boys in the front row are holding up a sign saying "DelhiFOSS 2.0", which shows the Qutub Minar in the middle and various free software logos, including Firefox, Linux, MySQL, Python, Android, and PHP.
[[file:img/mapping-party-update/orig/gisjtxvx0aatha.jpeg]]

#+CAPTION: The OSM + Prav community, with Chirag of ILUGD (in the middle at the back)
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/gisjtxrwyaazukr.jpeg]]

[fn:2] He's even contributing to public transport.

* Party #6 - SFLC.in (Sunday, 17th of March)
:PROPERTIES:
:CREATED:  2024-06-17T17:08:21+0530
:CUSTOM_ID: party-6
:END:
The next party was held indoors at the Software Freedom Law Center India on the 17th of March. We had planned to focus on iD and remote mapping. I am no iD expert (I do the overwhelming majority of my work in [[https://vespucci.io/][Vespucci]] and the remainder in [[https://josm.openstreetmap.de/][JOSM]]), so I took the lazy route and left the task of teaching to Sahil.

#+CAPTION: Teaching iD at SFLC.in
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/2024-03-17_iD_mapping_party.jpg][file:img/mapping-party-update/small/2024-03-17_iD_mapping_party.jpg]]

There were 15 participants, although 2 didn't really participate. I'm not sure how much mapping was done.

Before the party, I debated whether to get my laptop along or not, since it was heavy and I would mostly be mentoring and not mapping myself. Sahil suggested I get it along anyway, and it turned out to be very useful. Tejaswini from SFLC.in was present and I encouraged her to take a shot at OSM editing. She had to leave early, but managed to get in some changes. Orendra and Surbhi turned up (very) late, and apparently did not know that they needed laptops today. Once again, I was able to lend them my laptop.

#+CAPTION: Photo by Aaru Swartz.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/zb2rhZEQDr25zfaUBZy9zJW86WZTSEU5suQU64csaiR6zmF7W.jpg][file:img/mapping-party-update/small/zb2rhZEQDr25zfaUBZy9zJW86WZTSEU5suQU64csaiR6zmF7W.jpg]]

#+ATTR_HTML: :loading lazy :style "transform:rotate(90deg);"
[[file:img/mapping-party-update/orig/IMG_20240317_162928.jpg][file:img/mapping-party-update/small/IMG_20240317_162928.jpg]]

Afterwards, we went out to eat, and then I traveled with Aaru and Saurav to visit their room in Laxmi Nagar.

* Party #7 - Satya Niketan (Saturday, 13th of April)
:PROPERTIES:
:CREATED:  2024-06-17T17:08:36+0530
:CUSTOM_ID: party-7
:END:

** Date and time
:PROPERTIES:
:CREATED:  2024-06-19T16:36:46+0530
:CUSTOM_ID: date-time
:END:
It was beginning to get hot during the day, and our usual time of 12 to 2 would not do. We decided to hold this one in the evening, from 5 to 7.

Some participants were only available on Saturday and I wanted to accommodate them, but Sahil and Saswata - who had been involved in most if not all parties to this point - had a 6-day work week and thus were not free on Saturdays. [fn:3] At Sahil's suggestion, I decided to do without them this time and chose Saturday. (Starting with the next party, I would adopt a different strategy to work around this issue.)

[fn:3] They both work at [[https://unmukti.in/][Unmukti]], and have to travel from Gurgaon to attend the parties.

** Venue
:PROPERTIES:
:CREATED:  2024-06-19T16:37:04+0530
:CUSTOM_ID: venue
:END:
I made a long list of popular markets in Delhi which were easily accessible via Metro. Then I made a poll for a few of these markets. It was a tie between Lajpat Nagar and Satya Niketan.

I surveyed both places to map parks and the seating infrastructure. The park needed to be close to the Metro and have the infrastructure to seat 15+ people in one spot. (If not, there's always the fallback option of sitting in the grass.)

Satya Niketan had a [[https://www.openstreetmap.org/way/1267919232][large park nearby]] which was entirely missing on OSM when I went there; it had gazebos with benches, suitable for a large group to sit in.

#+CAPTION: Before. Screenshot of Organic Maps on the desktop, showing the missing park as a large blank spot in Satya Niketan.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/Organic Maps desktop 1-1 park 20240620_09:42:25.png]]

#+CAPTION: After. Screenshot of Organic Maps on the desktop, showing the newly-added park, two gazebos within it, paths, benches, entrances, a playground, and a fitness station.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/Organic Maps desktop 1-2 park 20240620_09:41:43.png]]

Lajpat Nagar presented some challenges - there was no publicly-accessible park with sufficient seating. Some parks were lacking infrastructure and had barren grounds. Others were locked, or had a guard keeping the general public out. [[https://www.openstreetmap.org/way/651977789][Lala Lajpat Rai Memorial Park]] was public and met the seating requirements, but it was off to a side of the Lajpat Nagar market. If we assigned a team to the other end of the market (near the Lajpat Nagar Metro station), it would take them 20 minutes to get there and 20 more minutes to get back, leaving them just 20 minutes to actually map.

I opted for Satya Niketan. It was the logistically safer choice - it was a smaller place, which I figured would be beneficial if the turnout wasn't big (which is what I expected). The smaller area also meant it would be easier for a small number of mentors to reach any team quickly, in case of trouble.

To assist the participants, I spent around 2 hours in JOSM, working on Satya Niketan's buildings, with the help of the building terracer plugin.

#+CAPTION: Before. Screenshot of the Standard Layer, showing roughly- and unevenly-drawn buildings on the map of Satya Niketan.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/osm-carto 1 2024-08-09 20-22.svg]]

#+CAPTION: After. Screenshot of the Standard Layer, showing accurately-traced and even-looking buildings on the map of Satya Niketan. Some missing streets have been added, and the streets look less crooked.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/osm-carto 2 2024-08-09 22-09 building terracing.svg]]

** Too much, too little
:PROPERTIES:
:CREATED:  2024-06-19T16:37:19+0530
:CUSTOM_ID: too-much-too-little
:END:
As the day of the party approached, we registered a record number of expected participants - around 25 people had told me over the phone that they would attend. I was concerned that Satya Niketan may not have enough data for everyone to map.

Then there was the question of mentors. When surveying in teams, each team should be small - the more the people, the greater the coordination needed between them, and the greater the scope for duplication and edit conflicts. With Every Door, we could have a team of at most 5 people, with 1-2 working on buildings, one on micromapping, and 1-2 in amenities mode. And each team should have at least one mentor to answer questions and point out errors.

With Sahil and Saswata not available, we were already down by two mentors. Aaru had moved away to his hometown in Tamil Nadu, and wouldn't be available. Ujjawal was an enthusiastic learner and mapper at this point, but still too new to serve as a mentor. Surbhi and Orendra (another two regular participants) informed me a few days before the party that the timing was too late in the day for them and the venue too far away from their place, so they wouldn't be coming.

In short, Ravi and I were the only available mentors I could truly rely on, and we had 25 expected participants. I was nervous about how we would make this work.

** D-Day
:PROPERTIES:
:CREATED:  2024-06-19T16:37:33+0530
:CUSTOM_ID: d-day
:END:
An hour before the mapping party was to start, the sky became overcast and there was every indication that it would rain. People began calling me to know if the plan was still on.

Would we have to throw away all our preparations because of the rain? I had no time to think. In the heat of the moment, I said the first thing that came to my mind.

"Yes, the plan is very much on. Let's meet at the Metro station and see what happens."

I was late by about 15 minutes on account of the rain, and Ravi had to keep everyone together until I got there. In contrast to the 25 anticipated participants, there were only around 12 or 13 of us. We stood inside the Metro station building, and I introduced OSM and Organic Maps amidst the rain and the howling wind.

By the time I finished introducing Every Door, the wind had died down somewhat, although it was still raining. Ravi said I should give another demonstration of adding things in Every Door before the participants could try, so we walked over to the partly-covered skywalk, right across which was the market with its famous lineup of restaurants.

The skywalk's roof had gaps in it (by design), so there were patches where the rain still got in, but there were plenty of covered dry areas too. I demonstrated editing with Every Door again, and the participants started trying it out themselves.

It got a bit messy here. Some couldn't think of what to map. Others, I discovered later, had created duplicates. I should have planned out this activity in more detail, and assigned everyone to cover a building, the micromapping features outside it, and the shops inside it.

As usual, I went among everyone to provide guidance. Suresh wanted to learn the Go Map!! editor and I helped him as best I could (since I don't use an iPhone and don't have much experience with Go Map!!).

As everyone mapped from the skywalk, the rain died down. It was 6 PM - we still had an hour. It looked like we would actually be able to do the mapping as planned.

We made 4 teams of threes and fours. I showed them a map with the survey areas I had marked, and asked each team to choose an area.

#+CAPTION: Screenshot of the map of Satya Niketan using the Standard Layer, with differently-colored rounded rectangles showing the division of survey areas.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/2024-08-09_22-09_Satya_Niketan_8_teams.png]]

Once again, the team-based mapping strategy proved itself. Each team made very significant data additions to their area. The overall difference was night and day. It was beyond what any lone mapper - no matter how experienced - could do in the same time.

More than anything, I was amazed that we actually managed to hold a productive mapping party in spite of the rain.

#+CAPTION: Screenshot of the map of Satya Niketan using the Standard Layer, showing its state before the mapping party. The buildings are cleanly traced, but there are no addresses and only a handful of points of interest (around 25 in the entire neighbourhood).
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/osm-carto 2 2024-08-09 22-09 building terracing.svg]]

#+CAPTION: Screenshot of the map of Satya Niketan using the Standard Layer, showing its state after the mapping party. There is a drastic increase in the number of mapped shops and amenities - there are now 38 shops and amenities mapped on the main road, 24 in one street, and 17 in another street. There are 24 addresses mapped in a row along one street, 10 addresses mapped sporadically in the center block, and 14 addresses mapped along the southeast edge of the map.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/osm-carto 3 2024-04-14 01-39 mapping party.svg]]

Some of the new participants weren't really sold on OSM, and didn't really participate. (I tried to encourage them to voice their issues, but they did not speak up.) They left early. But everyone else was fairly enthusiastic.

This was our first time reaching out to r/Delhi on Reddit, and we got 3 registrations there.

I also learned that two participants were, in the past, active contributors to Google Maps...from them I learned about Google's contributor ranking system, and the incentives Google used to give to contributors (e.g. cloud storage space). This was useful insight, since I had zero experience in contributing to Google Maps.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240413_193104.jpg][file:img/mapping-party-update/small/IMG_20240413_193104.jpg]]

* Informal mini-party - Lajpat Nagar
:PROPERTIES:
:CREATED:  2024-06-17T17:32:51+0530
:CUSTOM_ID: informal-mini-party-lajpat-nagar
:END:
The next day, Ujjawal and I visited Satya Niketan once again to add more data. Such spontaneous mapping sessions were a unique experience for me. I've been mapping since 2016, but until last year I had never met any other mappers. And the serious mappers I've met in the past year all live too far away and are too busy to meet casually. Ujjawal seemingly gets a lot of time off from college, and I'm a freelancer (for better or worse), which is how this was able to come about.

#+CAPTION: Screenshot of the map of Satya Niketan using the Standard Layer, showing its state after the mapping party. Notice the gaps in the building rows in the southeast corner - this happened due to some buildings being mistagging by a new mapper.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/osm-carto 3 2024-04-14 01-39 mapping party.svg]]

#+CAPTION: Screenshot of the map of Satya Niketan using the Standard Layer, showing its state after the survey by Ujjawal and I on the next day. 8 buildings and many shops and amenities have been added - there are now 33 shops/amenities near the market square. Address coverage in the southeast corner is now nearly complete, with 3 rows completed and 3 rows sporadically mapped. The mistagged buildings and the gaps caused by them are now fixed. In the neighbouring Arakpur Bagh outside the southeast corner of Satya Niketan, I added buildings, streets, and 6 points of interest.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/osm-carto 4 2024-04-15 22-33.svg]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/Organic Maps desktop 7-1 20240621_00:58:51.png]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/Organic Maps desktop 7-2 20240621_00:58:14.png]]

Later in the week, we decided to meet up somewhere to map. We chose Lajpat Nagar as the venue, as it was easily accessible for the ones most likely to turn up. I hadn't planned on expending too much effort in inviting people over phone, but ended up calling a significant number of people regardless. I focused on the people who weren't able to make it to the previous party. We ended up with four participants, myself included.

Ujjawal worked on buildings in Vespucci, while Surbhi and Ishika used Every Door to add the shops. After an hour, we had covered one street. It may seem small, but it was significant work for 3 mappers, and it really drove home how productive a larger group can be.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240427_180138.jpg][file:img/mapping-party-update/small/IMG_20240427_180138.jpg]]

* Party #8 - the mapping weekend
:PROPERTIES:
:CREATED:  2024-06-17T12:17:48+0530
:CUSTOM_ID: party-8
:END:
This was the first party where we experimented with hosting two sessions (one on Saturday and one on Sunday) to allow people who cannot attend on one day to attend on the other. I'm confident that it increased the total number of people attending, and it also allows us to iterate on the teaching process more quickly...at the cost of making the individual sessions smaller.

Earlier, our mapping parties were held in relatively posh areas. While this was done in hope of providing a safe and comfortable experience to newcomers, it meant that poorer areas of the city had worse data. Having two sessions also allowed us to host parties in more diverse areas of the city, achieve more equitable coverage, and encourage more people to attend by choosing a location closer to them.

The initial idea was to host a "general" and an "advanced" party, with the latter being held in poorer areas and focusing on advanced techniques. But once I actually started writing the OSM Calendar entries, I realized that this distinction wasn't necessary - we could just host 2 sessions which were both open to all.

We chose Laxmi Nagar and Nehru Place as our venues.

** Mapping parties during heat waves
:PROPERTIES:
:CREATED:  2024-06-17T12:17:46+0530
:CUSTOM_ID: heat-wave
:END:
India has been [[https://en.wikipedia.org/wiki/2024_Indian_heat_wave][experiencing a heat wave]]. It began during April, and even at the time of writing (the third week of June), the maximum temperature in Delhi regularly exceeds 45°C. Even at night, the temperature lingers around the 35°C mark. There's no longer such a thing as a refreshing cool night breeze...just the hot wind baking you alive during the day, and a somewhat less hot wind failing to provide any respite at night.

This is too hot for outdoor surveys. We tried to work around this by hosting the Laxmi Nagar session early in the day, from 8 to 10 [fn:4] in an area with narrow and shaded streets, and having the Nehru Place session in the evening from 4 to 6.

[fn:4] Something like 7 to 9 would have been better, which the OSM Bangalore community has tried for its own mapping parties, but people in Delhi protest having to get up that early.

** Session 1 - Laxmi Nagar
:PROPERTIES:
:CREATED:  2024-06-20T00:35:58+0530
:CUSTOM_ID: session-1-laxmi-nagar
:END:
A week before the party, I went to scout out Laxmi Nagar in order to find nearby parks, how long it would take to reach them from the Metro station, and to see just how hot it would be in the morning. There were two parks nearby, and they seemed sufficient. The weather was tolerable by Indian summer standards - cool in the shade, and warm in the sun.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/2-1 2024-05-13 bounding box NE.png]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/2-2 2024-05-14 bounding box NE.png]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/4-1 2024-05-13 bounding box SW.png]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/4-2 2024-05-14 bounding box SW.png]]

I had already scouted out Nehru Place. The first park I tried was [[https://www.openstreetmap.org/way/80511825][Asaf Ali Park]], which had excellent infrastructure for seating large groups of people in gazebos, and even had public toilets...but the entrance connecting it to Nehru Place had been sealed off. That made it a fairly long walk from the Nehru Place Metro station, and therefore unviable.

That left us with [[https://www.openstreetmap.org/way/479658972][Astha Kunj]], which fortunately turned out to have a gazebo quite close to the Metro station.

Last time, I mapped buildings in the survey area to aid Every Door users. This was a task I could delegate, lightening my workload as well as letting remote mappers (especially those who won't be able to attend) indirectly participate in the session.

I reached out to online Indian OSM communities, encouraging them to map buildings along Vikas Marg in Laxmi Nagar. (The buildings in Nehru Place were fewer in number, and had long been mapped.) There wasn't much (or any) response to it - fortunately, Ujjawal expressed an interest in doing it, and mapped a large number of buildings.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/5-1 Akbar-birbal building tracing 1.svg]]

#+CAPTION: Buildings mapped by Ujjawal (1/2).
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/5-2 Akbar-birbal building tracing 1.svg]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/6-1 Akbar-birbal building tracing 2.svg]]

#+CAPTION: Buildings mapped by Ujjawal (2/2).
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/6-2 Akbar-birbal building tracing 2.svg]]

For the Laxmi Nagar session, I asked everyone to meet outside the Metro station's gate #2. We would then walk to the park, meet up, and then walk back to Vikas Marg to start mapping. The idea was to ensure everyone's safety by not requiring anyone to travel alone on foot, especially female participants.

Unfortunately, this did not work out. For one, the lone female participant still ended up having to walk alone to the park. For another, it cost us a lot of time, first in waiting for everyone to arrive at the Metro station (plenty of people were late), and second in moving to the park and then going back to Vikas Marg.

Consequently, we spent no more than 30 minutes on the actual team-based mapping, and couldn't prolong it, not least because the heat was worsening as time passed. We did not have our usual level of productivity.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/7-1 mapping party park.svg]]

#+CAPTION: Changes made by the party as a whole after we exited the park.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/7-2 mapping party park.svg]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/8-1 mapping party SE team.svg]]

#+CAPTION: Changes made by the south-east team.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/8-2 mapping party SE team.svg]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/9-1 mapping party NW team.svg]]

#+CAPTION: Changes made by the north-west team.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/9-2 mapping party NW team.svg]]

#+CAPTION: Photo by participant Mythili Srinivasamurthy, who did not wish to be photographed.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240518_103023.jpg][file:img/mapping-party-update/small/IMG_20240518_103023.jpg]]

** Session 2 - Nehru Place
:PROPERTIES:
:CREATED:  2024-06-20T09:31:03+0530
:CUSTOM_ID: session-2-nehru-place
:END:
It was 4 PM, and the weather was ridiculously hot - it may as well have been noon. I had expected this, but my hands were tied - if I held the party later, some of the regular participants would have faced difficulties in getting back home. I was banking on the temperature being somewhat tolerable in the shade of the park gazebo, and afterwards in the shade of the Nehru Place office buildings. But in none of these places was the onslaught of the heat noticeably diminished.

During the Laxmi Nagar session I noticed some participants getting restless and distracted. Later at home, I thought about it and realized that experienced participants already knew about OSM and Organic Maps - there was no need to make them sit through that again.

That gave me an idea which we tried today - having separate, parallel tracks for newcomers and returning participants. After the meet and greet (involving everyone), one mentor would introduce newcomers to OSM, Organic Maps, and a simple editor like Every Door, while another mentor would teach returning participants about other ways of using OSM (e.g. OsmAnd) and editing it (e.g. StreetComplete, Vespucci, etc).

Thus, Ravi mentored the group unfamiliar with Organic Maps and Every Door - it was just two people, so they headed out to map before the rest of us. (I sent them the map of survey areas over XMPP and assigned a zone to them.) I and Ujjawal mentored the other four in the use of OsmAnd and StreetComplete.

#+CAPTION: Map of Nehru Place as rendered by map-machine, with differently-colored rounded rectangles (drawn using Inkscape) showing the division of survey areas.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/2024-05-18 teams.svg]]

As with the Laxmi Nagar session, this too didn't go as smoothly as I had hoped - albeit for different reasons. I've only ever used the F-Droid version of OsmAnd, but asked everyone to install it from the Play Store/App Store. This way, we could avoid the step of installing yet another application (F-Droid) and also accommodate the lone iOS user.

I was under the impression that the demo version of OsmAnd unlocked the full version features (especially OsmAnd Live, one of the killer features of not just OsmAnd but OSM itself) for those who contributed 30 changes to OSM...but that wasn't the case here, as participants with well over 30 OSM changes authenticated themselves and still couldn't enable OsmAnd Live. It's rather embarrassing to be teaching a session and to publicly discover that you don't know what you thought you did...to say nothing of the participants' time wasted in awkward fumbling around. Guess I'll be recommending the F-Droid version in the future instead.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240519_190107.jpg][file:img/mapping-party-update/small/IMG_20240519_190107.jpg]]

After we wrapped up the introduction to OsmAnd and StreetComplete, we set out to begin mapping. My team included Anirudh and Ishika. Anirudh had become one of our most regular participants (and one of the few who attended both the Laxmi Nagar session yesterday and the Nehru Place session today) and was quite adept at mapping. As for Ishika, while she had only attended one mapping party before, she demonstrated a passion for contributing. Which is to say, I was confident enough in their skills and was thus able to do some mapping myself.

It was only once we stepped out after having some drinks (mojitos, sodas, lassi, and the like) at an air-conditioned restaurant and were preparing to depart did the weather seem somewhat hospitable again. Still hot and sticky, but survivable.

* The 9th mapping party
:PROPERTIES:
:CREATED:  2024-06-17T20:21:36+0530
:CUSTOM_ID: party-9
:END:
Due to the heat wave, we decided to hold our latest mapping party indoors. We would meet up at SFLC.in one day, and at the Internet Freedom Foundation the other. I had never been to IFF before, so I went in advance to check out the space, the seating, and the connectivity.

The heat was such that I questioned the wisdom of holding mapping parties in such situations. Open map data is probably low on the list of priorities if everyone's struggling to survive...although it's probably a good idea to teach people to use maps, and to perhaps map features that can aid survival -

1. water taps, drinking water dispensers, and fast food places which serve drinks;
2. benches, shelters, trees, tree lines, parks, and woods;
3. air-conditioned amenities, medical facilities, etc.

Besides, if you want to get to your destination faster to avoid the heat, a detailed and accurate map is probably useful...

** Party? Mapathon? Meetup?
:PROPERTIES:
:CREATED:  2024-06-17T12:20:12+0530
:CUSTOM_ID: party-mapathon-meetup
:END:
We had been using the term "mapping party" so far. People who aren't aware of OSM seem to regard the term "party" with suspicion, and we've had to constantly explain what it entails. Some alternatives we considered were -

+ "Mapathon". This has good associations because of the popularity of "hackathon", but it also sounded like a test of patience and endurance, and something for experts, i.e. not suitable for newcomers.

+ "Mapping session" or "mapping workshop" sounded too corporate, as somebody pointed out

This time, we bit the bullet and started calling it a "mapping meetup". Hopefully, it conveyed both the social and the mapping aspects of the event.

More possibilities include "mapping weekend" (as long as we stick to our weekend format), and "mapping potluck" for indoor events.

** Preparations
:PROPERTIES:
:CREATED:  2024-07-25T01:56:25+0530
:END:
One of my failures this time was that I did not check in with the regulars before I announced the dates. Many were out of town on both days, and I feared that it would be another of those parties with really low participation. Another lesson learned.

In the week leading up to the parties, I fretted and fussed over the teaching plan - it's not often that I get to teach iD, and I don't use it all that often either. Eventually I arrived at a structure I was satisfied with.

No mapping party is complete without a meal afterwards. Since I've been to SFLC.in a number of times - once for Software Freedom Day last year, once again for our first remote mapping party - I knew that there weren't too many good places to eat nearby. And the market with the eateries was about 10 minutes away on foot, which I wanted to avoid in this weather.

It occurred to me that I could make something for everyone myself. Like any office, both venues had microwaves and plates I could use to heat it up and serve it. I made penne arrabbiata with mushrooms and capsicum.

Some of our new participants found us via SFLC.in's post about the event on LinkedIn. One came from [[https://fsci.org.in/][FSCI]], and another one I found on osm.org as I was inspecting the edits of the Laxmi Nagar party.

** Session 1 - Software Freedom Law Center, India
:PROPERTIES:
:CREATED:  2024-07-09T00:28:16+0530
:END:

Mohit said that we should call it a "mapping potluck" 😄

#+CAPTION: Photo by Suraj.
#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240615_141533.jpg][file:img/mapping-party-update/small/IMG_20240615_141533.jpg]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240615_141649.jpg][file:img/mapping-party-update/small/IMG_20240615_141649.jpg]]

** Session 2 - Internet Freedom Foundation
:PROPERTIES:
:CREATED:  2024-07-09T00:27:58+0530
:END:
SFLC.in party - mostly newbies. IFF party - mostly experienced mappers.

Never really analyzed the data for either session, so I can't really say how productive they were.

I noticed that some of the more experienced mappers were just off doing their own thing in isolation. That's something they could do on their own, too (and they probably already do so regularly)...so was there something unique the mapping party could provide to them? Something that involves working on a common goal? Improvising, I asked them if they'd like to work on a task on the HOT Tasking Manager, and they readily agreed.

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240616_182931.jpg][file:img/mapping-party-update/small/IMG_20240616_182931.jpg]]

#+ATTR_HTML: :loading lazy
[[file:img/mapping-party-update/orig/IMG_20240616_183009_emoji.jpg]]

* Future ideas
:PROPERTIES:
:CUSTOM_ID: future-ideas
:END:
collaborative task(s) for experienced contributors. HOT Tasking Manager, or an area we select (I can suggest a few in Delhi and the NCR; I'm sure the experienced mappers can come up with one too)

Improving the social aspect, so everyone gets to interact with everyone. For indoor sessions, that might mean rotating people so everyone gets to sit with everyone. For outdoor sessions (here's hoping that the heat dies down and we have them again soon), it may be implemented by holding two mapping phases (separated by a refreshment break) in which we switch up the teams

In both indoor and outdoor parties, we may benefit from increasing the time we set, and announcing the plan for eating out, so people can plan their day accordingly and not have to leave in a hurry

indoor sessions - mapping potluck?

Reaching out to OSM contributors. Is it better to approach long-time mappers, or new ones?

OSM Delhi website and organization. Maybe apply for some grants, get paid for all this unpaid work I'm doing, make it something that can continue after me.

We're still a long way off from the community required to map Delhi completely - we need to expand. We need to reach out to more people, so we can map out larger chunks of the city at a time. We need to hold more parties each month, and hold them in new areas - Dwarka, Gurgaon, Indirapuram, Meerut, Rohtak, Ghaziabad, Faridabad, and more. I can't do that alone - others have to step up and start organizing in their areas.

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+INCLUDE: includes.org::#footer :only-contents t
