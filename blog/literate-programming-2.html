<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-07-31 Wed 18:24 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Literate Programming 2</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">Literate Programming 2</h1>
<p class="subtitle" role="doc-subtitle">Jumping from compiler errors to the literate program</p>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="keyboard-machinations-kmonad.html">← Previous: Keyboard machinations with Kmonad</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="chronometrist-past-present-future.html">→ Next: Chronometrist - past, present, and future</a></div>
 </nav>
</p>
<section id="outline-container-the-problem" class="outline-2">
<h2 id="the-problem"><a href="#the-problem">The problem</a></h2>
<div class="outline-text-2" id="text-the-problem">
<p>
Most compilers and linters are not designed to operate on literate programs. <label id='fnr.1' for='fnr-in.1.1716693' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.1716693' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
The mere act of writing this line helped me realize that there's another solution to this problem, at least for literate Emacs Lisp programs&#x2026;it's called <code>literate-elisp-byte-compile-file</code> from the <code>literate-elisp</code> package, and it seems to be made to address this very problem.
</span> I have a <a href="https://tildegit.org/contrapunctus/dotemacs/src/branch/production/Makefile">Makefile</a> to tangle an Org LP and compile the resulting tangled sources, but if you use <code>M-x compile</code> to run <code>make</code>, compiler warnings and errors taking you to the tangled sources instead of the LP.
</p>
</div>
</section>
<section id="outline-container-the-basic-solution" class="outline-2">
<h2 id="the-basic-solution"><a href="#the-basic-solution">The basic solution</a></h2>
<div class="outline-text-2" id="text-the-basic-solution">
<p>
Org has a source block header argument called <code>:comments</code> - if it is set to <code>yes</code> or <code>link</code>, Org creates comments in the tangled file, delineating each source block. The comments contain links (in Org syntax) to the original LP. The command <code>org-babel-tangle-jump-to-org</code> can use these links to jump from the tangled source to the LP.
</p>

<p>
That sounds like what we're after.
</p>

<p>
The command for jumping to the error is called <code>compile-goto-error</code>, so my first approach was to create <code>:after</code> advice for it.
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #fb2874;">(</span><span style="color: #fb2874;">defun</span> <span style="color: #b6e63e;">my-org-lp-goto-error</span> <span style="color: #fd971f;">(</span><span style="color: #66d9ef;">&amp;rest</span> args<span style="color: #fd971f;">)</span>
  <span style="color: #fd971f;">(</span>org-babel-tangle-jump-to-org<span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>

<span style="color: #fb2874;">(</span>advice-add 'compile-goto-error <span style="color: #fd971f;">:after</span> #'my-org-lp-goto-error<span style="color: #fb2874;">)</span>
<span style="color: #555556;">;; </span><span style="color: #555556;">useful to keep handy during testing, or if something goes wrong</span>
<span style="color: #555556;">;; </span><span style="color: #555556;">(advice-remove 'compile-goto-error #'my-org-lp-goto-error)</span>
</pre>
</div>
</div>
</section>
<section id="outline-container-refinement-1" class="outline-2">
<h2 id="refinement-1"><a href="#refinement-1">Refinement 1 - preserving window configuration</a></h2>
<div class="outline-text-2" id="text-refinement-1">
<p>
The above works, but you'll find that each time you jump to an error, your window configuration changes. The solution was rather simple, but it took me some time to realize it.
</p>

<p>
We want to wrap <code>compile-goto-error</code> and <code>org-babel-tangle-jump-to-org</code> in a <code>save-window-excursion</code> (which saves and restores the window configuration), so we switch from <code>:after</code> to <code>:around</code> advice. Note that the function signature changes accordingly.
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #fb2874;">(</span><span style="color: #fb2874;">defun</span> <span style="color: #b6e63e;">my-org-lp-goto-error</span> <span style="color: #fd971f;">(</span>oldfn <span style="color: #66d9ef;">&amp;rest</span> args<span style="color: #fd971f;">)</span>
  <span style="color: #fd971f;">(</span><span style="color: #fb2874;">let</span> <span style="color: #b6e63e;">(</span>buffer position<span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span><span style="color: #fb2874;">save-window-excursion</span>
      <span style="color: #66d9ef;">(</span>funcall oldfn<span style="color: #66d9ef;">)</span>
      <span style="color: #66d9ef;">(</span>org-babel-tangle-jump-to-org<span style="color: #66d9ef;">)</span>
      <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">setq</span> buffer   <span style="color: #fb2874;">(</span>current-buffer<span style="color: #fb2874;">)</span>
            position <span style="color: #fb2874;">(</span>point<span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span>select-window <span style="color: #66d9ef;">(</span>get-buffer-window buffer<span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span>goto-char position<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>

<span style="color: #fb2874;">(</span>advice-add 'compile-goto-error <span style="color: #fd971f;">:around</span> #'my-org-lp-goto-error<span style="color: #fb2874;">)</span>
</pre>
</div>
</div>
</section>
<section id="outline-container-refinement-2" class="outline-2">
<h2 id="refinement-2"><a href="#refinement-2">Refinement 2 - when the Org LP buffer is not visible</a></h2>
<div class="outline-text-2" id="text-refinement-2">
<p>
The above works fine when the Org literate program buffer is visible - the window configuration is restored - but if it isn't, we get an error. Let's sort that out.
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #fb2874;">(</span><span style="color: #fb2874;">defun</span> <span style="color: #b6e63e;">my-org-lp-goto-error</span> <span style="color: #fd971f;">(</span>oldfn <span style="color: #66d9ef;">&amp;rest</span> args<span style="color: #fd971f;">)</span>
  <span style="color: #fd971f;">(</span><span style="color: #fb2874;">let</span> <span style="color: #b6e63e;">(</span>buffer position<span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span><span style="color: #fb2874;">save-window-excursion</span>
      <span style="color: #66d9ef;">(</span>funcall oldfn<span style="color: #66d9ef;">)</span>
      <span style="color: #66d9ef;">(</span>org-babel-tangle-jump-to-org<span style="color: #66d9ef;">)</span>
      <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">setq</span> buffer   <span style="color: #fb2874;">(</span>current-buffer<span style="color: #fb2874;">)</span>
            position <span style="color: #fb2874;">(</span>point<span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span><span style="color: #fb2874;">let</span> <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">(</span>org-window <span style="color: #fd971f;">(</span>get-buffer-window buffer<span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span>
      <span style="color: #555556;">;; </span><span style="color: #555556;">if the Org buffer is visible, switch to its window</span>
      <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">if</span> <span style="color: #fb2874;">(</span>window-live-p org-window<span style="color: #fb2874;">)</span>
          <span style="color: #fb2874;">(</span>select-window org-window<span style="color: #fb2874;">)</span>
        <span style="color: #fb2874;">(</span>switch-to-buffer buffer<span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span>goto-char position<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>

<span style="color: #fb2874;">(</span>advice-add 'compile-goto-error <span style="color: #fd971f;">:around</span> #'my-org-lp-goto-error<span style="color: #fb2874;">)</span>
</pre>
</div>
</div>
</section>
<section id="outline-container-refinement-3" class="outline-2">
<h2 id="refinement-3"><a href="#refinement-3">Refinement 3 - interop with <code>literate-elisp-byte-compile-file</code></a></h2>
<div class="outline-text-2" id="text-refinement-3">
<p>
The latest iteration is correct in itself. However, the link generation and/or <code>org-babel-tangle-jump-to-org</code> itself have some bugs - the latter sometimes places me in parts of the Org LP buffer which are wildily different from the position in the tangled source.
</p>

<p>
Till a time that the Org bugs are fixed, I can use <code>literate-elisp-byte-compile-file</code>. Since that, too, uses <code>compilation-mode</code> and <code>compile-goto-error</code>, we patch our advice to handle cases where <code>compile-goto-error</code> leads to an Org LP. We could remove the advice, but unlike <code>literate-elisp</code> it has the advantanges of working with non-Elisp LPs as well as with multiple files at a time.
</p>

<p>
The code is beginning to look a little ugly—there's probably a clearer way to write it. But this is where I'm calling it a day for the time being.
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #fb2874;">(</span><span style="color: #fb2874;">defun</span> <span style="color: #b6e63e;">my-org-lp-goto-error</span> <span style="color: #fd971f;">(</span>oldfn <span style="color: #66d9ef;">&amp;rest</span> _args<span style="color: #fd971f;">)</span>
  <span style="color: #fd971f;">(</span><span style="color: #fb2874;">let</span> <span style="color: #b6e63e;">(</span>buffer position tangled-file-exists-p<span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span><span style="color: #fb2874;">save-window-excursion</span>
      <span style="color: #66d9ef;">(</span>funcall oldfn<span style="color: #66d9ef;">)</span>
      <span style="color: #555556;">;; </span><span style="color: #555556;">`</span><span style="color: #fd971f;">compile-goto-error</span><span style="color: #555556;">' might be called from the output of</span>
      <span style="color: #555556;">;; </span><span style="color: #555556;">`</span><span style="color: #fd971f;">literate-elisp-byte-compile-file</span><span style="color: #555556;">', which means</span>
      <span style="color: #555556;">;; </span><span style="color: #555556;">`</span><span style="color: #fd971f;">org-babel-tangle-jump-to-org</span><span style="color: #555556;">' would error</span>
      <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">when</span> <span style="color: #fb2874;">(</span><span style="color: #fb2874;">ignore-errors</span> <span style="color: #fd971f;">(</span>org-babel-tangle-jump-to-org<span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>
        <span style="color: #fb2874;">(</span><span style="color: #fb2874;">setq</span> buffer         <span style="color: #fd971f;">(</span>current-buffer<span style="color: #fd971f;">)</span>
              position       <span style="color: #fd971f;">(</span>point<span style="color: #fd971f;">)</span>
              tangled-file-exists-p t<span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span>
    <span style="color: #555556;">;; </span><span style="color: #555556;">back to where we started - the `</span><span style="color: #fd971f;">compilation-mode</span><span style="color: #555556;">' buffer</span>
    <span style="color: #b6e63e;">(</span><span style="color: #fb2874;">if</span> tangled-file-exists-p
        <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">let</span> <span style="color: #fb2874;">(</span><span style="color: #fd971f;">(</span>org-window <span style="color: #b6e63e;">(</span>get-buffer-window buffer<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>
          <span style="color: #555556;">;; </span><span style="color: #555556;">if the Org buffer is visible, switch to its window</span>
          <span style="color: #fb2874;">(</span><span style="color: #fb2874;">if</span> <span style="color: #fd971f;">(</span>window-live-p org-window<span style="color: #fd971f;">)</span>
              <span style="color: #fd971f;">(</span>select-window org-window<span style="color: #fd971f;">)</span>
            <span style="color: #fd971f;">(</span>switch-to-buffer buffer<span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>
          <span style="color: #fb2874;">(</span>goto-char position<span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span>
      <span style="color: #66d9ef;">(</span>funcall oldfn<span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>

<span style="color: #fb2874;">(</span>advice-add 'compile-goto-error <span style="color: #fd971f;">:around</span> #'my-org-lp-goto-error<span style="color: #fb2874;">)</span>
</pre>
</div>
</div>
</section>
<section id="outline-container-bonus" class="outline-2">
<h2 id="bonus"><a href="#bonus">Bonus - preserving column, and temporary disable via prefix argument</a></h2>
<div class="outline-text-2" id="text-bonus">
<p>
I noticed that <code>org-babel-tangle-jump-to-org</code> does not preserve the column position for me. We save the column when we're in the tangled source and restore that later. Also, I sometimes want to temporarily disable the advice so I can see where <code>compile-goto-error</code> itself takes us - so I add support for a prefix argument. Calling <code>compile-goto-error</code> with a prefix argument will now skip the rest of the code, as though there was no advice.
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #fb2874;">(</span><span style="color: #fb2874;">defun</span> <span style="color: #b6e63e;">my-org-lp-goto-error</span> <span style="color: #fd971f;">(</span>oldfn <span style="color: #66d9ef;">&amp;optional</span> prefix <span style="color: #66d9ef;">&amp;rest</span> args<span style="color: #fd971f;">)</span>
  <span style="color: #7f7f80;">"Make `</span><span style="color: #fd971f;">compile-goto-error</span><span style="color: #7f7f80;">' lead to an Org literate program, if present.</span>
<span style="color: #7f7f80;">This is meant to be used as `</span><span style="color: #fd971f;">:around</span><span style="color: #7f7f80;">' advice for `</span><span style="color: #fd971f;">compile-goto-error</span><span style="color: #7f7f80;">'.</span>
<span style="color: #7f7f80;">OLDFN is `</span><span style="color: #fd971f;">compile-goto-error</span><span style="color: #7f7f80;">'.</span>
<span style="color: #7f7f80;">With PREFIX arg, just run `</span><span style="color: #fd971f;">compile-goto-error</span><span style="color: #7f7f80;">' as though unadvised.</span>
<span style="color: #7f7f80;">ARGS are ignored."</span>
  <span style="color: #fd971f;">(</span><span style="color: #fb2874;">interactive</span> <span style="color: #e2c770;">"P"</span><span style="color: #fd971f;">)</span>
  <span style="color: #fd971f;">(</span><span style="color: #fb2874;">if</span> prefix
      <span style="color: #b6e63e;">(</span>funcall oldfn<span style="color: #b6e63e;">)</span>
    <span style="color: #b6e63e;">(</span><span style="color: #fb2874;">let</span> <span style="color: #66d9ef;">(</span>buffer position column tangled-file-exists-p<span style="color: #66d9ef;">)</span>
      <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">save-window-excursion</span>
        <span style="color: #fb2874;">(</span>funcall oldfn<span style="color: #fb2874;">)</span>
        <span style="color: #fb2874;">(</span><span style="color: #fb2874;">setq</span> column <span style="color: #fd971f;">(</span>- <span style="color: #b6e63e;">(</span>point<span style="color: #b6e63e;">)</span> <span style="color: #b6e63e;">(</span>point-at-bol<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>
        <span style="color: #fb2874;">(</span><span style="color: #fb2874;">when</span> <span style="color: #fd971f;">(</span><span style="color: #fb2874;">ignore-errors</span> <span style="color: #b6e63e;">(</span>org-babel-tangle-jump-to-org<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span>
          <span style="color: #fd971f;">(</span><span style="color: #fb2874;">setq</span> buffer         <span style="color: #b6e63e;">(</span>current-buffer<span style="color: #b6e63e;">)</span>
                position       <span style="color: #b6e63e;">(</span>point<span style="color: #b6e63e;">)</span>
                tangled-file-exists-p t<span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span>
      <span style="color: #555556;">;; </span><span style="color: #555556;">back to where we started - the `</span><span style="color: #fd971f;">compilation-mode</span><span style="color: #555556;">' buffer</span>
      <span style="color: #66d9ef;">(</span><span style="color: #fb2874;">if</span> tangled-file-exists-p
          <span style="color: #fb2874;">(</span><span style="color: #fb2874;">let</span> <span style="color: #fd971f;">(</span><span style="color: #b6e63e;">(</span>org-window <span style="color: #b6e63e;">(</span>get-buffer-window buffer<span style="color: #b6e63e;">)</span><span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span>
            <span style="color: #555556;">;; </span><span style="color: #555556;">if the Org buffer is visible, switch to its window</span>
            <span style="color: #fd971f;">(</span><span style="color: #fb2874;">if</span> <span style="color: #b6e63e;">(</span>window-live-p org-window<span style="color: #b6e63e;">)</span>
                <span style="color: #b6e63e;">(</span>select-window org-window<span style="color: #b6e63e;">)</span>
              <span style="color: #b6e63e;">(</span>switch-to-buffer buffer<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span>
            <span style="color: #fd971f;">(</span>goto-char <span style="color: #b6e63e;">(</span>+ position column<span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>
        <span style="color: #fb2874;">(</span>funcall oldfn<span style="color: #fb2874;">)</span><span style="color: #66d9ef;">)</span><span style="color: #b6e63e;">)</span><span style="color: #fd971f;">)</span><span style="color: #fb2874;">)</span>

<span style="color: #fb2874;">(</span>advice-add 'compile-goto-error <span style="color: #fd971f;">:around</span> #'my-org-lp-goto-error<span style="color: #fb2874;">)</span>
</pre>
</div>

<p>
You can see what the final code looks like in my <a href="https://tildegit.org/contrapunctus/dotemacs/src/branch/production/init.org#compile-org-lp-jump">init.org</a>.
</p>
</div>
</section>
<section id="outline-container-conclusion" class="outline-2">
<h2 id="conclusion"><a href="#conclusion">Conclusion</a></h2>
<div class="outline-text-2" id="text-conclusion">
<p>
If this post helped you, or maybe you know a clearer way to write the advice, I'd love to hear about it—<a href="../contact.html">write to me!</a>
</p>


<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="keyboard-machinations-kmonad.html">← Previous: Keyboard machinations with Kmonad</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="chronometrist-past-present-future.html">→ Next: Chronometrist - past, present, and future</a></div>
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
