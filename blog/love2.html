<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-07-31 Wed 18:14 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>On Love (Part 2)</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">On Love (Part 2)</h1>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="love.html">← Previous: On Love</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="specifications.html">→ Next: Specifications</a></div>
 </nav>
</p>

<p class="note">
<i>This is the second post in a three-part series.</i>
 <br/>
<i>For context, see <a href="love.html">On Love</a>. To find out how this series concludes, see <a href="love3.html">On Love (Part 3)</a>.</i>
</p>

<p>
Phew, that sure was a <i>heavy</i> post back there!
</p>

<p>
In light of a cooler dawn, I have many remarks to make about it, but it was already getting too full - any more, and it would have distracted from the state of mind I was in when I was writing it. Hence this separate post, with commentary and follow-up.
</p>
<section id="outline-container-background" class="outline-2">
<h2 id="background"><a href="#background">Background</a></h2>
<div class="outline-text-2" id="text-background">
<blockquote>
<p>
At the time of writing, I'm about to turn 33 in a few months.
</p>

<p>
In my whole life, I've experienced a grand fucking total of <i>two</i> romantic relationships. They both began in college.<label id='fnr.1' for='fnr-in.1.5738824' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.5738824' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
The first when I was 18, the second when I was 19.
</span> They both ended in college, too.
</p>
</blockquote>
<p>
Note these two well, dear reader. We'll discuss them later.
</p>

<blockquote>
<p>
You know how people start dating in school? I never experienced that. Nobody was interested in me.
</p>
</blockquote>

<p>
No surprises there, actually. I never understood how to socialize, and was a markedly different person from the others. I didn't understand the other kids, and they struggled to understand me. I didn't know it then, but they tried to interact with me to the best of what they knew at that age. I took their jokes to be insults - naturally, that doesn't make life wonderful for either side. Eventually, I sort of cast myself out of all circles.
</p>

<p>
Then, there was no guidance on dating. Perhaps the other guys picked up what they knew from their peer groups. I didn't have one. Even if there was, I was already convinced that nobody could possibly like me in school.
</p>

<blockquote>
<p>
Nobody has expressed genuine romantic interest in me over the past 12 years, either. The vast majority of people around me are either in relationships, or were in one not too long ago, and will have no difficulty finding others who are interested in them.
</p>

<p>
I cannot say the same for myself.
</p>
</blockquote>
<p>
There have been up to three women<label id='fnr.2' for='fnr-in.2.9481302' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.9481302' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
We can call them The Dancer, Mystery Email Girl, and The Actor&#x2026;mostly so I don't forget or miscount.
</span> who expressed an interest, but because it never amounted to anything meaningful, I don't trust it was genuine. Perhaps I misread things, or perhaps it was their circumstances. They make for intriguing if not entertaining stories on their own, but that's a subject for another time.
</p>
</div>
</section>
<section id="outline-container-why" class="outline-2">
<h2 id="why"><a href="#why">Why?</a></h2>
<div class="outline-text-2" id="text-why">
</div>
<div id="outline-container-appearance" class="outline-3">
<h3 id="appearance"><a href="#appearance">Appearance?</a></h3>
<div class="outline-text-3" id="text-appearance">
<blockquote>
<p>
Is it my appearance? Are only people who look a certain way supposed to be loved? Are the others supposed to resign themselves to a life without it?
</p>

<p>
I put significant attention into my appearance. I'm often complimented on my hair, skin, and clothing. I've heard no end of women say that I have an attractive appearance.
</p>

<p>
I have a hard time taking it seriously. If I'm attractive, how has nobody ever considered me worthy of dating?
</p>

<p>
I'm slightly overweight by certain standards.<label id='fnr.3' for='fnr-in.3.2232248' class='margin-toggle sidenote-number'><sup class='numeral'>3</sup></label><input type='checkbox' id='fnr-in.3.2232248' class='margin-toggle'><span class='sidenote'><sup class='numeral'>3</sup>
Such as the waistline not exceeding half of one's height.
</span> Sometimes I suspect it plays a role. After all, college - the only time in my life I've been in a relationship - was also the only time in my life I was thin and lanky rather than chubby and overweight.
</p>

<p>
It's almost comforting to attribute it to weight. It shifts the problem - "<i>I</i>, as a person, am wholly lovable; it's just my body that is flawed." Which leads to, "All I need to do is lose weight, and everything will be fine." "If I run enough, everything will be fine."
</p>

<p>
But then, I've seen people far more overweight than me, who enjoy social popularity, who enjoy loving relationships and marriages.
</p>
</blockquote>
<p>
I wasn't always this way, actually. Through school, college, and beyond, I was labouring under the strange notion of "people should like me for my work and character, not my clothes." The idea of "my presentation is an expression of myself which I cultivate - a way to tell people about myself, a canvas for me to have fun with" came much later. As recently as 2016, I recall myself wearing ill-fitting, color-oblivious and worn-out clothes that were aimed at hiding as much of myself as possible.
</p>

<p>
(I was also slouching a lot to "hide" my weight. An unexpected side effect of taking up regular running in 2022 (!) was that it forced me to fix my posture - you <i>cannot</i> run over a kilometer with incorrect posture. The resulting shoulder pain sees to that.
</p>

<p>
The process of fixing my posture also helped me discover that slouching makes your weight look <i>worse</i>, whereas correct posture - chin up, chest out - helps you look a wee bit thinner, in addition to projecting confidence and energy.)
</p>

<p>
I always took basic care of my hair (regular shampoo and combing), and was thus always complimented on my hair - even in school, where I had unwittingly done my best to make myself a misfit. When I dived into clothing and started paying attention to it, I started being complimented for that, too.
</p>

<p>
With that realization, the compliments may not be as unbelieveable as I initially wrote.
</p>
</div>
</div>
<div id="outline-container-going-out" class="outline-3">
<h3 id="going-out"><a href="#going-out">Going out?</a></h3>
<div class="outline-text-3" id="text-going-out">
<blockquote>
<p>
Some who know me closely say that I don't step out of home enough. I find it hard to believe - I've seen absolute homebodies in enviable relationships.
</p>

<p>
Between college and now, I've gone to a martial arts class, attended a German language course, worked in theatre for four years, gone to a number of music schools, worked in an office, and gone to many events (Calcutta International Classical Guitar Festival for many years, and most recently <a href="anthillhacks-2022-intro.html">AnthillHacks 2022</a>). My residence has moved between four different parts of the city. I frequently go out to survey for <a href="https://openstreetmap.org/user/contrapunctus">OpenStreetMap</a>. I spent much of 2022 regularly going out to run.
</p>

<p>
Clearly, I'm no shut-in.
</p>
</blockquote>

<p>
In German class, I was still struggling to socialize. For much of the time, I was alone. I got through A1 without making <i>any</i> friends. Lunch time was a solitary affair. In later levels, I made some friends. But I still hesitated to interact with the girls.
</p>

<p>
Many of these engagements are not places where you'd expect to meet women, or indeed anyone. Certainly not solo OSM surveys, where I am too busy mapping to talk to people. Hell, I just held a group OSM mapathon, and even in this group setting it was difficult for me to balance mapping and socializing.
</p>

<p>
I never interacted much with the people in my neighbourhood. Can't really expect anything to arise that way.
</p>

<p>
Also, while it sounds like a large number of activities when put together, perhaps it's too little for 12 years.
</p>
</div>
</div>
<div id="outline-container-conversation" class="outline-3">
<h3 id="conversation"><a href="#conversation">Conversation?</a></h3>
<div class="outline-text-3" id="text-conversation">
<blockquote>
<p>
Perhaps I don't know how to hold a conversation.
</p>

<p>
I've tried to observe and (in my own way) emulate my more charming, entertaining, more socially- and romantically-successful peers. I would say I had a degree of success. After years of effort, I'm a markedly different person from where I started out.
</p>

<p>
I remember myself as being pretty quiet, sullen, insecure, and hot-tempered in college. I'm now fairly chatty, if I can find something to talk about - talkative enough that I have to actively ensure I don't dominate the conversation, and let the other person talk about themselves, too. The insecurities are gone, and I now have a fairly relaxed demeanour on most days. I can find humor in just about anything, and I laugh more than anyone I know.
</p>

<p>
And yet, I have no results to show for it. What reason have I to try any more? I'm tired. Each day is more draining than the last.
</p>
</blockquote>

<p>
I don't really feel tired or drained everyday (or at least, not on account of this subject). It's just certain days where I fall into superlative depths of despair. Is this depression? Or will it just go away when I learn to socialize better?
</p>

<p>
I'm very grateful to have had the opportunity to observe and pick up what may be called "people skills" from Aakash, Shreyas, and Gagan.
</p>
</div>
</div>
<div id="outline-container-expressing-interest" class="outline-3">
<h3 id="expressing-interest"><a href="#expressing-interest">Expressing interest</a></h3>
<div class="outline-text-3" id="text-expressing-interest">
<blockquote>
<p>
My suspicion is that I don't know how to express romantic interest in others. It's a vicious circle. Why would you express interest, if you have over 12 years of experience telling you what the answer is going to be?
</p>

<p>
I'm trying to get into the habit of complimenting those I'm interested in. But I doubt anything will change.
</p>
</blockquote>

<p>
Right, here we are. Those two relationships in college? Both were largely initiated by the respective ladies. Only once in my life did I have the confidence to be specific in asking someone out on <i>a date</i>.
</p>

<p>
I've been falling prey to fear and to not knowing any way of expressing myself romantically.
</p>
</div>
</div>
</section>
<section id="outline-container-dating-apps" class="outline-2">
<h2 id="dating-apps"><a href="#dating-apps">"Use a dating app—"</a></h2>
<div class="outline-text-2" id="text-dating-apps">
<blockquote>
<p>
Suffer exploitation of your privacy, or suffer a lifetime of loneliness - what a choice! And what does it say of our world, that poses one with this choice? It doesn't sound like a world I want to live in.
</p>
</blockquote>
<p>
Fortunately, those are <i>not</i> the only choices. For one, I am confident that offline dating remains a viable option. For another, <a href="#resources">DNL</a> reaffirms it.
</p>

<p>
Also, for anyone actually interested in dating apps, there are privacy-friendly FOSS dating services. They aren't huge, but one may be more likely to find people with shared interests (such as an interest in privacy) there.<label id='fnr.4' for='fnr-in.4.8061131' class='margin-toggle sidenote-number'><sup class='numeral'>4</sup></label><input type='checkbox' id='fnr-in.4.8061131' class='margin-toggle'><span class='sidenote'><sup class='numeral'>4</sup>
I never expected in my wildest dreams to meet an intelligent and mature girl who runs Linux, uses F-Droid and Tor (!!), and tinkers with all kinds of tech - but meet her I did (although admittedly not over any dating app).
</span>
</p>
</div>
</section>
<section id="outline-container-resources" class="outline-2">
<h2 id="resources"><a href="#resources">Resources</a></h2>
<div class="outline-text-2" id="text-resources">
<blockquote>
<p>
If there are others like me out there, these are some things that were actually useful over the years.
</p>

<p>
<a href="https://www.doctornerdlove.com/category/basics/">Dr. Nerdlove</a> is one of the few resources I found on the subject that were actually helpful, rather than dismissive, sleazy, pseudo-scientific, and/or grounded in strange misogynistic worldviews. Indeed, he often seeks to liberate men from the toxicity of the latter, and introduces them to the challenges faced by women and how they affect heterosexual dating. I used to read it a few years ago and make notes. It might be time to dive into it again and get a booster dose.
</p>

<p>
<a href="https://www.youtube.com/watch?v=l7TONauJGfc">Non-violent communication</a> introduced me to a whole new worldview. It's not dating advice - it's far more general than that. It's there in my mind all the time, even if I don't use it verbally very often. It's the little light which shines even in the darkest of hours.
</p>
</blockquote>
<p>
Dr. Nerdlove&#x2026;the name makes me cringe.<label id='fnr.5' for='fnr-in.5.6442931' class='margin-toggle sidenote-number'><sup class='numeral'>5</sup></label><input type='checkbox' id='fnr-in.5.6442931' class='margin-toggle'><span class='sidenote'><sup class='numeral'>5</sup>
I don't like the "nerd" label. But I do appreciate his frequent clarification that he's not an actual doctor&#x2026;and his encouragement to get therapy for mental health.
</span> The headings sound like clickbait. The writing is not what I call "crisp", and I have to take notes to stand any chance of memorizing and applying the concrete parts. There's a lot of overlap between the different posts.
</p>

<p>
And yet, it has been an absolutely indispensable resource.
</p>

<p>
There's a lot of overlap between DNL and NVC. NVC is both more general (more widely-applicable), but also more distilled and not as verbose as DNL's conversational style. (To be honest, once you dip into NVC, you'll see it in action everywhere.)
</p>

<p>
On the other hand, DNL is (of course) a lot more relevant to dating, and has a lot of concrete advice. Whereas NVC taught me (among other things) about the general idea of being aware of my evaluations and thoughts about myself or others, re-reading DNL after my last post taught me that I had seemingly overlooked many such thoughts. I held beliefs so insidious that I did not even realize how deeply I had internalized them, until I read the DNL posts dissecting them.
</p>
</div>
</section>
<section id="outline-container-conclusion" class="outline-2">
<h2 id="conclusion"><a href="#conclusion">Conclusion</a></h2>
<div class="outline-text-2" id="text-conclusion">
<p>
My learning happened at a different pace than others. A lot of changes are quite recent, and the results don't show up overnight. And there's still a long way to go. Forgetting these things leads to misery (and depressing blog posts).
</p>

<p>
I must keep reminding myself of the reality, until the old ways of thinking are gone. Half of this reality is something everyone who knows me has constantly been telling me, while the other half is something which was perhaps hidden in plain sight from myself and from others. And that reality is&#x2026;
</p>

<p>
<i><b>I am a desirable and attractive person</b>&#x2026;who has simply avoided expressing romantic and sexual interest in people for much of his life.</i>
</p>

<iframe width="560"
        height="315"
        src="https://yewtu.be/embed/hf1DkBQRQj4?autoplay=0"
        title="YouTube video player"
        frameborder="0"
        allowfullscreen>
</iframe>

<p>
(Sorry, seemed appropriate 😂 If you know, you know.)
</p>

<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="love.html">← Previous: On Love</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="specifications.html">→ Next: Specifications</a></div>
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
