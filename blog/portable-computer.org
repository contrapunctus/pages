#+TITLE: Design notes for my ideal portable computer
#+CREATED: 2024-08-12

@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+TOC: headlines 2

* Me and my mania for mobility
:PROPERTIES:
:CREATED:  2024-08-12T21:02:17+0530
:CUSTOM_ID: user-needs
:END:
I love computers. Most if not all of my work happens on a computer of some kind -

1. Taking notes.
2. Sending messages over [[https://contrapunctus.codeberg.page/xmpp.html][XMPP]].
3. Reading. I much prefer ebooks to dead-tree books. I can customize what they look like, search their contents, quickly jump to sections, make backups of them...
4. Writing prose or poetry
5. Contributing to OpenStreetMap
6. Programming
7. Composing music
8. Practicing singing and guitar (primarily for musical scores, and accompaniment for singing)
10. Tracking time for various activities
11. Watching videos and listening to music

So, for much of the day, I'm working on a computer of some kind.

As an aside - I never could understand the numerous people in the world who talk about "digital detox" and limiting screen time. I once came across this line (paraphrased from memory) on a transhumanist-leaning corner of the Internet, and to this day I greatly identify with it -

#+BEGIN_QUOTE
"If the Internet is an extension of our mind, then to be offline is to suffer brain damage."
#+END_QUOTE

(Does anybody know the source? Please [[file:../contact.org][share it!]] I've looked in vain on many occasions.)

Moreover, when an idea takes hold of me, I usually work on it ceaselessly, from the moment I wake to the moment sleep finally overcomes me. But I also can't sit at my laptop for too long without getting restless and fidgety.

This makes my phone an indispensable tool for my work. With a phone, I can walk around as I work (without needing a specialized walking desk). With the help of Syncthing, my work is seamlessly synchronized between my laptop and phone, and I switch between the two depending on whether I'm sitting or standing at the laptop, or using the phone to work as I sit, stand, or walk...or even lie in bed.

But phones - and all existing portable computers, really - have several issues. I describe these issues below, as well as what a solution could look like.

* Design goals
:PROPERTIES:
:CREATED:  2024-08-12T23:43:33+0530
:CUSTOM_ID: design-goals
:END:

** Portability (obviously)
:PROPERTIES:
:CREATED:  2024-08-13T14:42:28+0530
:CUSTOM_ID: portability
:END:
An ideal portable computer should be usable in any situation, whether you are walking (e.g. on the street), sitting or standing (even in a packed bus, train, or plane), or lying in bed.

Keeping the weight down is also a goal, but secondary to the others listed.

** A proper typing experience
:PROPERTIES:
:CREATED:  [2024-12-15 Sun 19:12]
:CUSTOM_ID: typing-experience
:END:
Most smartphones don't have a physical keyboard, which results in poor typing feedback, poor typing speed (because you can only use two thumbs instead of ten fingers), and the inability to touch type.

Even on devices with a physical keyboard (like the Dragonboard Pyra, the Astro Slide, the MutantC, and others), you are limited to using two thumbs instead of 10 fingers.

While most laptops have a full physical keyboards, they are membrane keyboards with terrible typing feedback. The [[#laptops][MNT Reform]] might be the only exception.

Some suggest voice typing as an alternative, but it's susceptible to ambient noise, and broadcasts your actions to everyone around you, which causes disturbance and is a privacy issue.

An ideal portable computer should have a split, ortholinear mechanical keyboard for the best ergonomics and typing performance. You deserve good typing feedback, to be able to type using all ten fingers, and to be able to /look at what you are typing/, rather than /having to look at the keys!/

(I also don't mind a chorded keyboard like a Twiddler. But it has a high learning curve, whereas I want to make something which has /some/ mass appeal, so I want to explore full-size split keyboards first.)

** Ergonomics
:PROPERTIES:
:CREATED:  2024-08-13T14:42:35+0530
:CUSTOM_ID: ergonomics
:END:
When you're working on a computer as much as I am, even minor ergonomic issues become painfully amplified. And portable computers in their current form have some glaring ergonomic issues.

In most portable devices - smartphones, handhelds, and laptops - the screen and the controls are combined into a single unit. Usually, this means that you have to look down, straining your eyes and/or neck and/or back. Or you can try holding the phone up to keep the eyes and neck at a neutral position, but then your arms will hurt after a while. In any case, your chest also adopts a concave posture. Meh.

Sure, I could take frequent breaks, stretch while working, and so on. But where's the fun in that? I want to be able to work continuously, without breaking my flow, and without posture-related injuries. And improving ergonomics in harmony with the other constraints is an interesting problem.

So, in an ideal portable computer -

1. The screen and controls should be separable, so the eyes, neck, and back are in a relaxed and natural posture.

2. The controls should also be separable. (Hence the split keyboard.)

** Durability and Repairability
:PROPERTIES:
:CREATED:  2024-08-13T14:42:52+0530
:CUSTOM_ID: durability-and-repairability
:END:
Phones (and laptops) are notoriously difficult to repair. Sooner or later companies stop providing warranty for them, and then you're on your own.

An ideal portable computer should be a robust and "buy it for life" device, made with easily-available components. It should be open hardware, so users can make their own replacement parts.

In addition, most consumer hardware is susceptible to water. Many an OSM survey has been halted because of rain. An ideal portable computer should be weather-proof. (Probably something for the longer run.)

** Software
:PROPERTIES:
:CREATED:  2024-08-13T14:42:58+0530
:CUSTOM_ID: software
:END:
Smartphones typically run Android, developed by Google...who continually makes developments which make life worse for power users, privacy-conscious users, and software freedom proponents. SafetyNet, [fn:1] Scoped Storage [fn:2]...

Android is not a great for developers either, who often complain about the changes Google imposes.

An ideal portable computer should support venerable community-made free software operating systems like GNU/Linux and BSD, and all the software they support.

[fn:1] helping banking apps stop working if your phone is rooted or has a de-Googled ROM

[fn:2] Making it impossible for applications to access the filesystem, /even with root!/

** Good I/O
:PROPERTIES:
:CREATED:  2024-08-13T14:43:08+0530
:CUSTOM_ID: good-io
:END:
The I/O on most phones is limited to Bluetooth, WiFi, and a USB Type-C port. We should do better.

** Materials
:PROPERTIES:
:CREATED:  [2024-12-15 Sun 21:57]
:CUSTOM_ID: materials
:END:
The production, use, and disposal of the materials should cause the least possible damage to the world we live in.

Plastic should be avoided. Wood and metal should be preferred.

* What it might look like
:PROPERTIES:
:CREATED:  2024-08-13T06:13:05+0530
:CUSTOM_ID: what-it-might-look-like
:END:
Think of a handheld similar in size to a Steam Deck, but it's made of three modules -

1. The 2 outer modules are 6×4 vertical split keyboards with two trackballs and 4-8 thumb keys on each side. They have hand straps (like the [[#wearable-keyboards][GrabShell keyboard]], or a concertina/bandoneon) to allow you to hold it up without using your fingers. The straps also make it difficult to snatch.

2. The center module can be -

   * A dock to hold a smartphone in vertical or horizontal orientation. It could also have a power bank and wireless charging to charge the phone while docked.

   * The center module could be replaced with a compute module, containing a battery, SBC, display, SSD, ports, speakers, etc. The display can be as small or large as the user can tolerate.

** Modularity
:PROPERTIES:
:CREATED:  2024-12-13T23:26:34+0530
:CUSTOM_ID: modularity
:END:
The modules could be attached using magnets or other fasteners, allowing for the following modes of use -

1. Unified mode. All three modules are attached together, forming something like a large handheld. The device is held using the palm straps. The width of the center module allows the user's chest to be open. The center module or the AR glasses could be used as the display.

2. Split mode. The center module is detached and placed on a table to act as the display. The keyboard modules can be attached to each other directly, or used in a split configuration. Useful on flights and trains (chair cars), where there's not much lateral space, but a table is available to place the center module.

3. AR mode. Only the keyboard modules are used, either split or attached. The center module is placed in a pocket or bag. AR glasses are the sole means of display.

Like the GrabShell, it should stay upright if you put it down on a desk, allowing you to easily put it down and pick it up again.

*** Ways to connect modules :noexport:
:PROPERTIES:
:CREATED:  2025-01-14T18:59:51+0530
:CUSTOM_ID: ways-to-connect-modules
:END:
1. Magnets
2. Dovetail rail
3. https://en.m.wikipedia.org/wiki/Mortise_and_tenon

** Limitations
:PROPERTIES:
:CREATED:  2024-12-13T23:27:07+0530
:CUSTOM_ID: limitations
:END:
1. You can't put it into a pocket like a phone.
   + That's also good because it's impossible for a pickpocket to hide it.
   + It may be useful to add a strap to it (like a sling bag), so you can let go of it when you want to quickly free up your hands.

2. It probably won't be easy to remove your hands from the wrist straps when the keyboard modules are split. You'd probably have to connect them to the center module or to each other first, so one hand can hold the device still while you free the other hand from the wrist strap.

** A folding display as an alternative to AR glasses
:PROPERTIES:
:CREATED:  2024-12-13T23:28:36+0530
:CUSTOM_ID: folding-display
:END:
AR glasses are not always affordable or appropriate, so this display would be a useful addition.

This "display" has two arms at its sides. (If using a phone for compute and display, this can be a kind of smartphone holder.)

When folded down, the display and the arms are flush with the device body, allowing it to be used in a compact (but not very ergonomic) configuration.

Or it can be unfolded for better ergonomics (at the expense of vertical space) -
1. You unfold the arms upwards. The display now faces away from you.
2. You unfold the display upwards. The display now faces you, and should require less bending of the neck to use.

(Unfolding is really a one-step operation - you just pull the screen into the higher position. The two steps above are only to explain its design.)

** Advanced extensions :noexport:
:PROPERTIES:
:CREATED:  [2024-12-16 Mon 02:49]
:CUSTOM_ID: advanced-extensions
:END:
Multi-device quick-switch Bluetooth

Ability to place and use the keyboard on a desk, like traditional keyboards.

* Existing bits and pieces
:PROPERTIES:
:CREATED:  2024-08-15T20:35:21+0530
:CUSTOM_ID: existing-bits-and-pieces
:END:

** Laptops
:PROPERTIES:
:CREATED:  2025-01-25T12:11:24+0530
:CUSTOM_ID: laptops
:END:
1. [[https://mntre.com/reform.html][MNT Reform]]
2. [[https://www.byran.ee/posts/creation/][anyon_e]] ([[https://github.com/Hello9999901/laptop][GitHub]])

** AR glasses
:PROPERTIES:
:CREATED:  2024-08-15T20:35:24+0530
:CUSTOM_ID: ar-glasses
:END:
1. XReal AR glasses
   + [[https://www.youtube.com/watch?v=ZQJqPD3sckU][XReal Air 2 Pro  AR glasses — Spencer Scott Pugh]]
2. Rokid Max 2 AR
3. [[https://www.youtube.com/watch?v=PwmGNWkpKW8][Open Source Smart Glasses - DIY AR — Cayden Pierce]]
   + https://github.com/CaydenPierce/OpenSourceSmartGlasses
4. https://brilliant.xyz/

Problems
1. Some look too conspicuous
   + the XReal's are merely a little bulky
   + the Rokid looks a little weird
   + the Apple Vision Pro looks insanely conspicuous
2. In many cases, people nearby are able to see what is displayed on the AR glasses.
3. Safety/situational awareness (theft, assault, ...)
4. Most such glasses require a cable (I don't mind too much)

** Handhelds
:PROPERTIES:
:CREATED:  2024-08-15T20:35:51+0530
:CUSTOM_ID: handhelds
:END:
1. [[https://pyra-handheld.com/boards/pages/pyra/][DragonBox Pyra]]
2. [[https://mutantcybernetics.com/][MutantC]]
3. [[https://mecha.so/comet][Mecha Comet]]

** Wearable keyboards
:PROPERTIES:
:CREATED:  2024-08-15T20:36:22+0530
:CUSTOM_ID: wearable-keyboards
:END:
Twiddler
+ It is designed for one hand, and relies on keychording.

DataHand/Lalboard/Svalboard (?)

Grab Shell keyboard
+ A [[https://www.youtube.com/watch?v=O7p68Gxxlfo][review of the Grab Shell keyboard]] on YouTube

[[https://old.reddit.com/comments/qvpekz][I made a...thing]] (u/ExtremePocket in r/ErgoMechKeyboards)

* What now?
:PROPERTIES:
:CREATED:  2025-02-08T00:22:35+0530
:CUSTOM_ID: what-now
:END:
I'll probably have to learn CAD to make this happen. Also, I should get a job 😑



@@html: <nav class="navlinks">@@
[[blog:prev]] [[file:index.org][↩ blog]] [[blog:next]]
@@html: </nav>@@

#+INCLUDE: includes.org::#footer :only-contents t
