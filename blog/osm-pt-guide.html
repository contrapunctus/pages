<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2024-07-31 Wed 18:14 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>How I map public transport routes on OpenStreetMap</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">How I map public transport routes on OpenStreetMap</h1>
</header><p>
 <nav class="navlinks">
<a href="index.html">↩ blog</a>
 </nav>
</p>

<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#why-map-pt">Why survey PT for OSM?</a></li>
<li><a href="#surveying-route">Surveying the route</a>
<ul>
<li><a href="#recording-gps-track">Recording a GPS track</a></li>
<li><a href="#mapping-along-route">Mapping bus stops (and other features) along the route.</a></li>
<li><a href="#tips">Tips</a></li>
</ul>
</li>
<li><a href="#preparing-josm-editing">Preparing JOSM for editing</a>
<ul>
<li><a href="#setup">Setup</a></li>
<li><a href="#downloading-data">Downloading data</a></li>
<li><a href="#josm-basics">JOSM basics</a></li>
</ul>
</li>
<li><a href="#pt-editing">PT editing</a>
<ul>
<li><a href="#route-relations">Route relations</a></li>
<li><a href="#creating-stops">Creating stops</a></li>
<li><a href="#adding-or-updating-route-relation">Adding or updating a route relation</a></li>
<li><a href="#relation-editor">The relation editor</a></li>
<li><a href="#populating-empty-route-relation">Populating an empty route relation</a></li>
<li><a href="#adding-stops-to-relation">Adding stops to the relation</a></li>
<li><a href="#adding-ways-to-relation">Adding ways to the relation</a></li>
<li><a href="#stop-positions">Stop positions</a></li>
<li><a href="#roles">Roles</a></li>
<li><a href="#validation-uploading">Validation and uploading</a></li>
<li><a href="#notes">Notes</a></li>
</ul>
</li>
</ul>
</div>
</nav>

<p>
I've often felt like writing this guide to help newcomers get into public transport editing, and now it's finally here. <label id='fnr.1' for='fnr-in.1.1716693' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.1716693' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
(The PT pages on the OSM wiki are a mess and I don't feel like fighting its editors about it. This guide is derived from the wiki, and satisfies the PTv2 proposal, the JOSM validator, and the OsmAnd renderer and router.)
</span>
</p>

<p>
This guide is centered around surveying and adding bus routes, but it's also applicable to share taxis, coaches (interstate buses), and trains.
</p>

<p>
("Share taxis" is the OSM umbrella term for modes like share autorickshaws/Gramin Seva, share e-rickshaws, shared jeeps, and so on. Yes, these routes can be mapped! They are one of the unique features in OpenStreetMap - I don't know of any other map provider which supports them. OsmAnd supports viewing share taxi routes, and includes them in public transport navigation.)
</p>

<p>
Surveying and editing public transport are both difficult and time-consuming tasks. If approached as an adventure, it can be fun. Enhancing OsmAnd's multi-modal PT routing certainly makes it worthwhile for me. <label id='fnr.2' for='fnr-in.2.5738824' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.5738824' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
Is there any other application which uses OSM PT data for routing? Please <a href="../contact.html">let me know</a>.
</span>
</p>
<section id="outline-container-why-map-pt" class="outline-2">
<h2 id="why-map-pt"><a href="#why-map-pt">Why survey PT for OSM?</a></h2>
<div class="outline-text-2" id="text-why-map-pt">
<ol class="org-ol">
<li>PT authorities in your area may not have released GTFS data under a compatible license</li>
<li>Said data may be outdated.</li>
<li>Surveying may be the only source of data for some modes of transport, such as share taxis.</li>
<li>PT surveys may get you to survey in places you may otherwise not frequent.</li>
</ol>
</div>
</section>
<section id="outline-container-surveying-route" class="outline-2">
<h2 id="surveying-route"><a href="#surveying-route">Surveying the route</a></h2>
<div class="outline-text-2" id="text-surveying-route">
</div>
<div id="outline-container-recording-gps-track" class="outline-3">
<h3 id="recording-gps-track"><a href="#recording-gps-track">Recording a GPS track</a></h3>
<div class="outline-text-3" id="text-recording-gps-track">
<p>
<i>I've used OsmAnd for a long time, but some time in the past one year, OsmAnd acquired a very annoying bug where it creates gaps in the track recording if you so much as look at it funny. To work around it, I tried running OsmAnd in split screen to always keep it in the foreground, with Vespucci in the other split&#x2026;but even then, sometimes OsmAnd would just crash when recording a long track. These days I use OpenTracks instead, which is just fire-and-forget <label id='fnr.3' for='fnr-in.3.9481302' class='margin-toggle sidenote-number'><sup class='numeral'>3</sup></label><input type='checkbox' id='fnr-in.3.9481302' class='margin-toggle'><span class='sidenote'><sup class='numeral'>3</sup>
Don't "forget" to stop the recording, though. 😉
</span>.</i>
</p>

<ol class="org-ol">
<li>Install OsmAnd</li>
<li><p>
Enable the "Trip recording" plugin
</p>

<p>
Menu - Plugins - "Trip recording" - "On". If you go back to the main OsmAnd map screen, you should now see a "REC" button in the top-right corner.
</p></li>

<li>Logging interval - small files vs more accuracy
(I like to see my tracks colored by speed.)</li>

<li>Naming the track.</li>
</ol>
</div>
</div>
<div id="outline-container-mapping-along-route" class="outline-3">
<h3 id="mapping-along-route"><a href="#mapping-along-route">Mapping bus stops (and other features) along the route.</a></h3>
<div class="outline-text-3" id="text-mapping-along-route">
<ol class="org-ol">
<li><p>
Taking pictures with OpenCamera
</p>
<ol class="org-ol">
<li>Install OpenCamera</li>
<li>Enable location and compass direction</li>
<li>Enable touch-to-focus and double-tap-to-shoot</li>
</ol>

<p>
When photos aren't practical - e.g. it's too dark, or the vehicle is moving too fast. Or perhaps you just weren't able to photograph something in time.
</p></li>

<li><p>
Editing with Vespucci.
</p>

<p>
Vespucci is my editor of choice for most situations. The presets allow for rapid discovery and entry of many details. When traveling in a bus, I enable location-based autodownload <label id='fnr.4' for='fnr-in.4.2232248' class='margin-toggle sidenote-number'><sup class='numeral'>4</sup></label><input type='checkbox' id='fnr-in.4.2232248' class='margin-toggle'><span class='sidenote'><sup class='numeral'>4</sup>
It's a good idea to also adjust your autodownload speed in Preferences - Advanced preferences - Auto-download settings - Maximum auto-download speed.
</span> and maybe copy-paste an object to serve as a template.
</p>

<p>
This doesn't work well if the network is too slow or unreliable, or if the bus is traveling too fast for you to edit.
</p>

<p>
You could sidestep the problem by downloading data for offline editing in Vespucci, but I prefer to use OsmAnd for that, since it has hourly incremental updates.
</p></li>

<li><p>
Recording GPS waypoints with OsmAnd.
</p>

<p>
Other alternatives (I haven't tried them, but they may be worth looking into) - geotagged voice notes.
</p></li>
</ol>

<p>
Doing all three at once in a moving vehicle can be difficult. Don't worry if you struggle with it in the beginning. Doing things "well" is hard, but the important thing is to start and to persevere.
</p>
</div>
</div>
<div id="outline-container-tips" class="outline-3">
<h3 id="tips"><a href="#tips">Tips</a></h3>
<div class="outline-text-3" id="text-tips">
<ol class="org-ol">
<li>It's perfectly okay to survey the route bit-by-bit. But it may be more cost-effective to survey entire routes at once.</li>

<li>Bus routes can be long, so pack some water, snacks, a power bank, and a charging cable. Try to get a seat - you don't want to be standing the whole time.</li>

<li>Try getting a good seat with an unobstructed view. If you plan on adding bus stop details, choose a seat where you can easily see the entire bus stop.</li>

<li>Consider the time of day. Traveling during the day means lighting is good - suitable for photographs. If you are surveying during rush hour, buses may move more slowly (giving you more time to photograph), but they may also be more crowded.</li>
</ol>
</div>
</div>
</section>
<section id="outline-container-preparing-josm-editing" class="outline-2">
<h2 id="preparing-josm-editing"><a href="#preparing-josm-editing">Preparing JOSM for editing</a></h2>
<div class="outline-text-2" id="text-preparing-josm-editing">
<p>
Why not iD? &#x2026;
</p>

<p>
It's also perfectly possible to add and modify bus routes in Vespucci. But the tools are not yet as polished as they are in JOSM.
</p>

<p>
I strongly recommend using a mouse rather than a touchpad here - unless you're good at tap-to-click and tap-dragging with the touchpad. Even then, a mouse is more comfortable.
</p>
</div>
<div id="outline-container-setup" class="outline-3">
<h3 id="setup"><a href="#setup">Setup</a></h3>
<div class="outline-text-3" id="text-setup">
<ol class="org-ol">
<li><p>
Install JOSM. I should probably be using the version in my GNU/Linux distribution repositories, but instead I'm usually doing -
</p>

<div class="org-src-container">
<pre class="src src-shell">   wget -c https://josm.openstreetmap.de/josm-tested.jar
   java -jar josm-tested.jar
</pre>
</div></li>

<li><p>
In JOSM, install the <code>pt_assistant</code> plugin.
</p>

<p>
Go to Edit - Preference - Plugins - click the Download List button.
</p>

<p>
Once that's done, find <code>pt_assistant</code>, check the box, and click "Update plugins". Restart JOSM.
</p></li>
</ol>
</div>
</div>
<div id="outline-container-downloading-data" class="outline-3">
<h3 id="downloading-data"><a href="#downloading-data">Downloading data</a></h3>
<div class="outline-text-3" id="text-downloading-data">
<ol class="org-ol">
<li><p>
We'll use Overpass to download existing routes in the region we'll be working in, so we don't accidentally create duplicate routes.
</p>

<p>
First, enable Expert Mode. Go to Edit - Preferences - Enable Expert Mode.
</p>

<p>
Now, when you click the Download button, you should see a new "Download from Overpass API" tab. Open that tab and click the "Query Wizard" button.
</p>

<p>
If I'm adding a route that stays within Delhi (e.g. a bus or share taxi route), I'll enter the following query -
</p>

<p>
<code>(route=bus or route_master=bus) in Delhi</code>
</p>

<p>
or
</p>

<p>
<code>(route=share_taxi or route_master=share_taxi) in Delhi</code>
</p>

<p>
If I'm working on something like a national train route (e.g. a Shatabdi or Rajdhani route), I'll download train routes for the whole of India.
</p>

<p>
<code>(route=train or route_master=train) in India</code>
</p>

<p>
Click "Build query and execute". Depending on your region, you may now have some relations.
</p></li>

<li><p>
If you don't have a GPS track - perhaps you're adding a route from memory - it can be helpful to also download platforms and stop positions within your working area, e.g.
</p>

<p>
<code>((public_transport=stop_position and bus=yes) or (public_transport=platform and bus=yes) or highway=bus_stop) in Delhi</code>
</p>

<p>
And so on.
</p></li>

<li><p>
If you do have a GPS track, open it by going to File - Open (or press <code>Ctrl-o</code>).
</p>

<p>
Your GPS track should now be displayed.
</p>

<p>
Download data from OSM along this track. In the <code>Layers</code> pane, <label id='fnr.5' for='fnr-in.5.852288' class='margin-toggle sidenote-number'><sup class='numeral'>5</sup></label><input type='checkbox' id='fnr-in.5.852288' class='margin-toggle'><span class='sidenote'><sup class='numeral'>5</sup>
If you don't see a <code>Layers</code> pane, go to Windows and click on Layers.



 <nav class="navlinks">
<a href="index.html">↩ blog</a>
 </nav>



 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</span> right-click on the GPS trace layer, and click on <code>Download along GPS track</code>.
</p></li>
</ol>
</div>
</div>
<div id="outline-container-josm-basics" class="outline-3">
<h3 id="josm-basics"><a href="#josm-basics">JOSM basics</a></h3>
<div class="outline-text-3" id="text-josm-basics">
<p>
If this is your first time using JOSM -
</p>
<ol class="org-ol">
<li>You can move the map by holding down the right mouse button and dragging.</li>
<li>Press <code>a</code> to add data.</li>
<li>Press <code>s</code> to switch to the Select tool.</li>
</ol>

<ol class="org-ol">
<li>To select multiple objects, Ctrl-click them. During multiselect, Ctrl-click a selected object to deselect it. Click anywhere else to clear the selection.</li>
</ol>
</div>
</div>
</section>
<section id="outline-container-pt-editing" class="outline-2">
<h2 id="pt-editing"><a href="#pt-editing">PT editing</a></h2>
<div class="outline-text-2" id="text-pt-editing">
</div>
<div id="outline-container-route-relations" class="outline-3">
<h3 id="route-relations"><a href="#route-relations">Route relations</a></h3>
<div class="outline-text-3" id="text-route-relations">
<p>
PT routes on OSM are represented by <i>relations</i>. A relation is used to define a relationship between other objects (such as nodes, ways, and other relations). These objects are called the <i>members</i> of the relation.
</p>

<p>
Each route relation represents a <i>variant</i> of the route, e.g. for the DTC bus route 740, there will be
</p>
<ol class="org-ol">
<li>one variant going from Uttam Nagar to Anand Vihar, and</li>
<li>another variant going from Anand Vihar to Uttam Nagar.</li>
</ol>

<p>
Finally, there will be a <i>route master relation</i> (<code>type=route_master</code> + <code>route_master=bus</code> in this case) whose members will be the route relations representing all variants belonging to a route.<label id='fnr.6' for='fnr-in.6.9214321' class='margin-toggle sidenote-number'><sup class='numeral'>6</sup></label><input type='checkbox' id='fnr-in.6.9214321' class='margin-toggle'><span class='sidenote'><sup class='numeral'>6</sup>
A route like 740A is not a variant of route 740 in the OSM sense - on OSM, for variants to be considered part of a route master, they must have the same <code>ref=*</code>. Route 740A, route 740B, etc have a different <code>ref=*</code> than route 740, and thus get their own route and route master relations, separate from route 740.



As a counterexample, all variants of the OMS route - whether <code>(+)</code> or <code>(-)</code>, and whether covering the full route or just a part of it - are eligible to be part of the OMS route master, because they all share the same <code>ref=*</code>.
</span>
</p>

<p>
A PT route relation will usually contain (in order) -
</p>

<ol class="org-ol">
<li>A stop position at the start of the route</li>

<li>The stops of the route, in the order they occur</li>

<li>A stop position at the end of the route</li>

<li>The ways of the route, in the order of occurrence. For road transport like buses and share taxis, these are the roads (<code>highway=*</code> ways) that the vehicle travels along. For metro and train routes, these are railway lines. (<code>railway=rail</code> ways)</li>
</ol>
</div>
</div>
<div id="outline-container-creating-stops" class="outline-3">
<h3 id="creating-stops"><a href="#creating-stops">Creating stops</a></h3>
<div class="outline-text-3" id="text-creating-stops">
<p>
Stops are defined as places where the passengers wait for the vehicle, <label id='fnr.7' for='fnr-in.7.1471703' class='margin-toggle sidenote-number'><sup class='numeral'>7</sup></label><input type='checkbox' id='fnr-in.7.1471703' class='margin-toggle'><span class='sidenote'><sup class='numeral'>7</sup>
For bus stops, this is usually on the <i>side</i> of the road, even if the stop just consists of a sign in the middle of the road. Railway platforms may occur between the two tracks (<a href="https://en.wikipedia.org/wiki/Island_platform">island platforms</a>), or beside them (<a href="https://en.wikipedia.org/wiki/Side_platform">side platforms</a>).
</span> e.g. bus stops, Metro/train platforms, etc. <label id='fnr.8' for='fnr-in.8.9670868' class='margin-toggle sidenote-number'><sup class='numeral'>8</sup></label><input type='checkbox' id='fnr-in.8.9670868' class='margin-toggle'><span class='sidenote'><sup class='numeral'>8</sup>
Share taxis in Delhi don't have any fixed and dedicated stops, but there are some common starting points which can be considered stops. They also stop at bus stops, so these bus stops should also get a <code>share_taxi=yes</code>.
</span>
</p>

<p>
Any stop may be added as a node, open way (line), or closed way (area), with the tag <code>public_transport=platform</code>. The exact type of platform is determined by additional keys -
</p>

<ol class="org-ol">
<li><code>bus=yes</code> for bus stops</li>
<li><code>train=yes</code> for railway platforms</li>
<li><code>subway=yes</code> for Metro platforms</li>
<li><code>share_taxi=yes</code> for share taxi stops</li>
</ol>

<p>
This tagging scheme is called Public Transport v2 (or PTv2 for short), and is the latest and greatest way to tag platforms - it allows for elegantly tagging multi-modal platforms, e.g. bus stops which are also share taxi stops, bus stops which are also tram stops, etc.
</p>

<p>
There's also an older way of tagging, called PTv1, which used the keys <code>highway=bus_stop</code>, <code>railway=platform</code>, etc instead. You should not add these tags. I only mention them because you may encounter situations where only PTv1 is used - in such cases, add the PTv2 tags without removing the PTv1 tags. <label id='fnr.9' for='fnr-in.9.8101394' class='margin-toggle sidenote-number'><sup class='numeral'>9</sup></label><input type='checkbox' id='fnr-in.9.8101394' class='margin-toggle'><span class='sidenote'><sup class='numeral'>9</sup>
Some software (e.g. certain renderers) does not support PTv2. This software should be fixed, rather than requiring the data to fit the defective software.
</span>
</p>

<p>
In case any stops served by your route are unmapped -
</p>

<ol class="org-ol">
<li>Create the object

<ul class="org-ul">
<li>Create a node by pressing <code>a</code>, clicking the location of the platform, and pressing <code>Esc</code>; OR</li>

<li>Create a way by pressing <code>a</code>, clicking the location of each node of the way (to close the way, click on your starting node), and pressing <code>Esc</code>;</li>
</ul></li>

<li>Press <code>s</code> to go back to the Select tool, click your node/way to select it, and press <code>Alt-a</code> to add a key-value to the object.

<ul class="org-ul">
<li>Press <code>Enter</code> to add the key-value, or <code>Shift-Enter</code> to add another key-value without closing the dialogue.</li>

<li>Some additional key-values of interest for stops -
<ol class="org-ol">
<li><code>name=*</code></li>
<li><code>bench=yes/no</code></li>
<li><code>shelter=yes/no</code></li>
<li><code>tactile_paving=yes/no/incorrect</code></li>
</ol></li>
</ul></li>
</ol>
</div>
</div>
<div id="outline-container-adding-or-updating-route-relation" class="outline-3">
<h3 id="adding-or-updating-route-relation"><a href="#adding-or-updating-route-relation">Adding or updating a route relation</a></h3>
<div class="outline-text-3" id="text-adding-or-updating-route-relation">
<p>
Observe the Relations pane.<label id='fnr.10' for='fnr-in.10.2061938' class='margin-toggle sidenote-number'><sup class='numeral'>10</sup></label><input type='checkbox' id='fnr-in.10.2061938' class='margin-toggle'><span class='sidenote'><sup class='numeral'>10</sup>
If you don't see a <code>Relations</code> pane, go to Windows and click on Relations.
</span> Depending on the data in your area, you may see some relations there already.
</p>

<p>
Double-click a relation to highlight it on the map. Use the search bar in the relations pane to see if there's already a relation for the route you're trying to add.
</p>

<ul class="org-ul">
<li>If one exists and it's incorrect or incomplete, click to select it, then click the edit button.</li>
<li>If there's one similar to yours, you can select it and click the duplicate button to create a copy of it as a starting point.</li>
<li>Otherwise, create a new empty relation with the + button.</li>
</ul>
</div>
</div>
<div id="outline-container-relation-editor" class="outline-3">
<h3 id="relation-editor"><a href="#relation-editor">The relation editor</a></h3>
<div class="outline-text-3" id="text-relation-editor">
<p>
You should now be in a relation editor window, with the relation tags on the top, a member list on the bottom left, and a selection list on the bottom right.
</p>

<p>
If you select an object on the map, it will show up in the selection area. There are four buttons which add your selection to the relation members -
</p>
<ul class="org-ul">
<li>one to add them at the beginning of the members list,</li>
<li>one to add them at the end,</li>
<li>one to add them before the selected member, and</li>
<li>one to add them after the selected member.</li>
</ul>
</div>
</div>
<div id="outline-container-populating-empty-route-relation" class="outline-3">
<h3 id="populating-empty-route-relation"><a href="#populating-empty-route-relation">Populating an empty route relation</a></h3>
<div class="outline-text-3" id="text-populating-empty-route-relation">
<p>
If you created a new empty relation, the first order of business is to add some members, because closing the window without adding any members will delete your relation. So let's add a single stop of the route to the member list, to keep that from happening.
</p>

<p>
Then, let's add some basic tags which are common to all PT routes -
</p>
<ol class="org-ol">
<li><code>type=route</code></li>
<li><code>route=bus</code> (or <code>route=share_taxi</code>​/​<code>route=train</code>​/​<code>route=subway</code>, depending on what you're adding)</li>
<li><code>public_transport:version=2</code></li>
</ol>

<p>
Let's also add some essential tags so we can easily find our relation again (I'm using bus route 740 as an example) -
</p>
<ol class="org-ol">
<li><code>ref=740</code></li>
<li><code>from=Anand Vihar ISBT</code></li>
<li><code>to=Uttam Nagar</code></li>
<li><code>name=Bus 740: Anand Vihar ISBT -&gt; Uttam Nagar</code> or <code>name=Train 12313: Sealdah Rajdhani Express</code></li>
</ol>

<p>
Some other interesting keys
</p>
<ol class="org-ol">
<li><a href="https://wiki.openstreetmap.org/wiki/Key:air_conditioning"><code>air_conditioning=yes/no</code></a> if the bus/train has air conditioning</li>
<li><a href="https://wiki.openstreetmap.org/wiki/Key:power_supply"><code>power_supply=yes/no</code></a> if the bus/train provides charging sockets, and <a href="https://wiki.openstreetmap.org/wiki/Key:socket:*"><code>socket:*=*</code></a> if you know the kind of charging socket</li>
<li><a href="https://wiki.openstreetmap.org/wiki/Key:passenger"><code>passenger=yes/no</code></a> to specify whether the train carries passengers, or
<code>passenger=international/national/regional/suburban/local</code> to specify the scope of the train</li>
<li><a href="https://wiki.openstreetmap.org/wiki/Tag:amenity%3Dtoilets#Toilets_within_places"><code>toilets=yes/no</code></a> if the train has toilets</li>
</ol>
</div>
</div>
<div id="outline-container-adding-stops-to-relation" class="outline-3">
<h3 id="adding-stops-to-relation"><a href="#adding-stops-to-relation">Adding stops to the relation</a></h3>
<div class="outline-text-3" id="text-adding-stops-to-relation">
<p>
Add all the stops to the relation, in the order they occur along the route.
</p>

<p>
If your stops and the route relation are tagged correctly, you'll notice that JOSM applies the "platform" role to them when you add them.
</p>
</div>
</div>
<div id="outline-container-adding-ways-to-relation" class="outline-3">
<h3 id="adding-ways-to-relation"><a href="#adding-ways-to-relation">Adding ways to the relation</a></h3>
<div class="outline-text-3" id="text-adding-ways-to-relation">
<p>
Next is to add all the ways of the route to the route relation.
</p>

<p>
Sometimes a way may be missing entirely, and you'll have to create one.
</p>

<p>
Sometimes, you may need to split ways by selecting a way node and pressing <code>p</code>.
</p>

<ul class="org-ul">
<li>If you get a "you need to select two or more nodes to split a circular way" message, you have to select multiple nodes using Ctrl-click, then press <code>p</code>.</li>

<li>Sometimes, a node is shared by multiple ways, and JOSM doesn't know which way you want to split. Select the way to split as well as the node to split at, and press <code>p</code>.</li>
</ul>

<p>
It took me some time to discover, but the fastest way to add ways to a route relation in JOSM is -
</p>

<ol class="org-ol">
<li>select the ways (<i>without</i> closing the relation editor!),</li>

<li>add the selected ways to the relation members,</li>

<li>right click on the member list - zoom to gap/select next gap,</li>

<li>repeat.</li>
</ol>
</div>
</div>
<div id="outline-container-stop-positions" class="outline-3">
<h3 id="stop-positions"><a href="#stop-positions">Stop positions</a></h3>
<div class="outline-text-3" id="text-stop-positions">
<p>
The exact starting point and ending point of the route should be demarcated with a stop position. This is a node with the tags <code>public_transport=stop_position</code> + <code>bus=yes</code>​/​<code>share_taxi=yes</code>​/​<code>train=yes</code>​/​<code>subway=yes</code>.
</p>

<p>
Add the stop positions to the route members - one before the stops, and one after them.
</p>
</div>
</div>
<div id="outline-container-roles" class="outline-3">
<h3 id="roles"><a href="#roles">Roles</a></h3>
<div class="outline-text-3" id="text-roles">
<p>
Sometimes, routes don't have fixed stops - the vehicle may stop almost anywhere the passengers ask. For these situations, apply the <code>hail_and_ride</code> role to the ways where applicable, splitting the ways if necessary.
</p>

<p>
OsmAnd doesn't support <code>hail_and_ride</code> yet, so I add some common stops as platforms regardless.
</p>

<p>
Share autos
Noida bus
</p>
</div>
</div>
<div id="outline-container-validation-uploading" class="outline-3">
<h3 id="validation-uploading"><a href="#validation-uploading">Validation and uploading</a></h3>
<div class="outline-text-3" id="text-validation-uploading">
</div>
</div>
<div id="outline-container-notes" class="outline-3">
<h3 id="notes"><a href="#notes">Notes</a></h3>
<div class="outline-text-3" id="text-notes">
<p>
Customizing the interface - managing panes, customizing keybindings
</p>
</div>
</div>
</section>
</article>
</body>
</html>
