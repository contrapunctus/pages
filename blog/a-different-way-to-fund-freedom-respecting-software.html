<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2025-01-14 Tue 18:56 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>A different way to fund freedom-respecting software</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">A different way to fund freedom-respecting software</h1>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="unexpected-opposition-to-free-software-advocacy.html">← Previous: Unexpected opposition to free software advocacy</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="portable-computer.html">→ Next: Design notes for my ideal portable computer</a></div>
 </nav>
</p>

<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#the-idea-focused-fixed-goal-crowdfunding">The idea - focused, fixed-goal crowdfunding</a>
<ul>
<li><a href="#the-fund-to-develop-model">The fund-to-develop model</a></li>
<li><a href="#the-fund-to-release-model">The fund-to-release model</a></li>
</ul>
</li>
<li><a href="#the-benefits-of-the-ransom-model">Benefits of the fund-to-release model</a>
<ul>
<li><a href="#greater-incentive-to-pay">1. Greater incentive to pay</a></li>
<li><a href="#user-control-over-development">2. User control over development</a></li>
<li><a href="#lowering-obstacles-to-paid-contribution">3. Lowering obstacles to paid contribution</a></li>
<li><a href="#user-focused-development">4. User-focused development</a></li>
<li><a href="#higher-likelihood-of-delivery">5. Higher likelihood of delivery</a></li>
<li><a href="#developers-reach-out-to-users">6. Developers reach out to users</a></li>
</ul>
</li>
<li><a href="#downsides-of-the-fund-to-release-model">Downsides of the fund-to-release model</a>
<ul>
<li><a href="#increased-financial-risk-for-developers">1. Increased financial risk for developers</a></li>
<li><a href="#freeloading-or-hesitation">2. Freeloading or hesitation</a></li>
<li><a href="#delayed-code-review">3. Delayed code review</a></li>
<li><a href="#maintainer-resentment">4. Maintainer resentment</a></li>
<li><a href="#user-fatigue">5. User fatigue</a></li>
<li><a href="#potential-scams">6. Potential for scams</a></li>
</ul>
</li>
<li><a href="#other-considerations">Other considerations</a>
<ul>
<li><a href="#gaming-the-system">Gaming the system</a></li>
<li><a href="#how-the-crowd-affects-crowdfunding">How the crowd affects crowdfunding</a></li>
</ul>
</li>
<li><a href="#epilogue">Epilogue</a></li>
</ul>
</div>
</nav>
<section id="outline-container-introduction" class="outline-2">
<h2 id="introduction"><a href="#introduction">Introduction</a></h2>
<div class="outline-text-2" id="text-introduction">
<p>
I always get the impression that nobody has a definitive answer on how to make a living by contributing to freedom-respecting software (nor free data, nor free cultural works).
</p>

<p>
Corporate "open source" projects often choose the "open core" model (monetizing proprietary extensions to free software), which results in continued reliance on proprietary software, and is antithetical to the ideals of software freedom activists. A number of prominent examples are listed on <a href="https://en.wikipedia.org/wiki/Open-core_model#Examples">en.wikipedia.org/wiki/Open-core​_model#Examples</a>.
</p>

<p>
Matthew Miller (the Fedora Project Leader) recently cited the open core model as one of the reasons for <a href="https://fedoramagazine.org/fedora-moves-towards-forgejo-a-unified-decision/">Fedora choosing Forgejo over GitLab CE</a> -
</p>

<blockquote>
<p>
Open core software tends, over time, to get less open (no matter how good the initial intentions).
</p>
</blockquote>

<p>
Another monetization model is to provide paid support or services (like a chat client being free but the chat account requiring a subscription to use), but that doesn't work for all kinds of software.
</p>

<p>
A number of developers sell binaries on proprietary app stores like Google Play Store and Apple App Store. One example is <a href="https://osmand.net/">OsmAnd+</a>. While I don't know how successful this approach is, I certainly can say as a software freedom activist that it poses an obstacle to introduce people to these apps, especially on iOS where F-Droid doesn't exist and sideloading is unheard of. Most people are reluctant to pay for software, even if it respects your freedom.
</p>

<p>
Besides, this model is undermined by others compiling and/or publishing binaries themselves, such as app stores like F-Droid. And restricting that means curtailing the four software freedoms.
</p>

<p>
That leaves recurring donations, like Patreon, <a href="https://liberapay.com/">Liberapay</a>, GitHub Sponsors, and variations on the idea like <a href="https://snowdrift.coop/">snowdrift.coop</a>. But it's often observed that the number of donors on such platforms is only a small fraction of the number of users. Some point to these numbers as "proof" that you can't make a living working on free software.
</p>

<p>
I've tried to fund my free software, data, and cultural contributions using Liberapay, without much success. Part of it might be poor marketing, but there are also some issues I've observed which are inherent to the recurring donations model -
</p>

<ol class="org-ol">
<li>It requires you to be a consistent contributor to specific projects. <label id='fnr.1' for='fnr-in.1.9790334' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.9790334' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
These projects also need to be <i>popular</i>. See <a href="#how-the-crowd-affects-crowdfunding">How the crowd affects crowdfunding</a>.
</span> That doesn't work if your interests change as often as mine.</li>

<li>There's no way for users to know exactly how their money will be used, or whether it will result in any improvements they care about at all.</li>

<li>There's no incentive for the user to donate. They've already got the software, and the better future for the software (weakly promised by the recurring donation model) is nebulous.</li>

<li>Recurring donations are "yet another subscription". With companies trying to make everything into a subscription, people may be tired of subscriptions everywhere and may not want to add another one, even if it's for libre software/data/culture.</li>
</ol>
</div>
</section>
<section id="outline-container-the-idea-focused-fixed-goal-crowdfunding" class="outline-2">
<h2 id="the-idea-focused-fixed-goal-crowdfunding"><a href="#the-idea-focused-fixed-goal-crowdfunding">The idea - focused, fixed-goal crowdfunding</a></h2>
<div class="outline-text-2" id="text-the-idea-focused-fixed-goal-crowdfunding">
<p>
Recently, I thought of a different way to fund free software. I'll now describe two fundraising models, both of which seem promising to me. The crux of the idea is - <i>don't release the work in any form until it's funded.</i>
</p>
</div>
<div id="outline-container-the-fund-to-develop-model" class="outline-3">
<h3 id="the-fund-to-develop-model"><a href="#the-fund-to-develop-model">The fund-to-develop model</a></h3>
<div class="outline-text-3" id="text-the-fund-to-develop-model">
<ol class="org-ol">
<li>You announce that you would like to implement a bugfix or feature (or even an app). Mention the time it will take you, and set a corresponding crowdfunding target.</li>

<li>When the target is met, implement and release the work.</li>
</ol>

<p>
This is the classic Kickstarter/Indiegogo-style fixed-goal crowdfunding, scaled down to the bug/feature level.
</p>

<p>
It's how <a href="https://prav.app/">Prav</a> funded the custom username feature in their fork of the Quicksy XMPP client - they got a quote from a software consultancy firm, raised the funds, and paid the company.
</p>

<p>
Members of the Prav community are also behind <a href="https://xmpp.link/#debphoshfund@chat.disroot.org?join">DebPhoshFund</a>, trying to crowdfund improvements for mobile GNU/Linux applications in the same manner.
</p>
</div>
<div id="outline-container-a-variation-on-the-fund-to-develop-model" class="outline-4">
<h4 id="a-variation-on-the-fund-to-develop-model"><a href="#a-variation-on-the-fund-to-develop-model">A variation on the fund-to-develop model</a></h4>
<div class="outline-text-4" id="text-a-variation-on-the-fund-to-develop-model">
<p>
Crowdfund some amount (e.g. 50% of the costs) in advance, and the rest on completion. If the initial funding succeeds, the developer can begin. The software can be released once the rest is funded.
</p>

<p>
This way, risk is reduced for both sides - people don't pay everything up-front and end up without results, and the developer doesn't end up doing all the work without getting paid.
</p>
</div>
</div>
</div>
<div id="outline-container-the-fund-to-release-model" class="outline-3">
<h3 id="the-fund-to-release-model"><a href="#the-fund-to-release-model">The fund-to-release model</a></h3>
<div class="outline-text-3" id="text-the-fund-to-release-model">
<ol class="org-ol">
<li>You implement an application, bugfix, or a feature (while keeping track of the time it took), without publishing the source code.</li>
<li>Announce when you are done, and set a crowdfunding goal based on the time it took.</li>
<li>When the goal is met, release the code and make the PR.</li>
</ol>

<p>
<a href="https://wiki.snowdrift.coop/market-research/history/software#ransom-systems">The snowdrift.coop wiki</a> <label id='fnr.2' for='fnr-in.2.2856921' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.2856921' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
What the Snowdrift wiki describes is 'meet the threshold and the code becomes free rather than proprietary', whereas what I'm describing is 'meet the threshold, or you get no code'.
</span> calls this the <i>ransom model</i> or the <i>Street Performer Protocol</i>, although the Wikipedia descriptions of these terms differ slightly from what I'm describing.
</p>

<p>
In practice, there are some more steps involved, but the structure remains the same -
</p>
<ol class="org-ol">
<li>Discuss the app/feature/fix with users to understand their needs.</li>

<li><p>
In case of features or fixes, inform the maintainers about the intended changes. Discuss the desirability and the design.
</p>

<p>
Announce that you are working on it, to prevent duplicate efforts.
</p></li>

<li>Begin implementing the feature/fix. Track the time it takes. For long tasks, provide progress updates.</li>

<li>When finished, calculate the ransom amount based on the time you spent, and (if applicable) the time the maintainers spent helping you.</li>

<li>Announce the app/feature/fix and the ransom amount to users. Show demos in the form of screenshots or screencasts.<label id='fnr.3' for='fnr-in.3.572549' class='margin-toggle sidenote-number'><sup class='numeral'>3</sup></label><input type='checkbox' id='fnr-in.3.572549' class='margin-toggle'><span class='sidenote'><sup class='numeral'>3</sup>
<a href="https://pranavats.gitlab.io/">pranavats</a> suggested that one may also release an obfuscated binary as the proof of work, "and pray [that] nobody bothers reversing it."
</span>
<ul class="org-ul">
<li>If funding succeeds, make a PR and go through review. Pay the maintainers before or after review.</li>
<li>If funding does not succeed, optionally release the code or the money after some time.</li>
</ul></li>

<li>If the PR is merged - success! If the PR is not merged, make your code available in a fork.</li>
</ol>

<p>
This, too, is hardly a revolutionary idea - it's how freelancers work across the world. But somehow, it's not very popular in the free software world. The Snowdrift Wiki cites some examples of its use (e.g. liberating Blender as a whole), but never in the way I have described - that is, applied at the feature/bugfix level, and making releasing the work itself contingent on funding, rather than only changing its licensing.
</p>
</div>
</div>
</section>
<section id="outline-container-the-benefits-of-the-ransom-model" class="outline-2">
<h2 id="the-benefits-of-the-ransom-model"><a href="#the-benefits-of-the-ransom-model">Benefits of the fund-to-release model</a></h2>
<div class="outline-text-2" id="text-the-benefits-of-the-ransom-model">
</div>
<div id="outline-container-greater-incentive-to-pay" class="outline-3">
<h3 id="greater-incentive-to-pay"><a href="#greater-incentive-to-pay">1. Greater incentive to pay</a></h3>
<div class="outline-text-3" id="text-greater-incentive-to-pay">
<p>
Users are incentivized to pay, because they don't get the feature/fix if they don't.
</p>

<p>
For me, this is the biggest benefit, and it compensates for any downsides.
</p>
</div>
</div>
<div id="outline-container-user-control-over-development" class="outline-3">
<h3 id="user-control-over-development"><a href="#user-control-over-development">2. User control over development</a></h3>
<div class="outline-text-3" id="text-user-control-over-development">
<p>
Compared to recurring donations, users have finer-grained control over how their money is used.
</p>
</div>
</div>
<div id="outline-container-lowering-obstacles-to-paid-contribution" class="outline-3">
<h3 id="lowering-obstacles-to-paid-contribution"><a href="#lowering-obstacles-to-paid-contribution">3. Lowering obstacles to paid contribution</a></h3>
<div class="outline-text-3" id="text-lowering-obstacles-to-paid-contribution">
<p>
If the developer is not confident in their skills, the fund-to-release model allows them to successfully complete the task first, without publicly committing to it by crowdfunding it in advance.
</p>
</div>
</div>
<div id="outline-container-user-focused-development" class="outline-3">
<h3 id="user-focused-development"><a href="#user-focused-development">4. User-focused development</a></h3>
<div class="outline-text-3" id="text-user-focused-development">
<p>
Developers will focus on features/fixes most likely to be funded by the users.
</p>
</div>
</div>
<div id="outline-container-higher-likelihood-of-delivery" class="outline-3">
<h3 id="higher-likelihood-of-delivery"><a href="#higher-likelihood-of-delivery">5. Higher likelihood of delivery</a></h3>
<div class="outline-text-3" id="text-higher-likelihood-of-delivery">
<p>
There is no question of budget overruns or failure to deliver - the work is already done. Users have a much greater assurance of getting the feature/fix.
</p>

<p>
(In contrast, there have been plenty of Kickstarter-style campaigns that were successfully funded, but failed to deliver results.)
</p>
</div>
</div>
<div id="outline-container-developers-reach-out-to-users" class="outline-3">
<h3 id="developers-reach-out-to-users"><a href="#developers-reach-out-to-users">6. Developers reach out to users</a></h3>
<div class="outline-text-3" id="text-developers-reach-out-to-users">
<p>
Unlike bounties, users are not required to reach out to a platform to put up a bounty. (Very few users will do that.) Instead, the developer reaches out to users. This could also be done through the application itself (although it's kind of like an advertisement).
</p>
</div>
</div>
</section>
<section id="outline-container-downsides-of-the-fund-to-release-model" class="outline-2">
<h2 id="downsides-of-the-fund-to-release-model"><a href="#downsides-of-the-fund-to-release-model">Downsides of the fund-to-release model</a></h2>
<div class="outline-text-2" id="text-downsides-of-the-fund-to-release-model">
</div>
<div id="outline-container-increased-financial-risk-for-developers" class="outline-3">
<h3 id="increased-financial-risk-for-developers"><a href="#increased-financial-risk-for-developers">1. Increased financial risk for developers</a></h3>
<div class="outline-text-3" id="text-increased-financial-risk-for-developers">
<p>
The developer is at risk of not getting paid for work they've already done.
</p>
</div>
</div>
<div id="outline-container-freeloading-or-hesitation" class="outline-3">
<h3 id="freeloading-or-hesitation"><a href="#freeloading-or-hesitation">2. Freeloading or hesitation</a></h3>
<div class="outline-text-3" id="text-freeloading-or-hesitation">
<p>
If the ransom has a fixed expiry date (i.e. when the code is released regardless of ransom), people may wait for it to run out instead of paying.
</p>

<ul class="org-ul">
<li>The OpenAV model (which the Snowdrift Wiki cites as a failure of this model) used an expiry date, which naturally reduces the chances of successful funding - the freeloaders can just wait it out, as they already have a working older version and are assured of the new version being released eventually.</li>
</ul>

<p>
If there is no expiry date (or if it's too far away), people may hesitate to chip in, since their money could be held up without results until others chip in (or the ransom expires).
</p>

<ul class="org-ul">
<li>Theoretically, the developer could return incomplete ransoms after a while, without releasing the code. But that would mean being wholly uncompensated for their work - I doubt they would accept that.</li>
</ul>

<p>
This is why the initial community input phase is necessary - it prevents such stalemates by ensuring the demand exists.
</p>

<p>
It's also why fine-grained feature/bugfix-oriented  ransoms are helpful. OpenAV tried to apply this to software <i>releases</i>, which is too broad.
</p>

<p>
A matchfunding platform like Snowdrift could also help, but I don't think the Prisoner's Dilemma is the biggest factor here.
</p>
</div>
</div>
<div id="outline-container-delayed-code-review" class="outline-3">
<h3 id="delayed-code-review"><a href="#delayed-code-review">3. Delayed code review</a></h3>
<div class="outline-text-3" id="text-delayed-code-review">
<p>
When the code contributor is not part of the project, code may not get reviewed until the payments are in and the code is released. Maintainers may not merge the PR, e.g. for design or code quality reasons. That could waste the users' money, and deter future payments.
</p>

<ul class="org-ul">
<li>The developer could always publish the changes in a separate repository. But users are more concerned with changes being merged into upstream so they can (easily) use the feature they paid for.</li>

<li><p>
I suppose this also includes <a href="https://wiki.snowdrift.coop/market-research/history/software#ransom-systems">snowdrift.coop's concerns</a> about the fund-to-release model disincentivizing collaboration.
</p>

<p>
But I'm fine with that. The software remains freedom-respecting and the developers get paid. The collaboration, if any, happens after the developers are paid and the code is liberated. (Even as things stand, collaboration in the free software world is rare and most projects have a bus factor of 1.)
</p></li>
</ul>
</div>
</div>
<div id="outline-container-maintainer-resentment" class="outline-3">
<h3 id="maintainer-resentment"><a href="#maintainer-resentment">4. Maintainer resentment</a></h3>
<div class="outline-text-3" id="text-maintainer-resentment">
<p>
When the code contributor is not part of the project, maintainers may feel resentful of someone making money off their project while they don't.
</p>

<p>
One could add on a sum to the ransom which will be given to the maintainers. This could include -
</p>
<ul class="org-ul">
<li>their help in implementing the feature/fix.</li>
<li>any code review required for merging</li>
<li>past unpaid labour they invested into the codebase.</li>
</ul>

<p>
Alternatively, maintainers themselves could set a project-wide percent to be charged for reviewing (but not necessarily accepting) any paid PRs.
</p>
</div>
</div>
<div id="outline-container-user-fatigue" class="outline-3">
<h3 id="user-fatigue"><a href="#user-fatigue">5. User fatigue</a></h3>
<div class="outline-text-3" id="text-user-fatigue">
<p>
Users may get tired of being regularly notified of new ransoms for new features, and payments may decline. In this perspective, subscriptions require less thought&#x2026;
</p>
</div>
</div>
<div id="outline-container-potential-scams" class="outline-3">
<h3 id="potential-scams"><a href="#potential-scams">6. Potential for scams</a></h3>
<div class="outline-text-3" id="text-potential-scams">
<p>
It's possible to scam users by sharing fabricated screenshots or screencasts. Like any business transaction, the fund-to-release model requires trust, and it helps to have a positive reputation.
</p>

<p>
Alternatively, an escrow platform could be made and some sort of verification of work could be performed before funds are released.
</p>
</div>
</div>
</section>
<section id="outline-container-other-considerations" class="outline-2">
<h2 id="other-considerations"><a href="#other-considerations">Other considerations</a></h2>
<div class="outline-text-2" id="text-other-considerations">
</div>
<div id="outline-container-gaming-the-system" class="outline-3">
<h3 id="gaming-the-system"><a href="#gaming-the-system">Gaming the system</a></h3>
<div class="outline-text-3" id="text-gaming-the-system">
<p>
Some may claim that feature/bugfix-focused funding models incentivize the creation of half-baked/deliberately feature-incomplete software, to allow for greater ransoms for popular features in the future.
</p>

<p>
There may be people who will try that, but I doubt they will succeed. Feature/bugfix-focused crowdfunding relies on the software being useful to many people - a half-baked application won't acquire the critical mass of donors to begin with.
</p>
</div>
</div>
<div id="outline-container-how-the-crowd-affects-crowdfunding" class="outline-3">
<h3 id="how-the-crowd-affects-crowdfunding"><a href="#how-the-crowd-affects-crowdfunding">How the crowd affects crowdfunding</a></h3>
<div class="outline-text-3" id="text-how-the-crowd-affects-crowdfunding">
<p>
Crowdfunding always works better with a bigger crowd, and the ideas I have described are no different. All forms of crowdfunding favor popular projects with lots of users.
</p>

<p>
This is not necessarily a bad thing. There's a lot of complaining about duplication of effort in the free software world, and this works to hinder it.
</p>

<p>
But with feature/fix-focused models, it can also mean that only the most popular features/bugs are likely to be implemented/fixed. Less-popular features/fixes place greater strain on individual users, or will pay less. Code improvements that are not user-facing (such as refactoring, library migration, etc) are less likely to be funded, unless you can convince users of its necessity.
</p>

<p>
Developers could counteract this by clubbing costs for less-popular changes into more popular ones.
</p>
</div>
</div>
</section>
<section id="outline-container-epilogue" class="outline-2">
<h2 id="epilogue"><a href="#epilogue">Epilogue</a></h2>
<div class="outline-text-2" id="text-epilogue">
<p>
So those were my thoughts on some ideas which seem promising to me. Why aren't these models used more often? I intend to make use of them and find out.
</p>



<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="unexpected-opposition-to-free-software-advocacy.html">← Previous: Unexpected opposition to free software advocacy</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="portable-computer.html">→ Next: Design notes for my ideal portable computer</a></div>
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
