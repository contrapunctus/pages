<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2025-02-08 Sat 00:24 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Design notes for my ideal portable computer</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">Design notes for my ideal portable computer</h1>
</header><p>
 <nav class="navlinks">
<div class="navlink previous"><a href="a-different-way-to-fund-freedom-respecting-software.html">← Previous: A different way to fund freedom-respecting software</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="why-prav-is-important.html">→ Next: Why Prav is Important</a></div>
 </nav>
</p>

<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#user-needs">Me and my mania for mobility</a></li>
<li><a href="#design-goals">Design goals</a>
<ul>
<li><a href="#portability">Portability (obviously)</a></li>
<li><a href="#typing-experience">A proper typing experience</a></li>
<li><a href="#ergonomics">Ergonomics</a></li>
<li><a href="#durability-and-repairability">Durability and Repairability</a></li>
<li><a href="#software">Software</a></li>
<li><a href="#good-io">Good I/O</a></li>
<li><a href="#materials">Materials</a></li>
</ul>
</li>
<li><a href="#what-it-might-look-like">What it might look like</a>
<ul>
<li><a href="#modularity">Modularity</a></li>
<li><a href="#limitations">Limitations</a></li>
<li><a href="#folding-display">A folding display as an alternative to AR glasses</a></li>
</ul>
</li>
<li><a href="#existing-bits-and-pieces">Existing bits and pieces</a>
<ul>
<li><a href="#laptops">Laptops</a></li>
<li><a href="#ar-glasses">AR glasses</a></li>
<li><a href="#handhelds">Handhelds</a></li>
<li><a href="#wearable-keyboards">Wearable keyboards</a></li>
</ul>
</li>
<li><a href="#what-now">What now?</a></li>
</ul>
</div>
</nav>
<section id="outline-container-user-needs" class="outline-2">
<h2 id="user-needs"><a href="#user-needs">Me and my mania for mobility</a></h2>
<div class="outline-text-2" id="text-user-needs">
<p>
I love computers. Most if not all of my work happens on a computer of some kind -
</p>

<ol class="org-ol">
<li>Taking notes.</li>
<li>Sending messages over <a href="https://contrapunctus.codeberg.page/xmpp.html">XMPP</a>.</li>
<li>Reading. I much prefer ebooks to dead-tree books. I can customize what they look like, search their contents, quickly jump to sections, make backups of them&#x2026;</li>
<li>Writing prose or poetry</li>
<li>Contributing to OpenStreetMap</li>
<li>Programming</li>
<li>Composing music</li>
<li>Practicing singing and guitar (primarily for musical scores, and accompaniment for singing)</li>
<li>Tracking time for various activities</li>
<li>Watching videos and listening to music</li>
</ol>

<p>
So, for much of the day, I'm working on a computer of some kind.
</p>

<p>
As an aside - I never could understand the numerous people in the world who talk about "digital detox" and limiting screen time. I once came across this line (paraphrased from memory) on a transhumanist-leaning corner of the Internet, and to this day I greatly identify with it -
</p>

<blockquote>
<p>
"If the Internet is an extension of our mind, then to be offline is to suffer brain damage."
</p>
</blockquote>

<p>
(Does anybody know the source? Please <a href="../contact.html">share it!</a> I've looked in vain on many occasions.)
</p>

<p>
Moreover, when an idea takes hold of me, I usually work on it ceaselessly, from the moment I wake to the moment sleep finally overcomes me. But I also can't sit at my laptop for too long without getting restless and fidgety.
</p>

<p>
This makes my phone an indispensable tool for my work. With a phone, I can walk around as I work (without needing a specialized walking desk). With the help of Syncthing, my work is seamlessly synchronized between my laptop and phone, and I switch between the two depending on whether I'm sitting or standing at the laptop, or using the phone to work as I sit, stand, or walk&#x2026;or even lie in bed.
</p>

<p>
But phones - and all existing portable computers, really - have several issues. I describe these issues below, as well as what a solution could look like.
</p>
</div>
</section>
<section id="outline-container-design-goals" class="outline-2">
<h2 id="design-goals"><a href="#design-goals">Design goals</a></h2>
<div class="outline-text-2" id="text-design-goals">
</div>
<div id="outline-container-portability" class="outline-3">
<h3 id="portability"><a href="#portability">Portability (obviously)</a></h3>
<div class="outline-text-3" id="text-portability">
<p>
An ideal portable computer should be usable in any situation, whether you are walking (e.g. on the street), sitting or standing (even in a packed bus, train, or plane), or lying in bed.
</p>

<p>
Keeping the weight down is also a goal, but secondary to the others listed.
</p>
</div>
</div>
<div id="outline-container-typing-experience" class="outline-3">
<h3 id="typing-experience"><a href="#typing-experience">A proper typing experience</a></h3>
<div class="outline-text-3" id="text-typing-experience">
<p>
Most smartphones don't have a physical keyboard, which results in poor typing feedback, poor typing speed (because you can only use two thumbs instead of ten fingers), and the inability to touch type.
</p>

<p>
Even on devices with a physical keyboard (like the Dragonboard Pyra, the Astro Slide, the MutantC, and others), you are limited to using two thumbs instead of 10 fingers.
</p>

<p>
While most laptops have a full physical keyboards, they are membrane keyboards with terrible typing feedback. The <a href="#laptops">MNT Reform</a> might be the only exception.
</p>

<p>
Some suggest voice typing as an alternative, but it's susceptible to ambient noise, and broadcasts your actions to everyone around you, which causes disturbance and is a privacy issue.
</p>

<p>
An ideal portable computer should have a split, ortholinear mechanical keyboard for the best ergonomics and typing performance. You deserve good typing feedback, to be able to type using all ten fingers, and to be able to <i>look at what you are typing</i>, rather than <i>having to look at the keys!</i>
</p>

<p>
(I also don't mind a chorded keyboard like a Twiddler. But it has a high learning curve, whereas I want to make something which has <i>some</i> mass appeal, so I want to explore full-size split keyboards first.)
</p>
</div>
</div>
<div id="outline-container-ergonomics" class="outline-3">
<h3 id="ergonomics"><a href="#ergonomics">Ergonomics</a></h3>
<div class="outline-text-3" id="text-ergonomics">
<p>
When you're working on a computer as much as I am, even minor ergonomic issues become painfully amplified. And portable computers in their current form have some glaring ergonomic issues.
</p>

<p>
In most portable devices - smartphones, handhelds, and laptops - the screen and the controls are combined into a single unit. Usually, this means that you have to look down, straining your eyes and/or neck and/or back. Or you can try holding the phone up to keep the eyes and neck at a neutral position, but then your arms will hurt after a while. In any case, your chest also adopts a concave posture. Meh.
</p>

<p>
Sure, I could take frequent breaks, stretch while working, and so on. But where's the fun in that? I want to be able to work continuously, without breaking my flow, and without posture-related injuries. And improving ergonomics in harmony with the other constraints is an interesting problem.
</p>

<p>
So, in an ideal portable computer -
</p>

<ol class="org-ol">
<li>The screen and controls should be separable, so the eyes, neck, and back are in a relaxed and natural posture.</li>

<li>The controls should also be separable. (Hence the split keyboard.)</li>
</ol>
</div>
</div>
<div id="outline-container-durability-and-repairability" class="outline-3">
<h3 id="durability-and-repairability"><a href="#durability-and-repairability">Durability and Repairability</a></h3>
<div class="outline-text-3" id="text-durability-and-repairability">
<p>
Phones (and laptops) are notoriously difficult to repair. Sooner or later companies stop providing warranty for them, and then you're on your own.
</p>

<p>
An ideal portable computer should be a robust and "buy it for life" device, made with easily-available components. It should be open hardware, so users can make their own replacement parts.
</p>

<p>
In addition, most consumer hardware is susceptible to water. Many an OSM survey has been halted because of rain. An ideal portable computer should be weather-proof. (Probably something for the longer run.)
</p>
</div>
</div>
<div id="outline-container-software" class="outline-3">
<h3 id="software"><a href="#software">Software</a></h3>
<div class="outline-text-3" id="text-software">
<p>
Smartphones typically run Android, developed by Google&#x2026;who continually makes developments which make life worse for power users, privacy-conscious users, and software freedom proponents. SafetyNet, <label id='fnr.1' for='fnr-in.1.4993595' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.4993595' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
helping banking apps stop working if your phone is rooted or has a de-Googled ROM
</span> Scoped Storage <label id='fnr.2' for='fnr-in.2.1716693' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.1716693' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
Making it impossible for applications to access the filesystem, <i>even with root!</i>
</span>&#x2026;
</p>

<p>
Android is not a great for developers either, who often complain about the changes Google imposes.
</p>

<p>
An ideal portable computer should support venerable community-made free software operating systems like GNU/Linux and BSD, and all the software they support.
</p>
</div>
</div>
<div id="outline-container-good-io" class="outline-3">
<h3 id="good-io"><a href="#good-io">Good I/O</a></h3>
<div class="outline-text-3" id="text-good-io">
<p>
The I/O on most phones is limited to Bluetooth, WiFi, and a USB Type-C port. We should do better.
</p>
</div>
</div>
<div id="outline-container-materials" class="outline-3">
<h3 id="materials"><a href="#materials">Materials</a></h3>
<div class="outline-text-3" id="text-materials">
<p>
The production, use, and disposal of the materials should cause the least possible damage to the world we live in.
</p>

<p>
Plastic should be avoided. Wood and metal should be preferred.
</p>
</div>
</div>
</section>
<section id="outline-container-what-it-might-look-like" class="outline-2">
<h2 id="what-it-might-look-like"><a href="#what-it-might-look-like">What it might look like</a></h2>
<div class="outline-text-2" id="text-what-it-might-look-like">
<p>
Think of a handheld similar in size to a Steam Deck, but it's made of three modules -
</p>

<ol class="org-ol">
<li>The 2 outer modules are 6×4 vertical split keyboards with two trackballs and 4-8 thumb keys on each side. They have hand straps (like the <a href="#wearable-keyboards">GrabShell keyboard</a>, or a concertina/bandoneon) to allow you to hold it up without using your fingers. The straps also make it difficult to snatch.</li>

<li>The center module can be -

<ul class="org-ul">
<li>A dock to hold a smartphone in vertical or horizontal orientation. It could also have a power bank and wireless charging to charge the phone while docked.</li>

<li>The center module could be replaced with a compute module, containing a battery, SBC, display, SSD, ports, speakers, etc. The display can be as small or large as the user can tolerate.</li>
</ul></li>
</ol>
</div>
<div id="outline-container-modularity" class="outline-3">
<h3 id="modularity"><a href="#modularity">Modularity</a></h3>
<div class="outline-text-3" id="text-modularity">
<p>
The modules could be attached using magnets or other fasteners, allowing for the following modes of use -
</p>

<ol class="org-ol">
<li>Unified mode. All three modules are attached together, forming something like a large handheld. The device is held using the palm straps. The width of the center module allows the user's chest to be open. The center module or the AR glasses could be used as the display.</li>

<li>Split mode. The center module is detached and placed on a table to act as the display. The keyboard modules can be attached to each other directly, or used in a split configuration. Useful on flights and trains (chair cars), where there's not much lateral space, but a table is available to place the center module.</li>

<li>AR mode. Only the keyboard modules are used, either split or attached. The center module is placed in a pocket or bag. AR glasses are the sole means of display.</li>
</ol>

<p>
Like the GrabShell, it should stay upright if you put it down on a desk, allowing you to easily put it down and pick it up again.
</p>
</div>
</div>
<div id="outline-container-limitations" class="outline-3">
<h3 id="limitations"><a href="#limitations">Limitations</a></h3>
<div class="outline-text-3" id="text-limitations">
<ol class="org-ol">
<li>You can't put it into a pocket like a phone.
<ul class="org-ul">
<li>That's also good because it's impossible for a pickpocket to hide it.</li>
<li>It may be useful to add a strap to it (like a sling bag), so you can let go of it when you want to quickly free up your hands.</li>
</ul></li>

<li>It probably won't be easy to remove your hands from the wrist straps when the keyboard modules are split. You'd probably have to connect them to the center module or to each other first, so one hand can hold the device still while you free the other hand from the wrist strap.</li>
</ol>
</div>
</div>
<div id="outline-container-folding-display" class="outline-3">
<h3 id="folding-display"><a href="#folding-display">A folding display as an alternative to AR glasses</a></h3>
<div class="outline-text-3" id="text-folding-display">
<p>
AR glasses are not always affordable or appropriate, so this display would be a useful addition.
</p>

<p>
This "display" has two arms at its sides. (If using a phone for compute and display, this can be a kind of smartphone holder.)
</p>

<p>
When folded down, the display and the arms are flush with the device body, allowing it to be used in a compact (but not very ergonomic) configuration.
</p>

<p>
Or it can be unfolded for better ergonomics (at the expense of vertical space) -
</p>
<ol class="org-ol">
<li>You unfold the arms upwards. The display now faces away from you.</li>
<li>You unfold the display upwards. The display now faces you, and should require less bending of the neck to use.</li>
</ol>

<p>
(Unfolding is really a one-step operation - you just pull the screen into the higher position. The two steps above are only to explain its design.)
</p>
</div>
</div>
</section>
<section id="outline-container-existing-bits-and-pieces" class="outline-2">
<h2 id="existing-bits-and-pieces"><a href="#existing-bits-and-pieces">Existing bits and pieces</a></h2>
<div class="outline-text-2" id="text-existing-bits-and-pieces">
</div>
<div id="outline-container-laptops" class="outline-3">
<h3 id="laptops"><a href="#laptops">Laptops</a></h3>
<div class="outline-text-3" id="text-laptops">
<ol class="org-ol">
<li><a href="https://mntre.com/reform.html">MNT Reform</a></li>
<li><a href="https://www.byran.ee/posts/creation/">anyon<sub>e</sub></a> (<a href="https://github.com/Hello9999901/laptop">GitHub</a>)</li>
</ol>
</div>
</div>
<div id="outline-container-ar-glasses" class="outline-3">
<h3 id="ar-glasses"><a href="#ar-glasses">AR glasses</a></h3>
<div class="outline-text-3" id="text-ar-glasses">
<ol class="org-ol">
<li>XReal AR glasses
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=ZQJqPD3sckU">XReal Air 2 Pro  AR glasses — Spencer Scott Pugh</a></li>
</ul></li>
<li>Rokid Max 2 AR</li>
<li><a href="https://www.youtube.com/watch?v=PwmGNWkpKW8">Open Source Smart Glasses - DIY AR — Cayden Pierce</a>
<ul class="org-ul">
<li><a href="https://github.com/CaydenPierce/OpenSourceSmartGlasses">https://github.com/CaydenPierce/OpenSourceSmartGlasses</a></li>
</ul></li>
<li><a href="https://brilliant.xyz/">https://brilliant.xyz/</a></li>
</ol>

<p>
Problems
</p>
<ol class="org-ol">
<li>Some look too conspicuous
<ul class="org-ul">
<li>the XReal's are merely a little bulky</li>
<li>the Rokid looks a little weird</li>
<li>the Apple Vision Pro looks insanely conspicuous</li>
</ul></li>
<li>In many cases, people nearby are able to see what is displayed on the AR glasses.</li>
<li>Safety/situational awareness (theft, assault, &#x2026;)</li>
<li>Most such glasses require a cable (I don't mind too much)</li>
</ol>
</div>
</div>
<div id="outline-container-handhelds" class="outline-3">
<h3 id="handhelds"><a href="#handhelds">Handhelds</a></h3>
<div class="outline-text-3" id="text-handhelds">
<ol class="org-ol">
<li><a href="https://pyra-handheld.com/boards/pages/pyra/">DragonBox Pyra</a></li>
<li><a href="https://mutantcybernetics.com/">MutantC</a></li>
<li><a href="https://mecha.so/comet">Mecha Comet</a></li>
</ol>
</div>
</div>
<div id="outline-container-wearable-keyboards" class="outline-3">
<h3 id="wearable-keyboards"><a href="#wearable-keyboards">Wearable keyboards</a></h3>
<div class="outline-text-3" id="text-wearable-keyboards">
<p>
Twiddler
</p>
<ul class="org-ul">
<li>It is designed for one hand, and relies on keychording.</li>
</ul>

<p>
DataHand/Lalboard/Svalboard (?)
</p>

<p>
Grab Shell keyboard
</p>
<ul class="org-ul">
<li>A <a href="https://www.youtube.com/watch?v=O7p68Gxxlfo">review of the Grab Shell keyboard</a> on YouTube</li>
</ul>

<p>
<a href="https://old.reddit.com/comments/qvpekz">I made a&#x2026;thing</a> (u/ExtremePocket in r/ErgoMechKeyboards)
</p>
</div>
</div>
</section>
<section id="outline-container-what-now" class="outline-2">
<h2 id="what-now"><a href="#what-now">What now?</a></h2>
<div class="outline-text-2" id="text-what-now">
<p>
I'll probably have to learn CAD to make this happen. Also, I should get a job 😑
</p>



<p>
 <nav class="navlinks">
<div class="navlink previous"><a href="a-different-way-to-fund-freedom-respecting-software.html">← Previous: A different way to fund freedom-respecting software</a></div> <a href="index.html">↩ blog</a> <div class="navlink next"><a href="why-prav-is-important.html">→ Next: Why Prav is Important</a></div>
 </nav>
</p>

<p>
 <footer class="links">
<a href="../contact.html">Send me a comment</a><br>
•<br>
<a href="../support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
