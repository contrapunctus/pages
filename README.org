Source code for my personal website.

If you feel like helping me improve it, there are a number of tasks in the [[file:TODO.org]].

There's also probably a lot that could be done to improve accessibility, so do open an issue (for specific tasks) or a PR.

The HTML files are automatically generated from the Org files, and should not be edited by hand.

Org files are exported to HTML automatically after being saved in Emacs. The .dir-locals.el files set this up for you.
