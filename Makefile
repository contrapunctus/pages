.phony: publish-html

publish-html:
	emacs --batch --eval="(require 'ox-publish)" \
        --eval='(org-publish "tilde.team-html")'
