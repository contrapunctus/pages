* Footer
:PROPERTIES:
:CUSTOM_ID: footer
:END:
@@html: <footer class="links">@@
[[file:../contact.org][Send me a comment]]\\
•\\
[[file:../support.org][Buy me a coffee]]\\
•\\
[[https://codeberg.org/contrapunctus/pages][Study or improve this website on Codeberg]]\\
@@html: </footer>@@
