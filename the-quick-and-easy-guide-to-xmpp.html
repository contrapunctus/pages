<!DOCTYPE html>
<html lang="en">
<head>
<!-- 2025-02-15 Sat 02:09 -->
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>The Quick and Easy Guide to Jabber/XMPP</title>
<meta name="author" content="contrapunctus" />
<meta name="generator" content="Org Mode" />
<link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<article id="content" class="content">
<header>
<h1 class="title">The Quick and Easy Guide to Jabber/XMPP</h1>
<p class="subtitle" role="doc-subtitle">Essential information and resources for new Jabber/XMPP users</p>
</header><p>
<a href="index.html">↩ home</a>
</p>

<p>
 <div class="center">
</p>
<p loading="lazy">
<img src="img/xmpp/XMPP_logo_(without_text).svg" alt="XMPP_logo_(without_text).svg" class="org-svg" loading="lazy">
 </div>
</p>

<nav id="table-of-contents" role="doc-toc">
<h2>Table of Contents</h2>
<div id="text-table-of-contents" role="doc-toc">
<ul>
<li><a href="#why-xmpp-matters">Why XMPP matters</a>
<ul>
<li><a href="#modern-chat-features">Modern chat features</a></li>
<li><a href="#federated-not-centralized">Federated, not centralized</a></li>
<li><a href="#freedom-respecting-software">Freedom-respecting software</a></li>
<li><a href="#easy-to-self-host">Easy to self-host</a></li>
<li><a href="#community-governed-protocol">Community-governed protocol</a></li>
<li><a href="#bridges">Bridges</a></li>
</ul>
</li>
<li><a href="#getting-started">Getting started</a>
<ul>
<li><a href="#easiest-way">The easiest way</a></li>
<li><a href="#select-a-server">Select a server and make an account</a></li>
<li><a href="#installing-a-client">Install a client</a></li>
<li><a href="#getting-help">Getting help</a></li>
</ul>
</li>
<li><a href="#sharing-xmpp-links">Sharing XMPP links</a></li>
<li><a href="#channels-and-group-chats">Channels and group chats</a>
<ul>
<li><a href="#semi-anonymous-and-non-anonymous-mucs">Semi-anonymous and non-anonymous MUCs</a></li>
<li><a href="#public-and-private-mucs">Public and private MUCs</a></li>
<li><a href="#moderated-channels">Moderated channels</a></li>
</ul>
</li>
<li><a href="#public-channels">Public channel recommendations</a>
<ul>
<li><a href="#non-technical-channels">Non-technical channels</a></li>
<li><a href="#technical-channels">Technical channels</a></li>
<li><a href="#openstreetmap-communities">OpenStreetMap communities</a></li>
</ul>
</li>
<li><a href="#bridges">Bridges</a>
<ul>
<li><a href="#bridging-to-irc">Bridging to IRC</a></li>
<li><a href="#bridging-to-matrix">Bridging to Matrix</a></li>
<li><a href="#the-downside-of-bridges">The downsides of bridges</a></li>
</ul>
</li>
<li><a href="#onboarding-and-advocacy">How to bring people to XMPP</a></li>
</ul>
</div>
</nav>
<section id="outline-container-why-xmpp-matters" class="outline-2">
<h2 id="why-xmpp-matters"><a href="#why-xmpp-matters">Why XMPP matters</a></h2>
<div class="outline-text-2" id="text-why-xmpp-matters">
<p>
<a href="https://xmpp.org/">XMPP</a> (also known as Jabber) is <i>the</i> <a href="https://en.wikipedia.org/wiki/Internet_Standard">Internet Standard</a> for instant messaging.
</p>
</div>
<div id="outline-container-modern-chat-features" class="outline-3">
<h3 id="modern-chat-features"><a href="#modern-chat-features">Modern chat features</a></h3>
<div class="outline-text-3" id="text-modern-chat-features">
<p>
XMPP supports modern chat features in a standardized and interoperable way (unlike IRC). Features include -
</p>

<ul class="org-ul">
<li>Multi-device support, offline messages, multi-line messages, message styling, end-to-end encryption, message correction, deletion, replies, receipts, read markers, typing notifications&#x2026;</li>

<li>Contact and room synchronization, avatars, file transfer, image previews, stickers, message reactions, encrypted audio and video calls, message threads&#x2026;</li>
</ul>
</div>
</div>
<div id="outline-container-federated-not-centralized" class="outline-3">
<h3 id="federated-not-centralized"><a href="#federated-not-centralized">Federated, not centralized</a></h3>
<div class="outline-text-3" id="text-federated-not-centralized">
<p>
XMPP is federated, rather than centralized (unlike IRC <label id='fnr.1' for='fnr-in.1.4993595' class='margin-toggle sidenote-number'><sup class='numeral'>1</sup></label><input type='checkbox' id='fnr-in.1.4993595' class='margin-toggle'><span class='sidenote'><sup class='numeral'>1</sup>
IRC is <i>theoretically</i> federated, but in practice it's just a bunch of networks which don't federate - you can't talk to people on OFTC with a libera.chat account, and so on.
</span>, Telegram, Discord, Signal, Slack, &#x2026;). That means -
</p>

<ul class="org-ul">
<li>You're not locked in to one "official" app. Choose whichever apps you like.</li>

<li>You're not locked in to one server - you can choose the server with the features, privacy policy, and customer support you want. And whatever server you choose, you can talk to users on any other server, using any XMPP app.</li>

<li>It's resistant to <a href="https://en.wikipedia.org/wiki/Enshittification">enshittification</a> - if one operator shuts down, or goes evil/anti-user <label id='fnr.2' for='fnr-in.2.5738824' class='margin-toggle sidenote-number'><sup class='numeral'>2</sup></label><input type='checkbox' id='fnr-in.2.5738824' class='margin-toggle'><span class='sidenote'><sup class='numeral'>2</sup>
&#x2026;as commercial services are so prone to - see <a href="https://en.wikipedia.org/wiki/Privacy_concerns_with_Google">Google</a>, <a href="https://en.wikipedia.org/wiki/Criticism_of_Facebook">Facebook</a>, <a href="https://en.wikipedia.org/wiki/2023_Reddit_API_controversy">Reddit</a>, <a href="https://en.wikipedia.org/wiki/Stack_Overflow#2023_controversy_over_AI-generated_content_and_moderation_strike">StackOverflow</a>, and many more.
</span>, you can switch servers and continue to access the rest of the network and using your existing apps.</li>

<li>It's more resilient against censorship. And multiple smaller, independant servers make less tempting targets for backdoors than a single big server.</li>

<li>You can actually set up your own server (public or private), which provides an unmatched level of trust in the server.</li>
</ul>
</div>
</div>
<div id="outline-container-freedom-respecting-software" class="outline-3">
<h3 id="freedom-respecting-software"><a href="#freedom-respecting-software">Freedom-respecting software</a></h3>
<div class="outline-text-3" id="text-freedom-respecting-software">
<p>
XMPP has a diverse ecosystem of <a href="https://en.wikipedia.org/wiki/Free_software">freedom-respecting</a> clients and servers. (Unlike Discord, Slack, Signal <label id='fnr.3' for='fnr-in.3.9214321' class='margin-toggle sidenote-number'><sup class='numeral'>3</sup></label><input type='checkbox' id='fnr-in.3.9214321' class='margin-toggle'><span class='sidenote'><sup class='numeral'>3</sup>
Signal uses proprietary services like Amazon Web Services and CloudFlare.
</span>, &#x2026;)
</p>
</div>
</div>
<div id="outline-container-easy-to-self-host" class="outline-3">
<h3 id="easy-to-self-host"><a href="#easy-to-self-host">Easy to self-host</a></h3>
<div class="outline-text-3" id="text-easy-to-self-host">
<p>
XMPP is easy and lightweight to self-host. (Unlike Matrix.) This has given rise to a thriving network of public servers.
</p>

<ul class="org-ul">
<li><p>
Matrix servers are resource- and maintenance-heavy to run (such that it has been called "the Bitcoin of chat"), which has resulted in most of the Matrix community being concentrated around the matrix.org server.
</p>

<p>
The cost of running a Matrix server regularly forces smaller, community-run servers to give up on it, who often fall back to XMPP.
</p>

<p>
That effectively makes Matrix a centralized protocol, with all the drawbacks that implies. If or when the matrix.org service shuts down or becomes anti-user, there will not be enough community-run servers to take in the refugees.
</p></li>
</ul>

<p>
The clients are also lightweight and performant. (Again, unlike Matrix.)
</p>
</div>
</div>
<div id="outline-container-community-governed-protocol" class="outline-3">
<h3 id="community-governed-protocol"><a href="#community-governed-protocol">Community-governed protocol</a></h3>
<div class="outline-text-3" id="text-community-governed-protocol">
<p>
The protocol is governed by a privacy-conscious community (the <a href="https://xmpp.org/about/xmpp-standards-foundation/">XSF</a>) rather than a single company.
</p>
</div>
</div>
<div id="outline-container-bridges" class="outline-3">
<h3 id="bridges"><a href="#bridges">Bridges</a></h3>
<div class="outline-text-3" id="text-bridges">
<p>
It has bridges to other chat protocols, so you can use any XMPP client for all your chats.
</p>
<ul class="org-ul">
<li><a href="https://biboumi.louiz.org/">Biboumi</a> for IRC</li>
<li><a href="https://github.com/arianetwork/bifrost/tree/aria-net-bifrost">Bifrost</a> for Matrix</li>
<li><a href="https://slidge.im/core/user/index.html">Slidge</a> for Discord, Facebook Messenger, Matrix, Mattermost, Signal, Skype, Steam Chat, Telegram, and WhatsApp.</li>
</ul>
</div>
</div>
</section>
<section id="outline-container-getting-started" class="outline-2">
<h2 id="getting-started"><a href="#getting-started">Getting started</a></h2>
<div class="outline-text-2" id="text-getting-started">
</div>
<div id="outline-container-easiest-way" class="outline-3">
<h3 id="easiest-way"><a href="#easiest-way">The easiest way</a></h3>
<div class="outline-text-3" id="text-easiest-way">
<p>
The easiest way to join the XMPP network is to install <a href="https://quicksy.im">Quicksy</a> from the <a href="https://play.google.com/store/apps/details?id=im.quicksy.client">Google Play Store</a> or the <a href="https://apps.apple.com/us/app/quicksy/id6538727270">Apple App Store</a>. It uses your phone number for registration, contact discovery, and password recovery. The Quicksy account works like any XMPP account and can be used from any XMPP client.
</p>

<p>
I recommend Quicksy to the majority of people who
</p>
<ul class="org-ul">
<li>don't want to spend time selecting a server,</li>
<li>don't use a password manager, <label id='fnr.4' for='fnr-in.4.9115149' class='margin-toggle sidenote-number'><sup class='numeral'>4</sup></label><input type='checkbox' id='fnr-in.4.9115149' class='margin-toggle'><span class='sidenote'><sup class='numeral'>4</sup>
For some reason, most public XMPP servers don't provide any kind of account recovery. The upside is that you don't even need to provide an email address to register. The downside is that people who don't use password managers are quite likely to get locked out of their account.
</span> and</li>
<li>don't mind using their phone number to register.</li>
</ul>

<p>
If that's you - install Quicksy, and skip ahead to <a href="#installing-a-client">check out some more clients</a> to use your Quicksy account with.
</p>

<p>
If that's not you, you need to select a server, make an account, and install a client. <label id='fnr.5' for='fnr-in.5.9956131' class='margin-toggle sidenote-number'><sup class='numeral'>5</sup></label><input type='checkbox' id='fnr-in.5.9956131' class='margin-toggle'><span class='sidenote'><sup class='numeral'>5</sup>
Even if your account is not on Quicksy, you can still use Quicksy to associate your account with a phone number. That makes it easier for your Quicksy contacts to discover your account.
</span>
</p>
</div>
</div>
<div id="outline-container-select-a-server" class="outline-3">
<h3 id="select-a-server"><a href="#select-a-server">Select a server and make an account</a></h3>
<div class="outline-text-3" id="text-select-a-server">
<p>
Public server recommendations may be found at <a href="https://providers.xmpp.net/">providers.xmpp.net</a> or <a href="https://compliance.conversations.im/old/">compliance.conversations.im/old</a>. Some servers support registering from the client (called In-Band Registration - IBR), others require you to register on their website to prevent spam.
</p>

<p>
If you're interested in self-hosting a private server, <a href="https://snikket.org/">Snikket</a> aims to make it really simple. It also rebrands popular XMPP servers and clients to provide an experience similar to centralized services. <label id='fnr.6' for='fnr-in.6.7036216' class='margin-toggle sidenote-number'><sup class='numeral'>6</sup></label><input type='checkbox' id='fnr-in.6.7036216' class='margin-toggle'><span class='sidenote'><sup class='numeral'>6</sup>
Users don't install app Foo on Android and app Bar on iOS, and you don't need to explain that all the apps and services work together&#x2026;everything is just "Snikket".
</span>
</p>

<p>
Snikket and <a href="https://conversations.im/">conversations.im</a> also offer paid hosting.
</p>
</div>
</div>
<div id="outline-container-installing-a-client" class="outline-3">
<h3 id="installing-a-client"><a href="#installing-a-client">Install a client</a></h3>
<div class="outline-text-3" id="text-installing-a-client">
<p>
For clients, we recommend&#x2026;
</p>

<ol class="org-ol">
<li>On mobile devices
<ul class="org-ul">
<li><a href="https://monocles.wiki/index.php?title=Monocles_Chat">Monocles Chat</a> or <a href="https://cheogram.com/">Cheogram</a> <label id='fnr.7' for='fnr-in.7.4017446' class='margin-toggle sidenote-number'><sup class='numeral'>7</sup></label><input type='checkbox' id='fnr-in.7.4017446' class='margin-toggle'><span class='sidenote'><sup class='numeral'>7</sup>
Monocles Chat and Cheogram are both forks of <a href="https://conversations.im/">Conversations</a>. Cheogram has some additional features. Monocles Chat has even more, and takes many cues from WhatsApp.
</span> for modern Android devices</li>
<li><a href="https://www.yaxim.org/">Yaxim</a> or <a href="https://play.google.com/store/apps/details?id=org.yaxim.bruno">Bruno</a> for old/low-end Android devices</li>
<li><a href="https://monal-im.org/">Monal</a> or <a href="https://siskin.im/">Siskin</a> for iOS</li>
<li><a href="https://git.disroot.org/badrihippo/convo/src/branch/master/README.md">Convo</a> for KaiOS/JioPhone</li>
</ul></li>
<li>On the desktop
<ol class="org-ol">
<li><a href="https://monal-im.org/">Monal</a> for Mac</li>
<li><a href="https://apps.microsoft.com/store/detail/9PGGF6HD43F9?launch=true&amp;mode=mini">Gajim</a> for Windows</li>
<li><a href="https://gajim.org/">Gajim</a> for GNU/Linux <label id='fnr.8' for='fnr-in.8.3691904' class='margin-toggle sidenote-number'><sup class='numeral'>8</sup></label><input type='checkbox' id='fnr-in.8.3691904' class='margin-toggle'><span class='sidenote'><sup class='numeral'>8</sup>
<a href="https://dino.im/">Dino</a> is another option for GNU/Linux, but it's in pretty early stages and has many issues. It's also the only XMPP client which supports group AV calls at the moment.
</span></li>
<li><a href="https://poez.io/">Poezio</a> or <a href="https://profanity-im.github.io/">Profanity</a> for the terminal</li>
</ol></li>
<li><a href="https://movim.eu/">Movim</a> (social-network-like) and <a href="https://conversejs.org/fullscreen.html">Converse.js</a> in the browser <label id='fnr.9' for='fnr-in.9.2765996' class='margin-toggle sidenote-number'><sup class='numeral'>9</sup></label><input type='checkbox' id='fnr-in.9.2765996' class='margin-toggle'><span class='sidenote'><sup class='numeral'>9</sup>
<a href="https://app.prose.org/">Prose</a> (similar to Slack or Discord) is very new and incomplete, but worth keeping an eye on.
</span></li>
</ol>

<p>
More clients can be found at <a href="https://xmpp.org/software/">xmpp.org/software</a>. There's an excellent table comparing clients at <a href="https://apps.xmpp24.de/en/">apps.xmpp24.de/en</a>.
</p>

<p>
Avoid clients with poor XMPP support, such as Pidgin and Thunderbird.
</p>
</div>
</div>
<div id="outline-container-getting-help" class="outline-3">
<h3 id="getting-help"><a href="#getting-help">Getting help</a></h3>
<div class="outline-text-3" id="text-getting-help">
<p>
You can join the <a href="https://xmpp.link/#chat@joinjabber.org?join">JoinJabber General Chat</a> to ask any XMPP questions you may have.
</p>
</div>
</div>
</section>
<section id="outline-container-sharing-xmpp-links" class="outline-2">
<h2 id="sharing-xmpp-links"><a href="#sharing-xmpp-links">Sharing XMPP links</a></h2>
<div class="outline-text-2" id="text-sharing-xmpp-links">
<p>
At this point, you probably want to share your XMPP address (also known as "Jabber ID", or "JID") with others, so they can reach you on XMPP.
</p>

<p>
A JID looks like <code>you@yourserver.tld</code> - just like an email address.
</p>

<p>
For Quicksy users, it looks like <code>+&lt;2-digit country code&gt;&lt;10-digit mobile number&gt;@quicksy.im</code>, e.g. <code>+919876543210@quicksy.im</code>
</p>

<p>
However, it's probably better to share an XMPP link instead. There are two kinds of XMPP links -
</p>

<ol class="org-ol">
<li><p>
xmpp: URIs, e.g. <a href="xmpp:me@myserver.tld">xmpp:me@myserver.tld</a> for 1:1 chat, or <a href="xmpp:mychannel@myserver.tld?join">xmpp:mychannel@myserver.tld?join</a> for joining a channel.
</p>

<p>
These are supported by most (if not all) XMPP clients. But they aren't supported in most non-XMPP software, and may not be clickable there. (Tell the developers of such apps to add support for XMPP links 🙂)
</p>

<p>
However, xmpp: URIs aren't very useful if the recipient doesn't have an XMPP client.
</p></li>

<li><p>
Using web invitation links, e.g. <a href="https://xmpp.link/#me@myserver.tld">https://xmpp.link/#me@myserver.tld</a> for 1:1 chat or <a href="https://xmpp.link/#mychannel@myserver.tld?join">https://xmpp.link/#mychannel@myserver.tld?join</a> for joining a channel.
</p>

<p>
<a href="https://xmpp.link/">xmpp.link</a> is just one invite service. You can also use others, like <a href="https://join.jabber.network/">join.jabber.network</a>, or host your own instance of <a href="https://github.com/modernxmpp/easy-xmpp-invitation">easy-xmpp-invitation</a>.
</p>

<p>
Web invitation links are supported everywhere, and can be opened even if the recipient doesn't have an XMPP client installed. The invitation page also suggests clients based on the user's operating system.
</p>

<p>
However, the recipient's browser needs to support JavaScript for the invitation to work, and these web invites don't work with Tor Browser (where the ability to launch external applications is disabled).
</p></li>
</ol>
</div>
</section>
<section id="outline-container-channels-and-group-chats" class="outline-2">
<h2 id="channels-and-group-chats"><a href="#channels-and-group-chats">Channels and group chats</a></h2>
<div class="outline-text-2" id="text-channels-and-group-chats">
<p>
Channels and group chats on XMPP (also known by their collective technical name - "Multi-User Chats", or "MUCs") can take a few different forms.
</p>
</div>
<div id="outline-container-semi-anonymous-and-non-anonymous-mucs" class="outline-3">
<h3 id="semi-anonymous-and-non-anonymous-mucs"><a href="#semi-anonymous-and-non-anonymous-mucs">Semi-anonymous and non-anonymous MUCs</a></h3>
<div class="outline-text-3" id="text-semi-anonymous-and-non-anonymous-mucs">
<p>
In <i>semi-anonymous MUCs</i>, only moderators can see your Jabber ID (JID) - so only moderators can DM you, add you as a contact, or invite you to other MUCs. This helps protect members against spam and harassment from other members.
</p>

<p>
If you want to DM a member of a semi-anonymous MUC (without having their JID), you can use <i>whispers</i> (also known as <i>MUC PMs</i>) to exchange JIDs. Note that whispers may be disabled in some MUCs.
</p>

<p>
In <i>non-anonymous MUCs</i>, your JID is visible to all members of the MUC. Some features - like end-to-end encryption and read markers - are only available in non-anonymous MUCs.
</p>
</div>
</div>
<div id="outline-container-public-and-private-mucs" class="outline-3">
<h3 id="public-and-private-mucs"><a href="#public-and-private-mucs">Public and private MUCs</a></h3>
<div class="outline-text-3" id="text-public-and-private-mucs">
<p>
Anyone can join a public MUC (also known as a "channel"), whereas private MUCs (also known as "group chats") are invite-only.
</p>
</div>
</div>
<div id="outline-container-moderated-channels" class="outline-3">
<h3 id="moderated-channels"><a href="#moderated-channels">Moderated channels</a></h3>
<div class="outline-text-3" id="text-moderated-channels">
<p>
Some channels may also be <i>moderated</i> - that means new members can't send messages by default, and mods have to grant them voice manually. This is very effective against spam.
</p>

<p>
In moderated channels, you can ask for voice by whispering to the moderators. Monocles Chat and Gajim (among others) have special support for easily sending a voice request to all moderators of a channel.
</p>
</div>
</div>
</section>
<section id="outline-container-public-channels" class="outline-2">
<h2 id="public-channels"><a href="#public-channels">Public channel recommendations</a></h2>
<div class="outline-text-2" id="text-public-channels">
<p>
You can use <a href="https://search.jabber.network/">search.jabber.network</a> to find public channels. Most XMPP clients have a channel search feature, too.
</p>

<p>
Here are some channels I can recommend. Please read their rules before you post.
</p>
</div>
<div id="outline-container-non-technical-channels" class="outline-3">
<h3 id="non-technical-channels"><a href="#non-technical-channels">Non-technical channels</a></h3>
<div class="outline-text-3" id="text-non-technical-channels">
<ol class="org-ol">
<li>The <a href="https://xmpp.link/#news@conference.macaw.me?join">news channel</a>, the <a href="https://xmpp.link/#news-discussion@chat.disroot.org?join">news discussion channel</a>, and the <a href="https://xmpp.link/#uplifting-news@chat.disroot.org?join">uplifting news channel</a></li>
<li><a href="https://xmpp.link/#art@chat.disroot.org?join">The Art Café</a>, for admirers &amp; creators of all arts. A place to share and discuss literature, poetry, music, sound, painting, illustration, comics/graphic novels, photography, sculpture, dance, theatre, film, TV, games, crafts&#x2026;</li>
<li>The <a href="https://xmpp.link/#gaming@chat.disroot.org?join">gaming channel</a>, a safe space for gaming discussion</li>
<li>The <a href="https://xmpp.link/#dungeons-and-dragons@muc.xmpp.earth?join">channel for DnD and other tabletop RPGs</a></li>
<li>The <a href="https://xmpp.link/#anime-manga@chat.disroot.org?join">anime and manga channel</a>, a safe space for fans of anime and manga</li>

<li>The <a href="https://xmpp.link/#vegans@chat.disroot.org?join">channel for vegans</a>, with frequent food pictures and cooking discussions</li>
<li>The <a href="https://xmpp.link/#craftmanship@muc.loqi.im?join">Little Project Hub</a> - dedicated to physical DIY projects, including gardening, housing, woodworking, welding, 3D printing, car tuning, etc.</li>
<li>The <a href="https://xmpp.link/#earthward@chat.disroot.org?join">Earthward channel</a> for earth-centered living, permaculture, gardening, and the environment. Also gets a lot of nature/animal photos and videos - jokingly called the National Geographic Channel of XMPP 🙂</li>
<li>The <a href="https://xmpp.link/#cats@chat.jabberfr.org?join">Cat Café</a>, the channel for felines and their friends.</li>

<li>The <a href="https://xmpp.link/#anarchism@chat.disroot.org?join">anarchism channel</a></li>
<li>The <a href="https://xmpp.link/#science@chat.disroot.org?join">channel for natural sciences</a></li>
<li>The <a href="https://xmpp.link/#philosophy@chat.disroot.org?join">philosophy channel</a></li>
<li>The <a href="https://xmpp.link/#nvc@salas.suchat.org?join">channel for Nonviolent Communication / Compassionate Communication</a></li>
<li>The <a href="https://xmpp.link/#depression@chat.disroot.org?join">Mental Health Chat</a>, a safe space to talk about mental health</li>
<li>The <a href="https://xmpp.link/#lgbt@conference.queer-spark.org?join">channel for LGBTQIA+ and their allies.</a></li>
<li>The <a href="https://xmpp.link/#india@conference.a3.pm?join">channel for Indians and Indophiles</a></li>

<li>The <a href="https://xmpp.link/#travel@conference.conversations.im?join">Wanderer's Inn</a>, the channel for travelers, their stories, and their gear</li>
<li>The <a href="https://xmpp.link/#bicycles@conference.samwhited.com?join">bicycles channel</a>, hosted by a professional bicycle repairer. Has some <a href="https://en.wikipedia.org/wiki/Velomobile">velomobile</a> users too.</li>
<li>The <a href="https://xmpp.link/#trains@conference.jabjab.de?join">XMPP Railway Yard</a> - a channel for fans of trains. Very active, and just a little crazy.</li>
<li>The <a href="https://xmpp.link/#public-transit@muc.xmpp.earth?join">public transit channel</a></li>
<li>The <a href="https://xmpp.link/#carnuts@code.moparisthe.best?join">carnuts channel</a> for car-lovers</li>
</ol>
</div>
</div>
<div id="outline-container-technical-channels" class="outline-3">
<h3 id="technical-channels"><a href="#technical-channels">Technical channels</a></h3>
<div class="outline-text-3" id="text-technical-channels">
<ol class="org-ol">
<li>The <a href="https://xmpp.link/#chat@joinjabber.org?join">JoinJabber channel</a>, a safe space for XMPP users and developers</li>
<li>The <a href="https://xmpp.link/#tinkering@conference.macaw.me?join">Tinkerspace</a>, a safe space for programming and general tech discussion</li>
<li>The <a href="https://xmpp.link/#guix@chat.disroot.org?join">Guix channel</a></li>
<li>The <a href="https://xmpp.link/#emacs@conference.conversations.im?join">Emacs channel</a></li>
<li>The <a href="https://xmpp.link/#lisp@conference.a3.pm?join">Lisp channel</a> (for Common Lisp, Scheme, Emacs Lisp, Clojure, PicoLisp, and others)</li>
<li>The <a href="https://xmpp.link/#openhardware@conference.magicbroccoli.de?join">Open Hardware Chat</a></li>
<li>The <a href="https://xmpp.link/#lineage@conference.magicbroccoli.de?join">channel for LineageOS and other Android ROMs</a></li>
<li>The <a href="https://xmpp.link/#postmarketos@conference.vitali64.duckdns.org?join">channel for postmarketOS</a></li>
<li>The <a href="https://xmpp.link/#fsci@muc.tchncs.de?join">Free Software Community of India</a></li>
<li>The <a href="https://xmpp.link/#mechanicalkeyboards@conference.jabjab.de?join">Mechanical Keyboards</a> channel</li>
</ol>
</div>
</div>
<div id="outline-container-openstreetmap-communities" class="outline-3">
<h3 id="openstreetmap-communities"><a href="#openstreetmap-communities">OpenStreetMap communities</a></h3>
<div class="outline-text-3" id="text-openstreetmap-communities">
<ol class="org-ol">
<li>The <a href="https://xmpp.link/#openstreetmap@conference.macaw.me?join">global OpenStreetMap channel</a></li>
<li>The <a href="https://xmpp.link/#osm-fr@chat.jabberfr.org?join">French OSM community</a></li>
<li>The <a href="https://xmpp.link/#openstreetmap-de@rooms.dismail.de?join">German OSM community</a></li>
<li>The <a href="https://xmpp.link/#osm-in@conference.a3.pm?join">Indian OSM community</a></li>
<li>The <a href="https://xmpp.link/#osm-delhi@conference.jabbers.one?join">Delhi OSM community</a></li>
<li>The <a href="https://xmpp.org/#osmbengal@conference.conversations.im?join">West Bengal OSM community</a></li>
</ol>
</div>
</div>
</section>
<section id="outline-container-bridges" class="outline-2">
<h2 id="bridges"><a href="#bridges">Bridges</a></h2>
<div class="outline-text-2" id="text-bridges">
<p>
If you don't know what bridges, IRC, or Matrix are, skip ahead to <a href="#onboarding-and-advocacy">the final section</a>.
</p>
</div>
<div id="outline-container-bridging-to-irc" class="outline-3">
<h3 id="bridging-to-irc"><a href="#bridging-to-irc">Bridging to IRC</a></h3>
<div class="outline-text-3" id="text-bridging-to-irc">
<p>
You can join IRC channels by joining <code>#&lt;channel&gt;%&lt;IRC server&gt;@&lt;Biboumi server&gt;</code>. For example -
</p>

<p>
<code>#commonlisp%irc.libera.chat@irc.jabberfr.org</code>
</p>

<p>
In addition to <a href="https://jabberfr.org">irc.jabberfr.org</a>, <a href="https://hmm.st/">hmm.st</a> is another public Biboumi instance.
</p>

<p>
Similarly, you can send PMs to <code>&lt;username&gt;%&lt;IRC server&gt;@&lt;Biboumi server&gt;</code>. For example, to register your nickname on OFTC, send a message to -
</p>

<p>
<code>nickserv%irc.oftc.net@irc.jabberfr.org</code>
</p>

<p>
For more information, check out the <a href="https://doc.biboumi.louiz.org/9.0/user.html">Biboumi user documentation</a>.
</p>
</div>
<div id="outline-container-persistent" class="outline-4">
<h4 id="persistent"><a href="#persistent">Persistent IRC connections</a></h4>
<div class="outline-text-4" id="text-persistent">
<p>
You probably want to make Biboumi's connection to an IRC channel persistent. That way, you will remain in the IRC channel, even if your XMPP client is disconnected. That prevents sending excessive joins and parts to the IRC channel if you have a patchy network, and you won't miss the room history when disconnected.
</p>

<p>
For that, you may need to make a one-off configuration for each channel using an XMPP client which supports Ad-Hoc Commands (AHC). These include -
</p>
<ul class="org-ul">
<li><a href="https://conversejs.org/fullscreen.html">Converse.js</a> or <a href="https://movim.eu/">Movim</a> (web)</li>
<li><a href="https://gajim.org/">Gajim</a> (desktop)
<ul class="org-ul">
<li>open an IRC channel, click on the overflow menu - "Execute Command" - "Configure a few settings for this IRC channel" - enable "Persistent"</li>
</ul></li>
<li><a href="https://monocles.wiki/index.php?title=Monocles_Chat">Monocles Chat</a> (Android)
<ul class="org-ul">
<li>open an IRC channel, and tap on the "Commands" tab - "Configure a few settings for this IRC channel" - enable "Persistent"</li>
</ul></li>
<li><a href="https://poez.io/">Poezio</a> or <a href="https://profanity-im.github.io/">Profanity</a> (terminal)</li>
</ul>
</div>
</div>
</div>
<div id="outline-container-bridging-to-matrix" class="outline-3">
<h3 id="bridging-to-matrix"><a href="#bridging-to-matrix">Bridging to Matrix</a></h3>
<div class="outline-text-3" id="text-bridging-to-matrix">
<p>
You can speak to Matrix users by adding <code>&lt;user&gt;_&lt;domain&gt;@&lt;Bifrost server&gt;</code> as a contact. For example -
</p>

<p>
<code>alice_matrix.org@aria-net.org</code>
</p>

<p>
You can join Matrix rooms by joining <code>#&lt;room&gt;#&lt;Matrix server&gt;@&lt;Bifrost server&gt;</code>. For example -
</p>

<p>
<code>#malleable-systems#matrix.org@aria-net.org</code>
</p>

<p>
Some rooms need a different syntax, such as the OSM IRC channel -
</p>

<p>
<code>#_oftc_#osm#matrix.org@aria-net.org</code>
</p>

<p>
In addition to the <code>aria-net.org</code> Bifrost instance, there's also one on <code>matrix.org</code>, but it has fewer features.
</p>

<p>
You can also bridge a Matrix and an XMPP room together, so that the XMPP room is discoverable to XMPP users via <a href="https://search.jabber.network/">search.jabber.network</a>, and if either one goes down, the other keeps running as usual.
</p>

<ol class="org-ol">
<li>Invite <code>@_bifrost_bot:aria-net.org</code> to the Matrix side</li>

<li>Type <code>!bifrost bridge xmpp-js &lt;XMPP room domain&gt;.&lt;tld&gt; &lt;XMPP room name&gt;</code></li>
</ol>

<p>
You can join <a href="https://xmpp.link/#lighthouse@conference.lightwitch.org?join">The Lighthouse</a> for help with the aria-net.org bridge.
</p>
</div>
</div>
<div id="outline-container-the-downside-of-bridges" class="outline-3">
<h3 id="the-downside-of-bridges"><a href="#the-downside-of-bridges">The downsides of bridges</a></h3>
<div class="outline-text-3" id="text-the-downside-of-bridges">
<p>
While bridges are rather fashionable at the moment, there are several serious issues with them that conventional wisdom usually downplays.
</p>

<p>
In most cases, bridges are developed and encouraged only as far as they can help existing communities move to somebody else's ecosystem. The one controlling the new ecosystem is usually a heavily-funded party with the motive of poaching users. These parties may have you convinced (through excellent marketing) that their motives are purely altruistic, but look even a little closer and the façade fizzles away.
</p>

<p>
Downsides of bridges in general
</p>
<ol class="org-ol">
<li>Spam</li>
<li>Downtime, resulting in dropped messages</li>
<li>Messages arriving out of order</li>
<li>Being restricted to the mutually-supported subset of functionality.</li>
<li>Bridges may expose your XMPP address to all participants.</li>
<li>Bridges in general destroy any data sovereignty/privacy assurances you might otherwise have had. This is important even for public rooms. This is especially relevant when bridging to proprietary networks, but also when bridging to Matrix.</li>
</ol>

<p>
Downsides of bridges to proprietary software
</p>
<ol class="org-ol">
<li>Bridges keep us stuck in the past instead of improving things. Instead of helping people get away from legacy, proprietary, or centralized networks, bridges help people stay on them. They can even encourage people to move <i>away</i> from free networks to proprietary/centralized networks.</li>
</ol>

<p>
Downsides of Matrix bridges in particular
</p>
<ol class="org-ol">
<li>As with Matrix itself, Matrix bridges give a false sense of increased population - typically a 10:1 ratio of ghost users to real users. Plenty of people fall for this trick and are tempted to bridge to Matrix. Obviously, this inflated participant count does not result in an actual increase in activity.</li>

<li><p>
Matrix-XMPP bridges change XMPP links to Matrix links, so Matrix users don't have to leave Matrix to use XMPP.
</p>

<p>
On the other hand, the same bridges don't similarly translate Matrix links for XMPP users, thereby encouraging them to join Matrix.
</p>

<p>
This gives further weight to the suspicion that bridges are really a means of poaching users.
</p></li>

<li>Matrix bridges (just like Matrix servers) require significant server resources and human effort (in maintenance) to run. Low-resource community-run bridges may thus suffer from significant downtime. They can also take an age to connect to.</li>
</ol>

<p>
Bridges are antithetical to my aim of moving people from proprietary and centralized networks to federated networks of freedom-respecting software. Thus, I prefer to be in unbridged XMPP rooms, and to get existing communities to move to XMPP.
</p>

<p>
Further reading - <a href="https://www.freie-messenger.de/en/matrix/gedanken/#bridges">freie-messenger.de/en/matrix/gedanken/#bridges</a>
</p>
</div>
</div>
</section>
<section id="outline-container-onboarding-and-advocacy" class="outline-2">
<h2 id="onboarding-and-advocacy"><a href="#onboarding-and-advocacy">How to bring people to XMPP</a></h2>
<div class="outline-text-2" id="text-onboarding-and-advocacy">
<p>
Proprietary and centralized platforms currently dominate the instant messaging world. A lot of people prefer to use these platforms rather than XMPP. If we care about wresting back control of our communications, it is <i>imperative</i> that we fix this.
</p>

<p>
It basically boils to this - <i>use XMPP for everything.</i>
</p>

<p>
Do as many of the following as possible -
</p>

<ol class="org-ol">
<li><i>Minimize the value you create for other platforms</i>, by&#x2026;
<ol class="org-ol">
<li>Making yourself harder to reach there, e.g. checking them less often than XMPP, replying on them less quickly than XMPP, etc.</li>

<li>Minimizing your activity on them, e.g. using them only to invite others to XMPP.</li>

<li>Quitting them entirely.</li>
</ol></li>

<li><i>Increase the value you create for XMPP</i>, by&#x2026;
<ol class="org-ol">
<li>Making XMPP your primary means of communication.</li>

<li>Actively participating in XMPP communities.</li>

<li>Informing people that you prefer to be contacted over XMPP. If asked, tell them why.</li>

<li>Helping onboard friends, family, colleagues, students, etc to XMPP. If necessary, introduce them to mutual contacts, and channels they may be interested in.

<ul class="org-ul">
<li>Don't tell them to "use XMPP" - tell them to "install Quicksy [from the Play Store/App Store]". That takes care of selecting a client and a server in one go, while also providing contact discovery and easy password recovery. Tell them about alternative clients and "XMPP" (the underlying protocol) later.</li>
</ul></li>

<li>Moving existing communities, teams, companies, etc to XMPP.</li>

<li>Choosing XMPP as the primary chat for new communities, teams, companies, etc.</li>
</ol></li>

<li><i>Tell others to engage in the same steps above.</i></li>
</ol>

<p>
This is an easy way to contribute to freedom-respecting software and privacy-respecting technologies, without requiring the skills of a programmer, translator, or designer. <label id='fnr.10' for='fnr-in.10.3653647' class='margin-toggle sidenote-number'><sup class='numeral'>10</sup></label><input type='checkbox' id='fnr-in.10.3653647' class='margin-toggle'><span class='sidenote'><sup class='numeral'>10</sup>
Of course, XMPP clients are also - like most FOSS projects - in need of developers, translators, and designers.
</span>
</p>

<p>
Those suggestions come from experience - they are proven to work, provided you possess the necessary spine and steadfastness. Here's how I've applied these measures personally.
</p>

<ul class="org-ul">
<li>I'm not on WhatsApp, Telegram, Instagram, or Discord, so if people want to talk to me they <i>have</i> to use XMPP&#x2026;or fall back to SMS or email, which is very limiting.</li>

<li>When I first joined, many channels of my interest were missing. I made 9 of the channels I listed above, publicized them so people would join them, talked to channel participants to increase activity, and minimized my participation in IRC and Matrix. (Most of my activity on IRC these days is aimed at encouraging people to try and move to XMPP.)</li>

<li>As a frequent <a href="blog/mapping-party-tips.html#announcing-inviting">organizer of OpenStreetMap mapping parties</a>, I onboarded many people to XMPP, and made the OSM India XMPP channel. I announce these parties in XMPP channels multiple weeks in advance; in channels bridged to (proprietary) Telegram or (unsustainable) Matrix, I announce them one week before the party&#x2026;and I make sure to tell them to join the XMPP channel to get informed sooner 🙂</li>

<li>I've been part of efforts to spread awareness of XMPP at local FOSS conferences.</li>
</ul>

<p>
The more people who use and promote XMPP exclusively, the sooner we get a world where freedom-respecting, privacy-conscious, federated, and sustainable communication becomes the norm.
</p>



<p>
 <footer class="links">
<a href="contact.html">Send me a comment</a><br>
•<br>
<a href="support.html">Buy me a coffee</a><br>
•<br>
<a href="https://codeberg.org/contrapunctus/pages">Study or improve this website on Codeberg</a><br>
 </footer>
</p>
</div>
</section>
</article>
</body>
</html>
