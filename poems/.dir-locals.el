;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-html-postamble . nil)
              (org-export-with-section-numbers . nil)
              (org-export-with-toc . nil)
              (org-html-self-link-headlines . t)
              (org-html-head . "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\" />")
              (eval . (add-hook 'after-save-hook #'org-tufte-export-to-html 0 t))
              (eval . (auto-id-mode 1)))))
